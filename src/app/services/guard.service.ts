import {Inject, Injectable} from '@angular/core';
import {Router} from '@angular/router';

import { GlobalVariable } from '../core/com-classes/index';
import {LocalStorageService} from '../core/services';

@Injectable()
export class GuardService {
  constructor(private router: Router,
              private gVariable: GlobalVariable,
              private storage: LocalStorageService) {
  }

  public activateAuthentication() {
    const authentication = this.storage.get('authentication_pk');
    this.gVariable.authentication = authentication;
    this.gVariable.authentication.authorized = true;
  }

  public createAuthentication(value: any) {
    value.authorized = true;
    this.storage.set('authentication_pk', value);
    this.activateAuthentication();
  }

  public updateAuthentication(value: any) {
    this.storage.set('authentication_pk', value);
    this.activateAuthentication();
  }

  public removeAuthentication() {
    this.storage.remove('authentication_pk');
    this.gVariable.authentication = {};
  }

  public getAuthentication() {
    return this.storage.get('authentication_pk');
  }

  public setEntitlements(value: any) {
    if (this.storage.get('authentication_entitlements_pk')) {
        this.storage.remove('authentication_entitlements_pk');
    }

      this.storage.set('authentication_entitlements_pk', value);
      this.gVariable.entitlements = value;
  }

  public getEntitlements() {
    // console.log(this.storage.get('authentication_entitlements_pk'));
      return this.storage.get('authentication_entitlements_pk');
  }

  public setForgetPasswordEmail(value: any) {
      if (this.storage.get('forget_password_email_pk')) {
          this.storage.remove('forget_password_email_pk');
      }

      this.storage.set('forget_password_email_pk', value);
  }

  public getForgetPasswordEmail() {
    return this.storage.get('forget_password_email_pk');
  }


}
