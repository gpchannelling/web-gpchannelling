import { Injectable } from '@angular/core';
import * as jsSHA from 'jssha';
import {StaticConfig} from '../core/config';

@Injectable({
    providedIn: 'root'
})

export class MapHashingService {
    // mapHasing.service
    private staticConfig: any = StaticConfig;

    constructor() {

    }

    public createHash(hashText) {
        let js_sha = new jsSHA('SHA-256', 'TEXT');
        js_sha.update(this.staticConfig.secretKey + btoa(hashText));
        const hasCode = js_sha.getHash('HEX');
        // console.log(hasCode);
        return hasCode.toUpperCase();
    }
}