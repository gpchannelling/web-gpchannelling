import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {GlobalVariable} from '../core/com-classes';
import {MessagesService} from './message.service';
import {GuardService} from './guard.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private gVariable: GlobalVariable, private router: Router, private messagesSev: MessagesService, private guardSev: GuardService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.gVariable.platformBrowser) {
        this.gVariable.authentication = this.guardSev.getAuthentication() || {};
        if (this.gVariable.authentication && this.gVariable.authentication.authorized ) {
            return true;
        } else {
            this.messagesSev.mesageError('You are not authorized to access');
            this.router.navigate(['']);
        }
    }

  }
}
