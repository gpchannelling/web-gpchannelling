import { Injectable } from '@angular/core';
import { Md5 } from 'ts-md5';

import { HttpService, AppConfig, ApiServiceConfig, GlobalVariable } from '../core';
import {reject} from 'q';
import {DataMapService} from '../core/services';
import {MapHashingService} from './mapHashing.service';

@Injectable()
export class ClientService {

  constructor(private httpService: HttpService,
              private gVariable: GlobalVariable,
              private mapSev: DataMapService,
              private mapHashingSev: MapHashingService) { }

    public getCenterListForMap(req) {
        let responseData = [];
        const longitude = req.longitude;
        const latitude = req.latitude;
        const distance = req.distance;


        const promise = new Promise((resolve, reject) => {
            this.httpService.httpGet(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE,
                 '/centers?longitude=' + longitude + '&latitude=' + latitude + '&distance=' + distance, {}, {})
                .then((data: any) => {
                    if (data){
                        for (let i in data) {
                            responseData.push(this.mapSev.mapCenterListForMap(data[i]));
                        }
                    }
                    resolve(responseData);
                }).catch((error: any) => {
                    reject(error);
            });
        });
        return promise;
    }

    public login(req: any) {
        const formattedReq = {
          'email': req.userName || null,
          'password': this.mapHashingSev.createHash(req.passWord) || null
        };

        const promise = new Promise((resolve, reject) =>
          this.httpService.httpPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/users/signin', formattedReq, {})
            .then((data: any) => {
              if (data) {
                resolve(data);
              }
            }).catch((error: any) => {
            resolve(error);
          }));
        return promise;
    }

    public passwordUpdate(req: any) {
        const formattedReq = {
            oldPassword: this.mapHashingSev.createHash(req.currentPassword) || null,
            newPassword: this.mapHashingSev.createHash(req.newPassword) || null,
            reNewPassword: this.mapHashingSev.createHash(req.confirmNewPassword) || null,
        };

        const promise = new Promise((resolve, reject) =>
            this.httpService.httpPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/users/reset-password',
                formattedReq, {})
                .then((data: any) => {
                    if (data) {
                        resolve(data);
                    } else {
                        resolve({});
                    }
                }).catch((error: any) => {
                reject(error);
            }));
        return promise;
    }

    public forgetPassword(req: any) {
        const formattedReq = {
            email: req.email || null,
            callBackUrl: req.callBackUrl || null
        };

        let promise = new Promise( (resolve , reject) =>
            this.httpService.httpPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/users/forgot-password', formattedReq, {})
                .then( (data: any) => {

                    resolve(data);
                }).catch( (error: any) => {
                    reject(error);
            }));
        return promise;
    }

    public passwordReset(req: any) {

        const formattedReq = {
            token: req.token ||  null,
            newPassword: this.mapHashingSev.createHash(req.newPassword) || null,
            reNewPassword: this.mapHashingSev.createHash(req.reNewPassword) || null,
        };

        const promise = new Promise((resolve, reject) =>
            this.httpService.httpPut(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/users/change-password',
                formattedReq, {})
                .then((data: any) => {
                    if (data) {
                        resolve(data);
                    } else {
                        resolve({});
                    }
                }).catch((error: any) => {
                reject(error);
            }));
        return promise;
    }

    public updateDoctor(req: any) {
        // console.log(req);
        const promise = new Promise((resolve, reject) =>
            this.httpService.httpPut(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/doctors/' + req.doctorId, req, {})
                .then((data: any) => {
                    if (data) {
                        // console.log(data);
                        resolve(data);
                    }
                }).catch((error: any) => {
                resolve(error);
            }));
        return promise;
    }

    public updateCenter(req: any) {
        // console.log(req);
        const promise = new Promise((resolve, reject) =>
            this.httpService.httpPut(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/centers/' + req.centerId, req, {})
                .then((data: any) => {
                    if (data) {
                        // console.log(data);
                        resolve(data);
                    }
                }).catch((error: any) => {
                resolve(error);
            }));
        return promise;
    }

    public getFeesList() {
        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/charge-codes/', {}, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    // to get the session list of a particular center and doctor
    public getDoctorById(doctorId: number) {
        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/doctors/' + doctorId, {}, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    // to get the session list of a particular center and doctor
    public getCenterById(getCenterById: number) {
        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/centers/' + getCenterById, {}, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    public getAllDoctorsList(centerId: number) {
      const promise = new Promise( (resolve, reject) => {
          this.httpService.httpGet(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/centers/' + centerId + '/doctors', {} , {}).
          then( (response: any) => {
                  resolve(response);
          }).catch( (error: any) => {
              resolve(error);
          });
      });
      return promise;
  }


    public getDoctorsListByName(name: string) {
      const promise = new Promise( (resolve, reject) => {
          this.httpService.httpGet(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/doctors?name=' + name , {} , {}).
          then( (response: any) => {
                  resolve(response);
          }).catch( (error: any) => {
              resolve(error);
          });
      });
      return promise;
    }

    public getAllCentersList(doctorId: number) {
      const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
          AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/doctors/' + doctorId + '/centers', {}, {})
          .then( (response: any) => {

              resolve(response);
          }).catch((error: any) => {
              resolve(error);
          });
      });
      return promise;
  }

    public getSessionsList(req) {
      const promise = new Promise( (resolve, reject) => {this.httpService.httpPost(
          AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE,  '/sessions', req, {})
          .then( (response: any) => {

              resolve(response);
          }).catch((error: any) => {
              resolve(error);
          });
      });
      return promise;
  }

    public bookAppointment(req: any, sessionId) {

        const promise = new Promise((resolve, reject) =>
            this.httpService.httpPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/sessions/' + sessionId + '/appointments',
                req, {})
                .then((data: any) => {
                    if (data) {

                        resolve(data);
                    }
                }).catch((error: any) => {
                resolve(error);
            }));
        return promise;
    }

    public addSessionSchedule(req, centerId, doctorId) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpPost(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/centers/' + centerId + '/doctors/' + doctorId + '/sessions', req, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    public getSessionDataByID(sessionId) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/sessions/' + sessionId, {}, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    public updateSession(req) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpPut(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/sessions/update', req, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });


        return promise;
    }

    public getAppointmentsListBySessionId(sessionId) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/sessions/' + sessionId + '/appointments', {}, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });


        return promise;
    }

    public getAppointmentByRefNo(RefNo) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/appointments/' + RefNo, {}, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });


        return promise;
    }

    public getAppointmentByRefNoForPaymentUpdate(RefNo) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/appointments/' + RefNo + '/charges-update', {}, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });


        return promise;
    }

    public saveConfirmAppointment(RefNo) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/confirm-appointment/' + RefNo, {}, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });


        return promise;
    }

    public getAppointmentByHashKey(key) {
        const promise = new Promise( (resolve, reject) => {this.httpService.httpGet(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/appointments?key=' + key, {}, {})
            .then( (response: any) => {
                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    public addAppointmentViewed(req) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpPut(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/sessions/current-patient', req, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });


        return promise;
    }

    public changeAppointmentStatus(req) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpPut(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/appointments/status', req, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    public updateAppointment(req) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpPut(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/appointments/update', req, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    public updateSessionStatus(req) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpPut(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/sessions/status', req, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });


        return promise;
    }

    public doctorNotification(req, sessionId) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpPost(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/notifications/' + sessionId, req, {})
            .then( (response: any) => {
                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });


        return promise;
    }

    public getPaymentTypes() {
      const promise = new Promise( (resolve, reject) => {
          this.httpService.httpGet(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/payment-gateways', {} , {})
              .then( (response: any) => {
                  resolve(response);
              }).catch( (error: any) => {

          });
      });

      return promise;
    }

    public getIncomeReports(req) {
        const promise = new Promise( (resolve, reject) => {this.httpService.httpPost(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/income-reports', req, {})
            .then( (response: any) => {
                // console.log(JSON.stringify(response));
                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    public confirmPayment(req) {

        const promise = new Promise( (resolve, reject) => {
            this.httpService.httpPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/appointments/confirm', req, {})
                .then( (response: any) => {
                    resolve(response);
                }).catch( (error: any) => {
                    resolve(error);
            });
        });

        return promise;
    }

    public updateAppointmentPayment(req) {

        const promise = new Promise( (resolve, reject) => {this.httpService.httpPut(
            AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/appointments/payments', req, {})
            .then( (response: any) => {

                resolve(response);
            }).catch((error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    public getTermsAndConditions() {
      const promise = new Promise( (resolve, reject) => {
          this.httpService.httpGet(
              AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/terms-and-conditions', {}, {}
          ).then( (response: any) => {
                resolve(response);
          }).catch( (error: any) => {
              resolve(error);
          });
      });

      return promise;
    }

    public fileUpload(req) {
      const formattedReq = req;

      return new Promise( (resolve, reject) => {
          this.httpService.httpFileUploadPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/users/files', req, {})
              .then( (response: any) => {
                  resolve(response);
              })
              .catch( (error: any) => {
                  resolve(error);
              });
      });
    }

    public creditCheck(req) {

        return new Promise( (resolve, reject) => {
            this.httpService.httpEchPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/EChMobitelAddToBill/creditCheck', req, {})
                .then( (response: any) => {
                    resolve(response);
                })
                .catch( (error: any) => {
                    resolve(error);
                });
        });
    }

    public mobitelVerifyPayment(req) {

        return new Promise( (resolve, reject) => {
            this.httpService.httpEchPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/EChMobitelAddToBill/save-return-url', req, {})
                .then( (response: any) => {
                    resolve(response);
                })
                .catch( (error: any) => {
                    resolve(error);
                });
        });
    }

    public creditConfirm(req) {

        return new Promise( (resolve, reject) => {
            this.httpService.httpEchPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/EChMobitelAddToBill/confirm', req, {})
                .then( (response: any) => {
                    resolve(response);
                })
                .catch( (error: any) => {
                    resolve(error);
                });
        });
    }

    public payNow(req, path) {

        return new Promise( (resolve, reject) => {
            this.httpService.httpGet(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, path, req, {})
                .then( (response: any) => {
                    resolve(response);
                })
                .catch( (error: any) => {
                    resolve(error);
                });
        });
    }

    public getAllDoctors() {
        return new Promise( (resolve, reject) => {
            this.httpService.httpGet(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/doctors/names', {} , {})
                .then( (response: any) => {
                    resolve(response);
                })
                .catch( (error: any) => {
                    resolve(error);
                });
        });
    }

    public paymentComplete(queryString: string) {
        const promise = new Promise( (resolve, reject) => {
            this.httpService.httpGet(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/payment-complete?' + queryString, {} , {}).
            then( (response: any) => {
                resolve(response);
            }).catch( (error: any) => {
                resolve(error);
            });
        });
        return promise;
    }

    selfRegistration(req: any) {
        return new Promise( (resolve, reject) => {
            this.httpService.httpPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/self-registration', req, {})
                .then( (response: any) => {
                    resolve(response);
                })
                .catch( (error: any) => {
                    resolve(error);
                });
        });
    }

    userRegistration(req: any) {
        return new Promise( (resolve, reject) => {
            this.httpService.httpPost(AppConfig.API_URL, ApiServiceConfig.GPC_SERVICE, '/users/register', req, {})
                .then( (response: any) => {
                    resolve(response);
                })
                .catch( (error: any) => {
                    resolve(error);
                });
        });
    }
}
