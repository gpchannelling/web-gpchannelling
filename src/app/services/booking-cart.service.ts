import { Inject, Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

import { ApiServiceConfig, AppConfig, StaticConfig } from '../core/config/index';
import { HttpService } from '../core/services/http.service';
import { LocalStorageService } from '../core/services/local-storage.service';
import {promise} from 'selenium-webdriver';

@Injectable({
  providedIn: 'root'
})

export class BookingCartService {

  private cart: any = {
      appointment: {
          refNo: 0,
          request: {
              appointmentCharges: [],
              patient: {
                  dateOfBirth: null,
                  mobileNo: null,
                  name: null,
                  nic: null,

              },
              refNo: null
          },
          response: {
              appointmentCharges: [],
              appointmentNo: 0,
              appointmentTime: null,
              center: {
                  address: {
                      address: null,
                      city: null
                  },
                  centerId: 0,
                  centerName: null,
                  contactNo: null,
                  openTime: null,
                  status: null
              },
              patient: {
                  dateOfBirth: null,
                  mobileNo: null,
                  name: null,
                  nic: null,
                  patientId: null
              },
              refNo: 0,
              session: {
                  appointmentDate: null,
                  center: {
                      address: {
                          city: null,
                          address: null
                      },
                      centerId: 0,
                      centerName: null,
                      contactNo: null,
                      openTime: null,
                      status: null
                  },
                  chargeDescription: null,
                  currentPatient: 0,
                  doctor: {
                      doctorId: 0,
                      slmcNo: null,
                      user: {
                          contactNo: null,
                          email: null,
                          firstName: null,
                          lastName: null
                      }
                  },
                  endTime: null,
                  noOfPatient: 0,
                  noOfReservedPatient: 0,
                  sessionFees: [],
                  sessionId: 0,
                  startTime: null,
                  status: null,
                  viewDuration: 0,
                  webStatus: null,
              },
              total: 0,

          },
      },
      payment: {
          request: {
              paymentMode: null,
              refNo: null
          },
          response: {
              status: null
          }
      },
      selectedSessionId: 0,
      step: StaticConfig.appoinmentBookingStages.INIT,
      cartExpTime: null,
      createdTime: null
  };


  constructor(private storage: LocalStorageService, private datePipe: DatePipe, private httpService: HttpService) {}

  public setSelectedBookingObj(selectedObj) {
    const oldSelectedObj = this.storage.get('selectedBookingObj_pk');
    if (oldSelectedObj) {
      this.storage.remove('selectedBookingObj_pk');
    }

    this.storage.set('selectedBookingObj_pk', selectedObj);
  }

  public getSelectedBookingObj() {
      const selectedObj = this.storage.get('selectedBookingObj_pk');
      return selectedObj;
  }

  public initCart($event) {
      const cart = this.storage.get('bookingEChannelCart_pk');
      if (!cart) {
          this.storage.remove('bookingEChannelCart_pk');
      }

      const d = new Date();
      this.cart.createdTime = new Date();
      d.setHours(d.getHours() + 24);
      this.cart.cartExpTime = d.getTime();


      if ($event.selectedSessionId > 0) {
          this.cart.selectedSessionId = $event.selectedSessionId;
      }

      this.storage.set('bookingEChannelCart_pk', this.cart);
      return this.cart;
  }

  public getBookingCart() {
    let cart = this.storage.get('bookingEChannelCart_pk');
    if (!cart) {
      cart = this.initCart({});
    } else {
      const nowTime = new Date().getTime();
      if (!cart.cartExpTime) {
        cart = this.initCart({});
      } else if (cart.cartExpTime < nowTime) {
        cart = this.initCart({});
      }
    }
    return cart;
  }

  public removeCart() {
      const cart = this.storage.get('bookingEChannelCart_pk');
      if (!cart) {
          this.storage.remove('bookingEChannelCart_pk');
      }
  }


  public resetBookingCart() {
    const cart = this.getBookingCart();

    cart.appointment.request.appointmentCharges = [];
    cart.patient.dateOfBirth = null;
    cart.patient.name = null;
    cart.patient.nic = null;

    cart.appointment.response.appointmentCharges = [];
    cart.appointment.response.appointmentNo = 0;
    cart.appointment.response.appointmentTime = null;
    cart.appointment.response.refNo = 0;
    cart.appointment.response.totalCharge = 0;
    cart.step = StaticConfig.appoinmentBookingStages.INIT;

    this.storage.set('bookingEChannelCart_pk', cart);

    return this.getBookingCart();
  }


  public updateBookingReqData(bookingObj: any) {

      const cart = this.getBookingCart();
      cart.appointment.request.appointmentCharges = bookingObj.appointmentCharges || [];
      cart.appointment.request.patient.dateOfBirth = bookingObj.patient.dateOfBirth || null;
      cart.appointment.request.patient.name = bookingObj.patient.patientName || null;
      cart.appointment.request.patient.mobileNo = bookingObj.patient.mobileNumber || null;
      cart.appointment.request.patient.nic = bookingObj.patient.nic || null;
      cart.appointment.request.refNo = bookingObj.refNo || 0;

      this.storage.set('bookingEChannelCart_pk', cart);
  }

  public updateAppointmentResponse(response) {
      // console.log(response);
      return new Promise((resolve, reject) => {
              const cart = this.getBookingCart();
              cart.appointment.response = response;
              if (response.refNo) {
                  cart.appointment.refNo = response.refNo;
              }
              this.storage.set('bookingEChannelCart_pk', cart);
              resolve();
          });
  }

  public updateAppontmentRefNo(refNo) {

      const cart = this.getBookingCart();
      cart.appointment.refNo = refNo;
      this.storage.set('bookingEChannelCart_pk', cart);
  }

  public setBookingStep(step) {
      const cart = this.getBookingCart();
      cart.step = step;
      this.storage.set('bookingEChannelCart_pk', cart);
  }

}
