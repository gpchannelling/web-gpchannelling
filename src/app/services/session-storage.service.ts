import {Injectable} from '@angular/core';
import {GlobalVariable} from '../core/com-classes';
import {LocalStorageService} from '../core/services';

@Injectable({
    providedIn: 'root'
})
export class SessionStorageService {

    constructor(private gVarilable: GlobalVariable, private storageSev: LocalStorageService) {

    }

    public createSessionSelectedCenter(center: any) {
        this.removeCreateSessionSelectedCenter();
        this.storageSev.set('createSessionSelectedCenter_pk', center);
    }

    public getCreateSessionSelectedCenter() {
        return this.storageSev.get('createSessionSelectedCenter_pk');
    }

    public removeCreateSessionSelectedCenter() {
        const selectedCenter = this.storageSev.get('createSessionSelectedCenter_pk');
        if (selectedCenter) {
            this.storageSev.remove('createSessionSelectedCenter_pk');
        }
    }

    public createSessionSelectedDoctor(doctor: any) {
        this.removeCreateSessionSelectedDoctor();
        this.storageSev.set('createSessionSelectedDoctor_pk', doctor);
    }

    public getCreateSessionSelectedDoctor() {
        return this.storageSev.get('createSessionSelectedDoctor_pk');
    }

    public removeCreateSessionSelectedDoctor() {
        const selectedDoctor = this.storageSev.get('createSessionSelectedDoctor_pk');
        if (selectedDoctor) {
            this.storageSev.remove('createSessionSelectedDoctor_pk');
        }
    }

    public createSessionSelectedDate(date: any) {
        this.removeCreateSessionSelectedDate();
        this.storageSev.set('createSessionSelectedDate_pk', date);
    }

    public getCreateSessionSelectedDate() {
        return this.storageSev.get('createSessionSelectedDate_pk');
    }

    public removeCreateSessionSelectedDate() {
        const selectedDoctor = this.storageSev.get('createSessionSelectedDate_pk');
        if (selectedDoctor) {
            this.storageSev.remove('createSessionSelectedDate_pk');
        }
    }

    public setSelectedSearchObj(searchObj: any) {
        this.removeSelectedSearchObj();
        this.storageSev.set('selectedSearchObj', searchObj);
    }

    public getSelectedSearchObj() {
        return this.storageSev.get('selectedSearchObj');
    }

    public removeSelectedSearchObj() {
        const selectedDoctor = this.storageSev.get('selectedSearchObj');
        if (selectedDoctor) {
            this.storageSev.remove('selectedSearchObj');
        }
    }


    public setSessionID(sid: any) {
        this.storageSev.set('SID', sid);
    }

    public getSessionID() {
        return this.storageSev.get('SID');
    }

    public removeSessionID() {
        const selectedDoctor = this.storageSev.get('SID');
        if (selectedDoctor) {
            this.storageSev.remove('SID');
        }
    }


    public setCreatedAppointmentKey(appointment: any) {
        this.removeCreatedAppointmentKey();
        this.storageSev.set('createdAppointment_pk', appointment);
    }

    public getCreatedAppointmentKey() {
        return this.storageSev.get('createdAppointment_pk');
    }

    public removeCreatedAppointmentKey() {
        const createdAppointment = this.storageSev.get('createdAppointment_pk');
        if (createdAppointment) {
            this.storageSev.remove('createdAppointment_pk');
        }
    }

}
