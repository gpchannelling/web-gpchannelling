import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';

@Injectable()

export class GeolocationService {


    public getUserLocation(): Observable<any> {
        return Observable.create( observer => {
            if (window.navigator && window.navigator.geolocation) {
                window.navigator.geolocation.getCurrentPosition(
                    (position) => {
                        observer.next(position);
                        observer.complete();
                    }, (error) => observer.error(error)
                );
            } else {
                observer.error('Unsuppoted Browser');
            }
        });
    }


    public getDistance(distanceMatrix, origin, destination): Observable<any> {
        return Observable.create( observer => {
            if (distanceMatrix && origin && destination) {
                distanceMatrix.getDistanceMatrix({
                        origins: [origin],
                        destinations: [destination],
                        travelMode: 'DRIVING',
                        avoidTolls: true,
                    }, (response) => {
                        observer.next(response);
                        observer.complete();
                    }
                );
            } else {
                observer.error('Unsuppoted Browser');
            }
        });
    }


}
