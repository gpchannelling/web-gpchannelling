import { Injectable } from '@angular/core';
import { HttpService, AppConfig, ApiServiceConfig, GlobalVariable } from '../core/index';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '../../../node_modules/@angular/common/http';
import {GuardService} from './guard.service';
import {StaticConfig} from '../core/config';
import {BehaviorSubject} from 'rxjs/index';

@Injectable()

export class AuthenticationService {
    public loginStatusChange = new BehaviorSubject<any>(null);
    private staticConfig: any = StaticConfig;
    private appConfig: any = AppConfig;

    constructor(private httpClient: HttpClient, private gVariable: GlobalVariable, private guardSev: GuardService) {

    }

    public setEntitlements(response) {
        if (this.gVariable.platformBrowser) {
            const availableEntitlements = [];
            const userRoles = response.roles;

            // add public entitlements
            for (let q in this.staticConfig.entitlementsList) {
                if (this.staticConfig.entitlementsList[q].accessType === 'PUBLIC') {
                    availableEntitlements.push(this.staticConfig.entitlementsList[q]);
                }

            }

            // add private entitlements
            for(let i in userRoles) {

                const privileges = userRoles[i].privileges;

                for (let j in privileges) {

                    for (let q in this.staticConfig.entitlementsList) {

                        if (privileges[j].privilegeCode === this.staticConfig.entitlementsList[q].id) {
                            availableEntitlements.push(this.staticConfig.entitlementsList[q]);
                        }
                    }
                }
            }

            this.guardSev.setEntitlements(availableEntitlements);
        }
    }

    public checkEntitlementAvailability(entitlementArray: any) {
        if (this.gVariable.platformBrowser) {
            const entitlements = this.guardSev.getEntitlements();

            // console.log(entitlements);
            // console.log(entitlementArray);
            if ((Object.keys(entitlements).length > 0) && (entitlementArray.length > 0)) {
                for ( let i in entitlements) {
                    // console.log(entitlements[i]);
                    for (let j in entitlementArray) {
                        // console.log(entitlementArray[j]);
                        if ((entitlementArray[j] === entitlements[i].id)) {
                            return true;
                        }
                    }
                }

                return false;
            }
            // console.log(entitlementArray);
        }
    }

    public UpdateLoginStatus(value) {
        this.loginStatusChange.next(value);
    }

}
