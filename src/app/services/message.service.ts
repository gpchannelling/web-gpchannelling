import {Inject, inject, Injectable, InjectionToken} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBar, MatSnackBarConfig} from '@angular/material';
import {CustomMatSnakBarComponent} from '../views/utilities/custom-mat-snak-bar/custom-mat-snak-bar.component';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

    // public MAT_SNACK_BAR_DATA: InjectionToken<any>;

  constructor(private matSnakBar: MatSnackBar) { }

    public mesageInfo(message) {
        this.closeMessage();
        const config = new MatSnackBarConfig();
        config.panelClass = ['btn-info', 'toastMessage'];
        config.duration = 2000;
        config.verticalPosition = 'top';
        config.horizontalPosition = 'right';
        this.matSnakBar.open(message, undefined, config);
    }

    public mesageSuccess(message) {
      this.closeMessage();
      const config = new MatSnackBarConfig();
      config.panelClass = ['btn-success', 'toastMessage'];
      config.duration = 2000;
      config.verticalPosition = 'top';
      config.horizontalPosition = 'right';
      this.matSnakBar.open(message, undefined, config);
    }

    public mesageWarning(message) {
        this.closeMessage();
        const config = new MatSnackBarConfig();
        config.panelClass = ['label-warning', 'toastMessage'];
        config.duration = 3000;
        config.verticalPosition = 'top';
        config.horizontalPosition = 'right';
        this.matSnakBar.open(message, undefined, config);
    }

    public mesageError(message) {
        this.closeMessage();
        const config = new MatSnackBarConfig();
        config.panelClass = ['btn-danger', 'toastMessage'];
        config.duration = 3000;
        config.verticalPosition = 'top';
        config.horizontalPosition = 'right';
        this.matSnakBar.open(message, undefined, config);
    }

    public messageCustom(message) {
        this.closeMessage();
        this.matSnakBar.openFromComponent(CustomMatSnakBarComponent, {
            panelClass : ['btn-danger', 'toastMessage'],
            duration : 10000,
            verticalPosition : 'top',
            horizontalPosition : 'right',
            data: message
        });
    }
    
    public closeMessage() {
      this.matSnakBar.dismiss();
    }
}
