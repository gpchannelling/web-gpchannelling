import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import {GlobalVariable} from '../core/com-classes';
import {LocalStorageService} from '../core/services';

@Injectable()
export class GeoStorageService {
    constructor(private gVariable: GlobalVariable, private localStorage: LocalStorageService) {}

    // for the selected centers
    public setSelectedCenter(center: any) {
        const selectedCenter = this.localStorage.get('selectedCenter_pk');
        if (selectedCenter) {
            this.localStorage.remove('selectedCenter_pk');
        }
        this.localStorage.set('selectedCenter_pk', center);
        this.gVariable.selectedCenter = center;
    }

    public getSelectedCenter() {
        const selectedCenter = this.localStorage.get('selectedCenter_pk');
        return selectedCenter;
    }



    // for the selected doctor in search doctor
    public setSelectedSearchDoctor(doctor: any) {
        const selectedDoctor = this.localStorage.get('selectedSearchDoctor_pk');
        if (selectedDoctor) {
            this.localStorage.remove('selectedSearchDoctor_pk');
        }
        this.localStorage.set('selectedSearchDoctor_pk', doctor);
        this.gVariable.selectedSearchedDoctor = doctor;
    }

    public getSelectedSearchDoctor() {
        const selectedDoctor = this.localStorage.get('selectedSearchDoctor_pk');
        return selectedDoctor;
    }



    // for the selected map point
    public setSelectedMapPoint(point: any) {
        const selectedMapPoint = this.localStorage.get('selectedMapPoint_pk');
        if (selectedMapPoint) {
            this.localStorage.remove('selectedMapPoint_pk');
        }

        this.localStorage.set('selectedMapPoint_pk', point);
        this.gVariable.selectedMapPoint = point;
    }

    public getSelectedMapPoint() {
        const selectedMapPoint = this.localStorage.get('selectedMapPoint_pk');
        return selectedMapPoint;
    }

    // for the selected map zoom level
    public setSelectedMapZoomLevel(zoomLevel) {
        const selectedZoomLevel = this.localStorage.get('selectedMapZoomLevel_pk');
        if (selectedZoomLevel) {
            this.localStorage.remove('selectedMapZoomLevel_pk');
        }

        this.localStorage.set('selectedMapZoomLevel_pk', zoomLevel);
    }

    public getSelectedMapZoomLevel() {
        const selectedZoomLevel = this.localStorage.get('selectedMapZoomLevel_pk');
        return selectedZoomLevel;
    }

    // for the check the previouse path in appointent
    public setPreviousPath(path: any) {
        const previousPath = this.localStorage.get('previousPath_pk');
        if (previousPath) {
            this.localStorage.remove('previousPath_pk');
        }
        this.localStorage.set('previousPath_pk', path);
    }

    public getPreviousPath() {
        const previousPath = this.localStorage.get('previousPath_pk');
        return previousPath;
    }

}
