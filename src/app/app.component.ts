import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { GlobalVariable, ExternalJs, LocalStorageService } from './core';
import {ClientService, GuardService} from './services';
import {Meta, Title} from '@angular/platform-browser';
import {StaticConfig} from './core/config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  private subscribe: any;
  private queryParams: any = {};
  public loaderComplete = false;
  constructor(private translate: TranslateService,
              private guardSev: GuardService,
              private clientSev: ClientService,
              private acRoute: ActivatedRoute,
              private eJs: ExternalJs,
              private gVariable: GlobalVariable,
              private storage: LocalStorageService,
              private router: Router,
              private meta: Meta) {
    if (this.gVariable.platformBrowser) {
      translate.addLangs(['en']);
      translate.setDefaultLang('en');
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/en/) ? browserLang : 'en');
      // translate.addLangs(['en', 'si']);
      // translate.use(browserLang.match(/en|si/) ? browserLang : 'en');
      // this.translate.use("si");

      this.router.events
        .subscribe((event) => {
          if (event instanceof NavigationEnd) {
            this.initNavigationEnd();
          }
        });
    }

    this.meta.addTags([
        {name: 'title', content: StaticConfig.pageTitle},
        {name: 'url', content: StaticConfig.webUrl},
        {name: 'description', content: 'hybrid app for online medical channels booking'},
        {name: 'keywords', content: 'Online channelling, e-channelling, gp-channelling'},
        {name: 'image', content: StaticConfig.siteImage},
        {name: 'author', content: StaticConfig.author},
        {property: 'og:title', content: StaticConfig.pageTitle},
        {property: 'og:url', content: StaticConfig.webUrl},
        {property: 'og:description', content: 'hybrid app for online medical channels booking'},
        {property: 'og:keywords', content: 'Online channelling, e-channelling, gp-channelling'},
        {property: 'og:image', content: StaticConfig.siteImage},
        {property: 'og:author', content: StaticConfig.author}
    ]);
  }

  ngOnInit(): void {
    if (this.gVariable.platformBrowser) {
      this.subscribe = this.acRoute.queryParams.subscribe(
        params => {
          this.queryParams = params || {};
          this.activeUserLogin();
        });
    }
  }

  ngOnDestroy() {
    if (this.gVariable.platformBrowser) {
      this.subscribe.unsubscribe();
    }
  }

  private initNavigationEnd() {
    if (this.gVariable.platformBrowser) {
      this.eJs.initJs();
      this.activateUser();
    }
  }

  public activateUser() {
      this.gVariable.authentication = this.guardSev.getAuthentication() || {};
    // this.gVariable.authentication = this.storage.get('authentication_pk') || {};
  }

  /*
  changeLang() {
     this.translate.use('en');
     this.translate.get('Home').subscribe((res: string) => {
        console.log(res)
     });
  }
  */

  private activeUserLogin() {
    // console.log(this.queryParams);
    if (this.queryParams.token) {
      this.router.navigate(['', 'reset-forgotten-password'], {'queryParams': {token: this.queryParams.token}});
      // this.clientSev.confirmEmail(Object.assign({}, this.queryParams))
      //   .then((data: any) => {
      //     // console.log(data);
      //     this.gService.createAuthentication(data);
      //   }).catch((error: any) => {
      //   console.log(error.errorMessage);
      // });
    }
  }

}
