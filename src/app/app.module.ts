import {NgModule, Optional} from '@angular/core';
import {BrowserModule, BrowserTransferStateModule, makeStateKey, TransferState} from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser, DatePipe } from '@angular/common';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { StorageServiceModule } from 'angular-webstorage-service';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { FullLayoutComponent, SimpleLayoutComponent } from './containers';

export function HttpLoaderFactory(http: HttpClient) {
  // for development
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

// Import core
import { HttpService, GlobalVariable, ExternalJs, LocalStorageService } from './core';

// Import sev
import {BookingCartService, ClientService, GeoStorageService, GuardService, MapHashingService, SessionStorageService} from './services';
import { GeolocationService } from './services/geolocation.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthenticationService} from './services/authentication.service';
import { MessagesService } from './services/message.service';
import { MessageService } from 'primeng/api';
import {AccordionModule, CalendarModule, MessagesModule} from 'primeng/primeng';
import {AgmDirectionModule} from 'agm-direction';
import {ToastModule} from 'primeng/toast';
import {ButtonModule} from 'primeng/button';
import {SliderModule} from 'primeng/slider';
import {MessageModule} from 'primeng/message';
import {DialogModule} from 'primeng/dialog';
import {SidebarComponent} from './containers/sidebar/sidebar.component';
import {NavbarComponent} from './containers/navbar/navbar.component';
import {FooterComponent} from './containers/footer/footer.component';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCheckbox,
    MatCheckboxModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRippleModule,
    MatSelectModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTabsModule,
    MatTooltipModule
} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {AppointmentsFilter} from './pipes/AppointmentsFilter.pipe';
import {CustomMatSnakBarComponent} from './views/utilities/custom-mat-snak-bar/custom-mat-snak-bar.component';

const WEB_APP_CONFIG_KEY = makeStateKey<string>('WEB_APP_CONFIG_KEY');

@NgModule({
  imports: [
    BrowserModule.withServerTransition({ appId: 'web-client' }),
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserTransferStateModule,
    StorageServiceModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,
      ButtonModule,
      SliderModule,
      AgmDirectionModule,
      MessagesModule,
      MessageModule,
      CalendarModule,
      ToastModule,
      DialogModule,
      AccordionModule,
      MatFormFieldModule,
      MatButtonModule,
      MatRippleModule,
      MatInputModule,
      MatSelectModule,
      MatTooltipModule,
      MatDatepickerModule,
      MatSnackBarModule,
      MatDialogModule,
      NgxMaterialTimepickerModule,
      MatExpansionModule,
      MatListModule,
      MatTabsModule,
      MatStepperModule,
      MatIconModule,
      MatProgressBarModule,
      MatProgressSpinnerModule,
      MatCheckboxModule,
      MatSnackBarModule,
      MatAutocompleteModule

  ],
  declarations: [
    AppComponent,
    FullLayoutComponent, SimpleLayoutComponent,
      SidebarComponent,
      NavbarComponent,
      FooterComponent,
      CustomMatSnakBarComponent
  ],
  providers: [
      HttpService,
      GlobalVariable,
      ExternalJs,
      ClientService,
      BookingCartService,
      DatePipe,
      GuardService,
      LocalStorageService,
      GeolocationService,
      AuthenticationService,
      MessageService,
      GeoStorageService,
      MessagesService,
      SessionStorageService,
      MapHashingService
  ],
  bootstrap: [ AppComponent],
  entryComponents: [CustomMatSnakBarComponent]
})
export class AppModule {
  constructor(
    @Optional() @Inject('__WEB_APP_CONFIG__') protected __WEB_APP_CONFIG__: any,
    @Inject(PLATFORM_ID) platformId: Object,
    @Inject(APP_ID) appId: string,
    private tState: TransferState,
    private gVariable: GlobalVariable,
    private httpSev: HttpService) {
    this.gVariable.platformBrowser = isPlatformBrowser(platformId);
    const platform = this.gVariable.platformBrowser ? 'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId = ${appId}`);

    if (!this.gVariable.platformBrowser) { // we are in server
        this.gVariable.webAppConfig = __WEB_APP_CONFIG__;
        this.tState.set(WEB_APP_CONFIG_KEY, __WEB_APP_CONFIG__);
    } else {
        if (this.tState.hasKey(WEB_APP_CONFIG_KEY)) { // we are in browser
            const WEB_APP_CONFIG = this.tState.get(WEB_APP_CONFIG_KEY, null);
            this.tState.remove(WEB_APP_CONFIG);
            this.gVariable.webAppConfig = WEB_APP_CONFIG || {};
        } else { // in development mode
            this.loadConfig().then( (response: any) => {
                this.gVariable.webAppConfig = response;
            }).catch( (error: any) => {
                console.log(error);
            });
        }
    }
  }

  private loadConfig() {
      const jsonFile = '/assets/js/webapp.config.json';
      return new Promise( (resolve, reject) => {
          this.httpSev.httpGetLocalJson(jsonFile).then( (data: any) => {
              // console.log(data);
              if (data) {
                  resolve(data);
              } else {
                  reject({error: 'no content 204'});
              }
          }).catch((error: any) => {
              reject(error);
          });
      });
  }
}
