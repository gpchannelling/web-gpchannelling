import {Component, OnInit, AfterContentInit, AfterViewInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, NavigationStart } from '@angular/router';
import { TransferState, makeStateKey, DomSanitizer } from '@angular/platform-browser';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import {ClientService, GuardService} from '../../services';
import { GlobalVariable, StaticConfig, ExternalJs } from '../../core';
import { AppConfig } from '../../core/config';
import {AuthenticationService} from '../../services/authentication.service';
import PerfectScrollbar from 'perfect-scrollbar';
import {Subscription} from 'rxjs/index';
import { filter } from 'rxjs/operators';
import {createCustomElement} from '@angular/elements';

declare var $: any;
declare var HTMLElement: any;

@Component({
  selector: 'app-index',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit, AfterContentInit, AfterViewInit {

  private _router: Subscription;
  private lastPoppedUrl: string;
  private yScrollStack: number[] = [];
  @ViewChild('.main-panel') elemMainPanel: any;
  @ViewChild('.sidebar .sidebar-wrapper') elemSidebar: any;

  public appConfig: any = AppConfig;
  public staticConfig: any = StaticConfig;
  public copyrightText = StaticConfig.copyrightText;
  public copyrightYear = StaticConfig.copyrightYear;
  public webUrl = StaticConfig.webUrl;
  public entitlements: any[] = ['P001', 'P002', 'P003', 'P005'];

  constructor(private router: Router,
              private activeRouter: ActivatedRoute,
              private clientSev: ClientService,
              private tState: TransferState,
              public gVariable: GlobalVariable,
              private gService: GuardService,
              private sanitizer: DomSanitizer,
              private eJs: ExternalJs,
              public authSev: AuthenticationService,
              public location: Location) {
  }

  ngOnInit() {

    if (this.gVariable.platformBrowser) {
      // No result received
      // this.initMenu();
        const isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
      //
        if (isWindows && !document.getElementsByTagName('body')[0].classList.contains('sidebar-mini')) {
            // if we are on windows OS we activate the perfectScrollbar function

            document.getElementsByTagName('body')[0].classList.add('perfect-scrollbar-on');
        } else {
            document.getElementsByTagName('body')[0].classList.remove('perfect-scrollbar-off');
        }
        const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
        const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');

        this.location.subscribe((ev: any) => {
            this.lastPoppedUrl = ev.url;
        });
        this.router.events.subscribe((event: any) => {
            if (event instanceof NavigationStart) {
                if (event.url !== this.lastPoppedUrl) { this.yScrollStack.push(window.scrollY); }
            } else if (event instanceof NavigationEnd) {
                if (event.url === this.lastPoppedUrl) {
                    this.lastPoppedUrl = undefined;
                    window.scrollTo(0, this.yScrollStack.pop());
                } else { window.scrollTo(0, 0); }

            }
        });
        // this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
        //     elemMainPanel.scrollTop = 0;
        //     elemSidebar.scrollTop = 0;
        // });
        this._router = this.router.events.pipe(
            filter(event => event instanceof NavigationEnd)
        ).subscribe(() => {
            elemMainPanel.scrollTop = 0;
            elemSidebar.scrollTop = 0;
        });

        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            // let ps = new PerfectScrollbar(this.elemMainPanel);
            // ps = new PerfectScrollbar(this.elemSidebar);
        }
    }



  }

  ngAfterContentInit() {
      if (this.gVariable.platformBrowser) {
          this.eJs.initJs();
      }
  }

  ngAfterViewInit() {
      this.runOnRouteChange();
  }

  private initMenu() {
      // $('.collapse').collapse('toggle');

      // $('.collapse').collapse('show');
  }

  public closeMenu() {
      $('.collapse').collapse('hide');
  }

  onClickLogOut() {
      this.gService.removeAuthentication();
      this.redirectToHome();
  }

  private redirectToHome() {
      this.router.navigate(['']);
  }

  public closeOverlay() {
      if (this.gVariable.platformBrowser) {
          this.eJs.closeOverlay();
          $('.collapse').collapse('hide');
      }

  }


  isMaps(path) {
      if (this.gVariable.platformBrowser) {
          let titlee = this.location.prepareExternalUrl(this.location.path());
          titlee = titlee.slice(1);
          if (path === titlee) {
              return false;
          } else {
              return true;
          }
      }
  }

  runOnRouteChange(): void {
      if (this.gVariable.platformBrowser) {
          if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
              const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
              // const ps = new PerfectScrollbar(this.elemMainPanel);
              // ps.update();
          }
      }

  }

  isMac(): boolean {
      if (this.gVariable.platformBrowser) {
          let bool = false;
          if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
              bool = true;
          }
          return bool;
      }
  }
















}
