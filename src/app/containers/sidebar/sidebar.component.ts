import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {GlobalVariable} from '../../core';
import {AuthenticationService, GuardService} from '../../services';
import {StaticConfig} from '../../core/config';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    click: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/my-dashboard', title: 'Profile',  icon: 'person', class: '', click: ''},
    { path: '/session-list', title: 'Session List',  icon: 'library_books', class: '', click: ''},
    { path: '/report/revenue', title: 'Revenue Report', icon: 'content_paste', class: '', click: ''}
];

export const AUTHORIZESROUTES: RouteInfo[] = [
    { path: '/my-dashboard', title: 'Profile',  icon: 'person', class: '', click: ''},
    { path: '/session-list', title: 'Session List',  icon: 'library_books', class: '', click: ''},
    { path: '/report/revenue', title: 'Revenue Report', icon: 'content_paste', class: '', click: ''},
    { path: '/user-register', title: 'User Registration', icon: 'add_circle', class: '', click: ''},
    { path: '/', title: 'Logout', icon: 'bubble_chart', class: 'btn-logout', click: 'onClickLogOut'},
];

export const COMMONROUTES: RouteInfo[] = [
    {path: '/', title: 'Home', icon: 'dashboard', class: '', click: ''}
];


export const UNAUTHORIZESROUTES: RouteInfo[] = [
    {path: '/login', title: 'Login', icon: 'bubble_chart', class: '', click: ''},
    {path: '/self-registration', title: 'Self-register', icon: 'add_circle', class: '', click: ''}
];



@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  public staticConfig: any = StaticConfig;

  constructor(private gService: GuardService, private router: Router, public gVariable: GlobalVariable, private authSev: AuthenticationService) {
      this.getMenu();
      authSev.loginStatusChange.subscribe(() => {
          this.getMenu();
      });
  }

  ngOnInit() {
    this.getMenu();
  }

  public getMenu() {
      if (this.gVariable.platformBrowser) {
          this.menuItems = [];
          if (this.gVariable.authentication.authorized) {
              this.menuItems = [...AUTHORIZESROUTES.filter(menuItem => menuItem)];
          } else {
              this.menuItems = [...COMMONROUTES.filter(menuItem => menuItem), ...UNAUTHORIZESROUTES.filter(menuItem => menuItem)];
          }

          // console.log(this.menuItems);
          // this.menuItems = ROUTES.filter(menuItem => menuItem);
      }
  }

  isMobileMenu() {
      if (this.gVariable.platformBrowser) {
          if ($(window).width() > 991) {
              return false;
          }
          return true;
      }

  }

  onClickLogOut() {
    this.gService.removeAuthentication();
      this.getMenu();
    this.redirectToHome();
  }

  private redirectToHome() {
      this.getMenu();
      this.router.navigate(['']);
  }

  onclickSetEventFunction(functionName) {
      if (this[functionName]()) {
          this[functionName]();
      }
  }

}
