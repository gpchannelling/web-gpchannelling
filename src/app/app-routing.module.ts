import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';

const routes: Routes = [
  { path: '', component: FullLayoutComponent, loadChildren: './views/views.module#ViewsModule' },
  // { path: 'login', component: SimpleLayoutComponent, loadChildren: './views/views.module#ViewsModule'},
  { path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes,{useHash: true}) ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
