declare var $: any;

export class ExternalJs {

  public initJs() {
    const component = this;

    $(window).on('resize load', function() {
      if (window.matchMedia('(max-width: 991px)').matches) {
        $('.customOrder .col-md-4').appendTo('.customOrder');
      } else {
        $('.customOrder .col-xs-12').appendTo('.customOrder');
      }
    });

      // $('.collapse').collapse('toggle');

      $('.collapse').collapse('hide');




  }

  public closeOverlay() {
      $(document.body).addClass('ui-overflow-hidden');
      // $('.ui-widget-overlay').removeClass('ui-widget-overlay');
      $('.ui-widget-overlay').remove();
  }

  public onClickMarker() {
    // $('.si-float-wrapper').css('background-image', 'red !important');
  }

  public showSideBar() {
      $('.collapse').collapse('show');
  }

  public hideSideBar() {
      $('.collapse').collapse('hide');
  }

  public closeLoading() {
      $('.loader-wrapper').css('display', 'none');
  }


}
