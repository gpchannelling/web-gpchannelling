export class GlobalVariable {

  private _platformBrowser: boolean;
  private _webAppConfig: any = {};
  private _authentication: any = {};
  private _selectedCenter: any = {};
  private _selectedMapPoint: any = {};
  private _entitlements: any = [];
  public _selectedSearchedDoctor: any = [];


  // for the authentication
  get authentication(): any {
    return this._authentication;
  }
  set authentication(value: any) {
    this._authentication = value;
  }


  // for the browser validation
  get platformBrowser(): boolean {
    return this._platformBrowser;
  }

  set platformBrowser(value: boolean) {
    this._platformBrowser = value;
  }

    get webAppConfig(): any {
        return this._webAppConfig;
    }

    set webAppConfig(value: any) {
        this._webAppConfig = value;
    }


  // for the center
  get selectedCenter(): any {
    return this._selectedCenter;
  }

  set selectedCenter(value: any) {
    this._selectedCenter = value;
  }

    // for the center
    get selectedSearchedDoctor(): any {
        return this._selectedSearchedDoctor;
    }

    set selectedSearchedDoctor(value: any) {
        this._selectedSearchedDoctor = value;
    }


  // for the map point
  get selectedMapPoint(): any {
    return this._selectedMapPoint;
  }

  set selectedMapPoint(value: any) {
    this._selectedMapPoint = value;
  }


  get entitlements(): any {
    return this._entitlements;
  }

  set entitlements(value: any) {
    this._entitlements = value;
  }


}
