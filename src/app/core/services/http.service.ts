import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { GlobalVariable } from '../com-classes';
import { stringify } from 'querystring';

@Injectable()
export class HttpService {

  constructor(private httpClient: HttpClient, private gVariable: GlobalVariable) { }

  private static setHeader (header: any, headerValue: any) {
    if (headerValue) {
      for (const key in headerValue) {
        header = header.append(key, headerValue[key]);
      }
    }
    return header;
  }

  private static httpErrorHandler(error) {
    switch (error.status) {
      case 304:
        const eTag = error.headers.get('Etag');
        if (eTag) {
          error.errorMessage = eTag.replace(/"/g, '');
        }
        break;
      case 401:
        error.errorMessage = 'User not authorized';
        break;
      case 500:
        error.errorMessage = 'System Error';
        break;
      default:
        break;
    }
    // console.log(error);
    return error;
  }

  public httpGet(apiUrl: any, sevConfig: any, path: string, body: any, headerValue: any) {
    return new Promise((resolve, reject) => {
        let header = new HttpHeaders();
        header = header.append('Content-Type', 'application/json');
        header = header.append('Accept', '*/*');

        if (this.gVariable.authentication && this.gVariable.authentication.authorized) {
            header = header.append('authorization' , 'Token ' + this.gVariable.authentication.token);
        }

        const httpHeaders = HttpService.setHeader(header, headerValue);

        let apiPath = null;
        if (this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
            apiPath = this.gVariable.webAppConfig.API_URL.PUBLIC;
        }

        if (!this.gVariable.platformBrowser && this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
            apiPath = this.gVariable.webAppConfig.API_URL.PRIVATE;
        }

        // const url = apiPath + sevConfig.ROUTE_PATH + path;
        // const url = 'http://localhost:8080/gpc-service'  + path;
        const url = 'https://172.26.144.34:8443/gpc-service'  + path;
        console.log(url);
        return this.httpClient.get(url, { headers: httpHeaders} )
          .toPromise()
          .then(response => {
            resolve(response);
          })
          .catch(error => {
            this.log(true, 'GET', url, body, error);
            reject(HttpService.httpErrorHandler(error));
          });
    });
  }

  public httpPost(apiUrl: any, sevConfig: any, path: string, body: any, headerValue: any) {
    return new Promise((resolve, reject) => {
      const req_body: string = JSON.stringify(body);
      let header: HttpHeaders;
      header = new HttpHeaders().append('Content-Type', 'application/json');
      header = header.append('Accept', '*/*');

      if (this.gVariable.authentication && this.gVariable.authentication.authorized) {
        header = header.append('Authorization' , 'Token ' + this.gVariable.authentication.token);
      }

      const httpHeaders = HttpService.setHeader(header, headerValue);

        let apiPath = null;
        if (this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
            apiPath = this.gVariable.webAppConfig.API_URL.PUBLIC;
        }

        if (!this.gVariable.platformBrowser && this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
            apiPath = this.gVariable.webAppConfig.API_URL.PRIVATE;
        }

      // const url = apiPath + sevConfig.ROUTE_PATH + path;
        // const url = 'http://localhost:8080/gpc-service'  + path;
      const url = 'https://172.26.144.34:8443/gpc-service'  + path;
        console.log(this.gVariable.authentication.token);
      return this.httpClient.request('POST', url, {body: req_body, headers: httpHeaders, observe: 'response'})
        .toPromise()
        .then(response => {
          const responseData: any = [];
          responseData.data = response.body;
          responseData.status = response.status;

          resolve(responseData);
        })
        .catch(error => {
          this.log(true, 'POST', url, body, error);
          reject(HttpService.httpErrorHandler(error));
        });
    });
  }

  public httpPut(apiUrl: any, sevConfig: any, path: string, body: any, headerValue: any) {
    return new Promise((resolve, reject) => {
      const req_body = JSON.stringify(body);
      let header = new HttpHeaders().set('Content-Type', 'application/json');
      header = header.append('Accept', '*/*');

      if (this.gVariable.authentication && this.gVariable.authentication.authorized) {
          header = header.append('authorization' , 'Token ' + this.gVariable.authentication.token);
      }

      const httpHeaders = HttpService.setHeader(header, headerValue);

        let apiPath = null;
        if (this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
            apiPath = this.gVariable.webAppConfig.API_URL.PUBLIC;
        }

      if (!this.gVariable.platformBrowser && this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
        apiPath = this.gVariable.webAppConfig.API_URL.PRIVATE;
      }

      // const url = apiPath + sevConfig.ROUTE_PATH + path;
        const url = 'https://172.26.144.34:8443/gpc-service'  + path;
        // const url = 'http://localhost:8080/gpc-service'  + path;
        return this.httpClient.request('PUT', url, {body: req_body, headers: httpHeaders, observe: 'response'})
        .toPromise()
        .then(response => {

            const responseData: any = [];
            responseData.data = response.body;
            responseData.status = response.status;

          resolve(responseData);
        })
        .catch(error => {
          this.log(true, 'PUT', url, body, error);
          reject(HttpService.httpErrorHandler(error));
        });
    });
  }

    public httpFileUploadPost(apiUrl: any, sevConfig: any, path: string, body: any, headerValue: any) {
        return new Promise((resolve, reject) => {
            let header: HttpHeaders;
            header = new HttpHeaders();
            // header = new HttpHeaders().append('Content-Type', 'multipart/form-data');
            // header = header.append('Accept', '*/*');
            if (this.gVariable.authentication && this.gVariable.authentication.authorized) {
                header = header.append('Authorization' , 'Token ' + this.gVariable.authentication.token);
                console.log(this.gVariable.authentication.token);
            }

            const httpHeaders = HttpService.setHeader(header, headerValue);

            let apiPath = null;
            if (this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
                apiPath = this.gVariable.webAppConfig.API_URL.PUBLIC;
            }

            if (!this.gVariable.platformBrowser && this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
                apiPath = this.gVariable.webAppConfig.API_URL.PRIVATE;
            }

            // const url = apiPath + sevConfig.ROUTE_PATH + path;
            // const url = 'http://localhost:8080/gpc-service'  + path;
            const url = 'https://172.26.144.34:8443/gpc-service'  + path;
            console.log(url);
            const formData: FormData = new FormData();

            for (const key in body) {
                formData.append(key, body[key]);
            }

            return this.httpClient.request('POST', url, {body: formData, headers: httpHeaders, observe: 'response'})
                .toPromise()
                .then(response => {
                    const responseData: any = [];
                    responseData.data = response.body;
                    responseData.status = response.status;

                    resolve(responseData);
                })
                .catch(error => {
                    this.log(true, 'POST', url, body, error);
                    reject(HttpService.httpErrorHandler(error));
                });
        });
    }

    public httpEchPost(apiUrl: any, sevConfig: any, path: string, body: any, headerValue: any) {
        return new Promise((resolve, reject) => {
            const req_body: string = JSON.stringify(body);
            let header: HttpHeaders;
            header = new HttpHeaders().append('Content-Type', 'application/json');
            header = header.append('Accept', '*/*');

            if (this.gVariable.authentication && this.gVariable.authentication.authorized) {
                header = header.append('Authorization' , 'Token ' + this.gVariable.authentication.token);
            }

            const httpHeaders = HttpService.setHeader(header, headerValue);

            let apiPath = null;
            if (this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
                apiPath = this.gVariable.webAppConfig.API_URL.PUBLIC;
            }

            if (!this.gVariable.platformBrowser && this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
                apiPath = this.gVariable.webAppConfig.API_URL.PRIVATE;
            }

            const url = 'https://172.26.144.34:8443'  + path;
            console.log(this.gVariable.authentication.token);
            return this.httpClient.request('POST', url, {body: req_body, headers: httpHeaders, observe: 'response'})
                .toPromise()
                .then(response => {
                    const responseData: any = [];
                    responseData.data = response.body;
                    responseData.status = response.status;

                    resolve(responseData);
                })
                .catch(error => {
                    this.log(true, 'POST', url, body, error);
                    reject(HttpService.httpErrorHandler(error));
                });
        });
    }

    public httpGetLocalJson(path: string) {
        return new Promise( (resolve, reject) => {
            const header = new HttpHeaders().set('Content-Type', 'application/json');
            const httpHeaders = HttpService.setHeader(header, {});
            const url = path;
            return this.httpClient.get(url, {headers: httpHeaders})
                .toPromise()
                .then( (response: any) => {
                    resolve(response);
                });
        });
    }

  // private handleError (error: any) {
  //   return Observable.throw(error);
  // }

  // private getEventMessage (error: any) {
  //   console.log(error);
  // }

  private log(isError, method, url, req, res) {
    if (!this.gVariable.platformBrowser) {
      const log: any = {
        'method' : method,
        'path' : url,
        'request' : req
      };
      if (!isError) {
        log.response = stringify(res);
        // console.log(log);
      } else {
        log.error = stringify(res);
        console.log(log);
      }
    }
  }

}
