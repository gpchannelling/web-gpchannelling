import { Injectable } from '@angular/core';
import {DatePipe} from '@angular/common';

@Injectable({
    providedIn: 'root'
})

export class DataMapService {

    constructor(private datePipe: DatePipe) {}

    public mapCenterListForMap(data) {

        const response = {
            address: {
                address: data.address.address || null,
                addressId: data.address.addressId || 0,
                city: data.address.city || null,
                latitude: data.address.latitude || 0,
                longitude: data.address.longitude || 0,
            },
            centerId: data.centerId || 0,
            centerName: data.centerName || null,
            contactNo: data.contactNo || null,
            openTime: data.openTime || null,
            status: data.status || null,
            centerType: data.centerType || null,
            distance: '0 Km',
            duration: '0 Min'


        };

        return response;
    }
}
