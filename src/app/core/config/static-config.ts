export class StaticConfig {

  public static pageTitle = 'E-CHANNELING';

  public static webUrl = 'https://www.echannelling.com/';
  public static copyrightText = 'Copyright © ';
  public static copyrightYear = new Date();
  public static siteImage = '../../images/logo.jpeg';
  public static author = 'Mobitel';

  public static userTypes = {
      'DOCTOR': {id: 1, code: 'DOCTR'},
      'PATIENT': {id: 2, code: 'PATNT'},
      'CENTER_ADMIN': {id: 3, code: 'CEN_A'}
  };

  public static secretKey = 'ech@nne!-gpc';

  public static statusTypes = {
    'SESSION_STATUS': 'SESSION_STATUS',
    'WEB_STATUS': 'WEB_STATUS',
    'APPOINTMENT_STATUS': 'APPOINTMENT_STATUS'
  };

  public static sessionStatusList = {
      'OPEN': 'OPEN',
      'CLOSE' : 'CLOSE',
      'HOLIDAY' : 'HOLIDAY',
      'ACTIVE' : 'ACTIVE',
      'INACTIVE' : 'INACTIVE'
    };

  public static sessionDoctorNotificationStatus = {
      'ARRIVED': {
          'ID': 5,
          'NAME': 'ARRIVED'
      },
      'DELAY' : {
          'ID': 2,
          'NAME': 'DELAY'
      },
      'START' : {
          'ID': 3,
          'NAME': 'START'
      },
      'END' : {
          'ID': 4,
          'NAME': 'END'
      }
  };

  public static sessionWebStatusList = {
      'ENABLE': 'ENABLE',
      'DISABLE' : 'DISABLE'
  };

  public static appointmentsStatusList = {
      'ACTIVE': 'ACTIVE',
      'INACTIVE': 'INACTIVE',
      //'VISITED': 'VISITED',
      'PAID': 'PAID'
  };

  public static appoinmentBookingStages = {
    'INIT': 'INIT',
    'EDIT': 'EDIT',
    'COMFIRM': 'COMFIRM',
    'PAYMENT': 'PAYMENT',
    'DETAILS': 'DETAILS'
  };

  public static days = [
      {
            id: 1,
            code: 'SUNDAY',
            description: 'Sunday',
      },
      {
          id: 2,
          code: 'MONDAY',
          description: 'Monday',
      },
      {
          id: 3,
          code: 'TUESDAY',
          description: 'Tuesday',
      },
      {
          id: 4,
          code: 'WEDNESDAY',
          description: 'Wednesday',
      },
      {
          id: 5,
          code: 'THURSDAY',
          description: 'Thursday',
      },
      {
          id: 6,
          code: 'FRIDAY',
          description: 'Friday',
      },
      {
          id: 7,
          code: 'SATURDAY',
          description: 'Saturday',
      }
  ];


  public static entitlementsList = {
      viewMap: {
          id: '001',
          accessType: 'PUBLIC',
          description: 'view map',
          code: 'VIEW_MAP'
      },
      viewCenterDetails: {
          id: '002',
          accessType: 'PUBLIC',
          description: 'view center details',
          code: 'VIEW_CENTER_DETAILS'
      },
      viewSessionList: {
          id: '003',
          accessType: 'PUBLIC',
          description: 'view session list',
          code: 'VIEW_SESSION_LIST'
      },


      // for doctor
      createSession_doctor: {
          id: 'P001',
          accessType: 'PRIVATE',
          description: 'create session for registered center by doctor',
          code: 'CREATE_SESSION_DOCT'
      },
      viewSession_doctor: {
          id: 'P002',
          accessType: 'PRIVATE',
          description: 'view session for registered center by doctor',
          code: 'VIEW_SESSION_DOCT'
      },
      editSession_doctor: {
          id: 'P003',
          accessType: 'PRIVATE',
          description: 'view session for registered center by doctor',
          code: 'EDIT_SESSION_DOCT'
      },
      createAppoinment_doctor: {
          id: 'P004',
          accessType: 'PRIVATE',
          description: 'create appointment for registered center by doctor',
          code: 'CREATE_APPOINMENT_DOCT'
      },
      viewAppoinment_doctor: {
          id: 'P005',
          accessType: 'PRIVATE',
          description: 'view appointment for registered center by doctor',
          code: 'VIEW_APPOINMENT_DOCT'
      },
      editAppoinment_doctor: {
          id: 'P006',
          accessType: 'PRIVATE',
          description: 'edit appoinment for resistered center by doctor',
          code: 'EDIT_APPOINMENT_DOCT'
      },
      viewIncome_doctor: {
          id: 'P007',
          accessType: 'PRIVATE',
          description: 'view income of doctor',
          code: 'VIEW_INCOME_DOCT'
      },
      profileUpdate_doctor: {
          id: 'P008',
          accessType: 'PRIVATE',
          description: 'update profile of doctor',
          code: 'PROFILE_UPDATE_DOCT'
      },


      // common
      resetPassword: {
          id: 'P009',
          accessType: 'PRIVATE',
          description: 'reset(forgot) password',
          code: 'RESET_PASSWORD'
      },



      // for center admin
      createSession_CenterAdmin: {
          id: 'P050',
          accessType: 'PRIVATE',
          description: 'create session for center',
          code: 'CREATE_SESSION_CEAD'
      },
      viewSession_CenterAdmin: {
          id: 'P051',
          accessType: 'PRIVATE',
          description: 'view session and doctor of the center',
          code: 'VIEW_SESSION_CEAD'
      },
      editSession_CenterAdmin: {
          id: 'P052',
          accessType: 'PRIVATE',
          description: 'update session of the center',
          code: 'EDIT_SESSION_CEAD'
      },
      createAppointment_CenterAdmin: {
          id: 'P053',
          accessType: 'PRIVATE',
          description: 'create appointment for centers relevent sessions',
          code: 'CREATE_APPOINMENT_CEAD'
      },
      viewAppointment_CenterAdmin: {
          id: 'P054',
          accessType: 'PRIVATE',
          description: 'view appointment for centers relevent sessions',
          code: 'VIEW_APPOINMENT_CEAD'
      },
      updateAppointment_CenterAdmin: {
          id: 'P055',
          accessType: 'PRIVATE',
          description: 'edit appointment for centers relevent sessions',
          code: 'UPDATE_APPOINMENT_CEAD'
      },
      viewIncome_CenterAdmin: {
          id: 'P056',
          accessType: 'PRIVATE',
          description: 'view income of center',
          code: 'VIEW_INCOME_CEAD'
      },
      profileUpdate_CenterAdmin: {
          id: 'P010',
          accessType: 'PRIVATE',
          description: 'update profile of center admin',
          code: 'PROFILE_UPDATE_CEAD'
      }
  };

  public static privilegesList = [{description: 'Create session for the center', privilegeCode: 'P050'},
        {description: 'view session and doctor of the center', privilegeCode: 'P051'},
        {description: 'Edit sessions of the center', privilegeCode: 'P052'},
        {description: 'Create appointment for sessions of the center', privilegeCode: 'P053'},
        {description: 'View appointment of sessions in the center', privilegeCode: 'P054'},
        {description: 'Edit appointment of sessions in the center', privilegeCode: 'P055'},
        {description: 'Income view of the center', privilegeCode: 'P056'},
        {description: 'Create sessions for registerd centers of doctor', privilegeCode: 'P001'},
        {description: 'View sessions of registered centers of doctor', privilegeCode: 'P002'},
        {description: 'Edit sessions of registered centers of doctor', privilegeCode: 'P003'},
        {description: 'Create appointment for sessions of doctor in any registered center', privilegeCode: 'P004'},
        {description: 'view appointments of sessions of doctor in any registered center', privilegeCode: 'P005'},
        {description: 'Edit appointments of session of doctor in any registered center', privilegeCode: 'P006'},
        {description: 'Income view of the dotor', privilegeCode: 'P007'},
        {description: 'Doctor profile update', privilegeCode: 'P008'},
        {description: 'Reset/Forgot Password ', privilegeCode: 'P009'},
        {description: null, privilegeCode: 'P010'}
  ];

}
