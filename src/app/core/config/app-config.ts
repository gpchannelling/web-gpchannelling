export class AppConfig {
  // this confog no longer use due to this has been moved to assets/js. This is not deleted due to reference purposes
  public static API_URL = {
    // PUBLIC : 'http://203.94.85.168:8080/gpc-service',
    // PRIVATE : 'http://203.94.85.168:8080/gpc-service',
    // PAYMENT : 'http://203.94.85.168:8443/gpc-service'

    // local and QA
    // PUBLIC : 'https://203.94.85.168:8443/gpc-service',
    // PRIVATE : 'https://203.94.85.168:8443/gpc-service',
    // PAYMENT : 'https://203.94.85.168:8443/gpc-service'

    // production
    PUBLIC : '/gpc-service',
    PRIVATE : 'https://localhost/gpc-service',
    PAYMENT : 'https://yourdoc.lk/gpc-service'
  };


}
