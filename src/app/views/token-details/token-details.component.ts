import {Component, Input, OnInit} from '@angular/core';
import {BookingCartService, SessionStorageService} from '../../services';
import {StaticConfig} from '../../core/config';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService} from '../../services';
import {ExternalJs, GlobalVariable} from '../../core/com-classes';
import {MessagesService} from '../../services/message.service';

@Component({
  selector: 'app-token-details',
  templateUrl: './token-details.component.html',
  styleUrls: ['./token-details.component.css']
})
export class TokenDetailsComponent implements OnInit {

    @Input() parentOrderRef: any;
    @Input() parentOrderDetails: any;

    public tokenDetails: any = [];
    public appointmentDetailsOrder: any = [];
    public totalCharge = 0;
    public cart: any = {};
    public staticConfig: any = StaticConfig;
    public requestPending = false;

    constructor(private clientSev: ClientService,
                private sessionStorageSev: SessionStorageService,
                private router: Router,
                private bookingCartSev: BookingCartService,
                private acRoute: ActivatedRoute,
                public gVariable: GlobalVariable,
                private messagesSev: MessagesService,
                private externalJS: ExternalJs) {

        if (!this.gVariable.platformBrowser) {

        } else {
            this.getRouterDetails();
        }
    }

    ngOnInit() {
        if (!this.gVariable.platformBrowser) {

        } else {
            this.externalJS.closeLoading();
            this.getRouterDetails();
        }

    }

    private getRouterDetails() {
        if (this.gVariable.platformBrowser) {
            if (this.parentOrderDetails && this.parentOrderDetails.refNo > 0) {
                this.tokenDetails = this.parentOrderDetails;
                this.calculateTotalChrges();
            } else {
                this.getAppointmentDetails(this.parentOrderRef);
            }
        }
    }


    private calculateTotalChrges() {
        this.totalCharge = 0;

        if (this.tokenDetails && this.tokenDetails.appointmentCharges) {
            for (let i in this.tokenDetails.appointmentCharges) {
                this.totalCharge = this.totalCharge + this.tokenDetails.appointmentCharges[i].amount;
            }
        }

        this.requestPending = false;
        this.onPrintReceipt();
    }


    private getAppointmentDetails(RefNo) {
        //console.log(RefNo);
        if (RefNo > 0) {
            this.requestPending = true;
            this.tokenDetails = [];
            this.clientSev.getAppointmentByRefNo(RefNo).then( (appontmentResponse: any) => {
                //console.log(appontmentResponse);
                if (appontmentResponse.refNo) {
                    this.tokenDetails = appontmentResponse;

                    this.bookingCartSev.updateAppointmentResponse(appontmentResponse).then((resp: any) => {
                        this.calculateTotalChrges();
                    });
                }


            }).catch( (error: any) => {
            });
        }

    }

    onPrintReceipt() {
        setTimeout(() => {
            window.print();
        }, 100);

    }

}
