import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionScheduleComponent } from './session-schedule.component';

describe('SessionScheduleComponent', () => {
  let component: SessionScheduleComponent;
  let fixture: ComponentFixture<SessionScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
