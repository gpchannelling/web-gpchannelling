import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ClientService, GuardService, SessionStorageService} from '../../../services';
import {StaticConfig} from '../../../core/config';
import { Accordion } from 'primeng/primeng';
import {DatePipe} from '@angular/common';
import {makeStateKey, Title, TransferState} from '@angular/platform-browser';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import {Router} from '@angular/router';
import {MessageService} from 'primeng/api';
import { Message } from 'primeng/api';
import {MessagesService} from '../../../services/message.service';
import {AuthenticationService} from '../../../services/authentication.service';
import {toInt} from 'ngx-bootstrap/chronos/utils/type-checks';
import {CustomMatSnakBarComponent} from '../../utilities/custom-mat-snak-bar/custom-mat-snak-bar.component';
import {MatInput, MatSnackBar, MatSnackBarConfig} from '@angular/material';

const FEES_LIST = makeStateKey<string>('FEES_LIST');

@Component({
  selector: 'app-session-schedule',
  templateUrl: './session-schedule.component.html',
  styleUrls: ['./session-schedule.component.css']
})
export class SessionScheduleComponent implements OnInit {

    public staticConfig: any = StaticConfig;
    public doctorId = 0;
    public centerId = 0;

    public daysOfWeek: any = StaticConfig.days;

    public sessionObj: any = {
        startDate: new Date(),
        endDate: new Date(),
        sessionFees: [],
        exceptionList: [],
        showNo: true
    };

    public feesList: any = [];

    public selectedCharge: any = {};
    public exceptionalSessionsList: any = [];
    public dateOption: any = {
        startDate : {
            minDate : new Date()
        },
        endDate : {
            minDate: this.sessionObj.startDate,
            viewDate: true
        }
    };

    public requestPending = false;
    @ViewChild('test') appStartNo: ElementRef;
    constructor(private clientSev: ClientService,
                private datePipe: DatePipe,
                private tState: TransferState,
                public gVariable: GlobalVariable,
                private sessionStorageSev: SessionStorageService,
                private router: Router,
                private messageSev: MessagesService,
                private authSev: AuthenticationService,
                private guardSev: GuardService,
                private matSnakBar: MatSnackBar,
                private externalJS: ExternalJs,
                title: Title) {

        if (this.gVariable.platformBrowser) {
            const auth = this.guardSev.getAuthentication();
            if (!auth || !auth.authorized ||
                (!authSev.checkEntitlementAvailability([
                    this.staticConfig.entitlementsList.createSession_doctor.id,
                    this.staticConfig.entitlementsList.createSession_CenterAdmin.id]))) {
                this.messageSev.mesageError('You do not have permission');
                this.router.navigate(['']);
            }
        }

        title.setTitle(StaticConfig.pageTitle + ' - SCHEDULE SESSION');

    }

    ngOnInit() {

        if (this.tState.hasKey(FEES_LIST)) {
            this.feesList = this.tState.get(FEES_LIST, 'FEES_LIST');
        } else if (!this.gVariable.platformBrowser) {

        } else {
            this.externalJS.closeLoading();
            this.getFeesList();
            this.getDoctorAndCenterIds();

            if (this.doctorId < 1 || this.centerId < 1) {
                this.router.navigate(['']);
            }
        }

    }

    private getDoctorAndCenterIds() {
        this.doctorId = this.sessionStorageSev.getCreateSessionSelectedDoctor();
        this.centerId = this.sessionStorageSev.getCreateSessionSelectedCenter();
    }


    private getFeesList() {
        this.clientSev.getFeesList().then( (response: any) => {
            if (response) {
                this.feesList = response;
            } else {
                this.feesList = [];
            }

        }).catch( (error: any) => {
            this.feesList = [];
        });
    }

    onChangeStartDate() {
        // console.log(this.dateOption.endDate.viewDate);
        this.sessionObj.endDate = this.sessionObj.startDate;
    }

    onSelectChargeType() {
        // console.log(this.selectedCharge.chargeType);
    }

    onClickAddAmount() {

        if (this.selectedCharge.chargeAmount == '') {
            this.messageSev.mesageError('Charge Amount Not Specified');
            return;
        }
        let alredyExist = false;

        for (let i in this.feesList) {
            // console.log(this.feesList[i]);
            if (this.feesList[i].chargeCode == this.selectedCharge.chargeType) {

                for (let j in this.sessionObj.sessionFees) {

                    if ( this.sessionObj.sessionFees[j].feeCode == this.feesList[i].chargeCode) {
                        alredyExist = true;
                    }
                }


                if (alredyExist == false) {
                    this.sessionObj.sessionFees.push(
                        {
                            'feeCode' : this.feesList[i].chargeCode,
                            'amount' : this.selectedCharge.chargeAmount,
                            'description' : this.feesList[i].description,
                            'onlineViewable' : this.selectedCharge.onlineViewable
                        }
                    );

                    this.selectedCharge.chargeAmount = '';
                    this.selectedCharge.chargeType = 0;
                } else {
                    this.messageSev.mesageError('Charge Type Already Exists');
                }

            }
        }

    }

    onClickRemoveCharge(chargeCode) {
        for (let i in this.sessionObj.sessionFees) {
            if (this.sessionObj.sessionFees[i].feeCode === chargeCode) {
                this.sessionObj.sessionFees.splice(i, 1);
            }
        }

        // console.log(this.sessionObj.sessionFees);
    }


    onClickAddException(exceptionalSessionsList) {

        const selectedExceptionObj = {
            day: exceptionalSessionsList.selectedDay,
            startDate: exceptionalSessionsList.exceptionStartTime,
            noOfPatient: exceptionalSessionsList.exceptionNoOfPatient
        };

        if (selectedExceptionObj.day && selectedExceptionObj.startDate && selectedExceptionObj.noOfPatient) {
            let alreadyExit = false;
            for (let i in this.sessionObj.exceptionList) {
                if (this.sessionObj.exceptionList[i].day == selectedExceptionObj.day) {
                    alreadyExit = true;
                }
            }

            if (alreadyExit == false) {
                this.sessionObj.exceptionList.push(selectedExceptionObj);
            }
        }


    }

    onCLickCancel() {
        this.sessionStorageSev.removeCreateSessionSelectedCenter();
        this.sessionStorageSev.removeCreateSessionSelectedDoctor();
        this.router.navigate(['session-list']);
    }

    onClickAddSessionSchedule() {

        if (this.centerId > 0 && this.doctorId > 0) {
            // this.requestPending = true;

            let req = {
                endDate: this.datePipe.transform(this.sessionObj.endDate, 'yyyy-MM-dd'),
                noOfPatient: this.sessionObj.noOfPatients,
                appStartNo: this.sessionObj.appStartNo || 0,
                sessionExceptions: [],
                sessionFees: [],
                startDate: this.datePipe.transform(this.sessionObj.startDate, 'yyyy-MM-dd'),
                startTime: this.converTime(this.sessionObj.startTime),
                timeInterval: this.sessionObj.timeInterval,
                viewDuration: this.sessionObj.viewDuration,
                showNo: this.sessionObj.showNo
            };


            for (let i in this.sessionObj.exceptionList) {
                const exception = this.sessionObj.exceptionList[i];
                req.sessionExceptions.push({
                    day: exception.day,
                    startTime: this.converTime(exception.startDate),
                    noOfPatient: exception.noOfPatient,
                });
            }

            for (let i in this.sessionObj.sessionFees) {
                req.sessionFees.push({
                    feeCode: this.sessionObj.sessionFees[i].feeCode,
                    amount: this.sessionObj.sessionFees[i].amount,
                    onlineViewable: this.sessionObj.sessionFees[i].onlineViewable ? 'YES' : 'NO',
                });
            }

            // console.log(req);
            // console.log(req.sessionFees);
            this.clientSev.addSessionSchedule(req, this.centerId, this.doctorId).then( (response: any) => {
                // console.log(response);
                if (response.status == 200) {
                    this.sessionStorageSev.removeCreateSessionSelectedCenter();
                    this.sessionStorageSev.removeCreateSessionSelectedDoctor();
                    if (response.data.length > 0) {
                        const msg = {
                            type: 'scheduleSessionError',
                            //heading: (response.data.length > 1) ? 'Already existing sessions' : 'Already existing session',
                            heading: 'Time slot already occupied',
                            //dataLists: response.data
                        };
                        this.messageSev.messageCustom(msg);
                    } else {
                        this.messageSev.mesageSuccess('Session successfully created');
                        this.router.navigate(['']);
                    }
                    // console.log(message);
                    // this.messageSev.customMesageSuccess(message, 10000);
                    this.requestPending = false;
                } else {
                    this.messageSev.mesageError('Failed to create ');
                    this.requestPending = false;
                }
            });

        } else {
            if (this.centerId <= 0 ) {
                this.messageSev.mesageError('Center not found');
            } else if (this.doctorId <= 0) {
                this.messageSev.mesageError('Doctor not found');
            }
        }



    }

    private converTime(value) {
        const TimeValue = value;
        let commonStartTime = '';

        const commonStartTimeType = TimeValue.split(' ');

        if (commonStartTimeType[1] === 'am') {

            const tempTimeList = commonStartTimeType[0].split(':');

            if (parseInt(tempTimeList[0], 0) === 12) {
                commonStartTime = '00:'  + tempTimeList[1] + ':00';
            } else {
                commonStartTime = commonStartTimeType[0] + ':00';
            }


        } else {
            const tempTimeList = commonStartTimeType[0].split(':');

            if (parseInt(tempTimeList[0], 0) < 12) {
                commonStartTime = (parseInt(tempTimeList[0], 0) + 12).toString() + ':' + tempTimeList[1] + ':00';
            } else {
                commonStartTime = commonStartTimeType[0] + ':00';
            }

        }

        return commonStartTime;
    }

}
