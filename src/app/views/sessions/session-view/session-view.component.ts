import { Component, OnInit } from '@angular/core';
import {AuthenticationService, ClientService, GuardService} from '../../../services';
import {StaticConfig} from '../../../core/config';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {DatePipe} from '@angular/common';
import {el} from '@angular/platform-browser/testing/src/browser_util';
import {MessagesService} from '../../../services/message.service';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import {Title} from '@angular/platform-browser';
import Swal from 'sweetalert2';

declare const $: any;

@Component({
  selector: 'app-session-view',
  templateUrl: './session-view.component.html',
  styleUrls: ['./session-view.component.css']
})
export class SessionViewComponent implements OnInit {

    public staticConfig: any = StaticConfig;
    public daysOfWeek: any = StaticConfig.days;

    public sessionObj: any = {};

    public feesList: any = [];

    public selectedCharge: any = {};
    public exceptionalSessionsList: any = [];
    public selectedSessionId = 0;
    public statusTypes: any = StaticConfig.statusTypes;
    public sessionStatusList: any = StaticConfig.sessionStatusList;
    public notificationStatusList: any = StaticConfig.sessionDoctorNotificationStatus;
    public sessionWebStatusList: any = StaticConfig.sessionWebStatusList;
    public seectedView = 'session';
    public formSubmitPending = false;

    public updateMessage: any = [];
    public incrementMessage: any = [];

    public sessionStarted: boolean;
    public currentTime: Date;
    public appointmentTime: Date;
    public appointmentHour: number;
    public appointmentMinute: number;

    constructor(private clientSev: ClientService,
                private route: Router,
                private acRoute: ActivatedRoute,
                private datePipe: DatePipe,
                private messagesSev: MessagesService,
                public gVariable: GlobalVariable,
                private guardSev: GuardService,
                public authSev: AuthenticationService,
                private externalJS: ExternalJs,
                title: Title) {

        if (this.gVariable.platformBrowser) {
            const auth = this.guardSev.getAuthentication();

            if (!auth || !auth.authorized ||
                !(authSev.checkEntitlementAvailability([
                    this.staticConfig.entitlementsList.viewSession_doctor.id,
                    this.staticConfig.entitlementsList.editSession_doctor.id,
                    this.staticConfig.entitlementsList.viewSession_CenterAdmin.id,
                    this.staticConfig.entitlementsList.editSession_CenterAdmin.id,
                    this.staticConfig.entitlementsList.viewAppoinment_doctor.id,
                    this.staticConfig.entitlementsList.editAppoinment_doctor.id,
                    this.staticConfig.entitlementsList.viewAppointment_CenterAdmin.id,
                    this.staticConfig.entitlementsList.updateAppointment_CenterAdmin.id]))) {
                this.messagesSev.mesageError('You do not have permission');
                this.route.navigate(['']);
            }
        }

        title.setTitle(StaticConfig.pageTitle + ' - SESSION');

    }

    ngOnInit() {
        if (this.gVariable.platformBrowser) {
            this.externalJS.closeLoading();
            this.getFeesList();
            this.getRouteParams();
        }
    }

    private getFeesList() {
        this.clientSev.getFeesList().then( (response: any) => {
            // console.log(response);
            this.feesList = response;
        }).catch( (error: any) => {

        });
    }

    private getRouteParams() {
        this.acRoute.params.subscribe( (routeData: any) => {
            // console.log(routeData);
            this.getSessionData(routeData.sessionId);
        });
    }

    private getSessionData(selectedSessionId) {
        this.clientSev.getSessionDataByID(selectedSessionId).then( (response: any) => {
            // console.log(response);

            this.sessionObj = response;
            this.selectedSessionId = response.sessionId;

            const startTime = response.startTime;
            const startDate = new Date(response.appointmentDate);

            this.sessionObj.startTimeOriginal = response.startTime;
            this.sessionObj.startTime = this.datePipe.transform(
                new Date(startDate.getFullYear() + '-' + startDate.getMonth() + '-' + startDate.getDate() + '-' + startTime), 'HH:mm');

            this.currentTime = new Date();
            this.appointmentHour = parseInt(this.sessionObj.startTime);
            this.appointmentMinute = parseInt(this.sessionObj.startTime.substr(3, 2));
            // tslint:disable-next-line:max-line-length
            this.appointmentTime = new Date(new Date(this.sessionObj.appointmentDate).setHours( this.appointmentHour, this.appointmentMinute, 0, 0));
            if (this.currentTime > this.appointmentTime) {
                this.sessionStarted = true;
            }
            // console.log(this.sessionObj.startTime);
            // console.log(this.selectedSessionId);
        }).catch( (error: any) => {
            // console.log(error);
        });
    }

    onSelectChargeType() {
      // console.log(this.selectedCharge.chargeType);
    }

    onClickUpdateSession(sessionObj) {
        this.formSubmitPending = true;

        const req = {
            sessionId: sessionObj.sessionId,
            noOfPatient: sessionObj.noOfPatient,
            startTime: this.converTime(sessionObj.startTime),
            viewDuration: sessionObj.viewDuration,
            appStartNo: sessionObj.appStartNo
        };

        this.clientSev.updateSession(req).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Successfully Updated');


                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);

                this.formSubmitPending = false;
            } else {
                this.messagesSev.mesageError('Update Failed, ' + response.error.message);
                this.formSubmitPending = false;
            }
        }).catch( (error: any) => {
            // console.log(error);
            this.messagesSev.mesageError('Update Failed, Try Again');
            this.formSubmitPending = false;
        });
    }


    onClickIncreaseCount() {

        if (this.sessionObj.noOfPatient > this.sessionObj.noOfReservedPatient) {
            const req = {
                currentPatientNo: this.sessionObj.currentPatient + 1,
                sessionId: this.sessionObj.sessionId
            };

            this.clientSev.addAppointmentViewed(req).then( (response: any) => {
                // console.log(response);
                if (response.status === 200) {
                    this.messagesSev.mesageSuccess('Successfuly updated');
                    this.sessionObj.currentPatient = req.currentPatientNo;

                    const selectedSessionId = this.selectedSessionId;
                    this.selectedSessionId = 0;
                    this.getSessionData(selectedSessionId);
                } else {
                    this.messagesSev.mesageError('Update Failed, ' + response.error.message);
                }
            }).catch( (error: any) => {
                // console.log(error)
                this.messagesSev.mesageError('Update Failed, ' + error.error.message);
            });

        } else {
            this.messagesSev.mesageError('Maximum limit reached');
        }

    }


    onClickActive() {
        const req = {
            changeType: this.statusTypes.SESSION_STATUS,
            sessionId: this.sessionObj.sessionId,
            status: this.sessionStatusList.ACTIVE
        };

        this.clientSev.updateSessionStatus(req).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Session Activated');

                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);
            } else {
                this.messagesSev.mesageError('Failed to activate');
            }

        }).catch( (error: any) => {
            this.messagesSev.mesageError('Failed to activate');
        });
    }

    onClickDeActive() {
        const req = {
            changeType: this.statusTypes.SESSION_STATUS,
            sessionId: this.sessionObj.sessionId,
            status: this.sessionStatusList.INACTIVE
        };

        this.clientSev.updateSessionStatus(req).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Session Deactivated');

                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);
            } else {
                this.messagesSev.mesageError('Session Contains Appointments, Please Deactivate Those First');
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Failed to Deactivate');
        });
    }

    onClickOpen() {
        const req = {
            changeType: this.statusTypes.SESSION_STATUS,
            sessionId: this.sessionObj.sessionId,
            status: this.sessionStatusList.OPEN
        };

        this.clientSev.updateSessionStatus(req).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Session Opened');

                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);
            } else {
                this.messagesSev.mesageError('Failed to Open');
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Failed to Open');
        });
    }

    onClickClose() {
        const req = {
            changeType: this.statusTypes.SESSION_STATUS,
            sessionId: this.sessionObj.sessionId,
            status: this.sessionStatusList.CLOSE
        };

        this.clientSev.updateSessionStatus(req).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Session Closed');

                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);
            } else {
                this.messagesSev.mesageError('Failed to close');
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Failed to close');
        });
    }

    onClickHoliDay() {
        const req = {
            changeType: this.statusTypes.SESSION_STATUS,
            sessionId: this.sessionObj.sessionId,
            status: this.sessionStatusList.HOLIDAY
        };

        this.clientSev.updateSessionStatus(req).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Session marked as holiday');

                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);
            } else {
                this.messagesSev.mesageError('Failed to marked as holiday');
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Failed to marked as holiday');
        });
    }

    onClickOnlineEnable() {
        const req = {
            changeType: this.statusTypes.WEB_STATUS,
            sessionId: this.sessionObj.sessionId,
            status: this.sessionWebStatusList.ENABLE
        };

        this.clientSev.updateSessionStatus(req).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Session web enabled');

                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);
            } else {
                this.messagesSev.mesageError('Failed to web enable');
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Failed to web enable');
        });
    }

    onClickOnlineDisable() {
        const req = {
            changeType: this.statusTypes.WEB_STATUS,
            sessionId: this.sessionObj.sessionId,
            status: this.sessionWebStatusList.DISABLE
        };

        this.clientSev.updateSessionStatus(req).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Session web disabled');

                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);
            } else {
                this.messagesSev.mesageError('Failed to web disable');
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Failed to web disable');
        });
    }

    onEventTriggerAppointmentViewed($event) {
        // console.log($event);
        this.getRouteParams();
    }

    private converTime(value) {
        const TimeValue = value;
        let commonStartTime = '';

        const commonStartTimeType = TimeValue.split(' ');

        if (commonStartTimeType[1] === 'am') {

            const tempTimeList = commonStartTimeType[0].split(':');

            if (parseInt(tempTimeList[0], 0) === 12) {
                commonStartTime = '00:'  + tempTimeList[1] + ':00';
            } else {
                commonStartTime = commonStartTimeType[0] + ':00';
            }


        } else {
            const tempTimeList = commonStartTimeType[0].split(':');

            if (parseInt(tempTimeList[0], 0) < 12) {
                commonStartTime = (parseInt(tempTimeList[0], 0) + 12).toString() + ':' + tempTimeList[1] + ':00';
            } else {
                commonStartTime = commonStartTimeType[0] + ':00';
            }

        }

        return commonStartTime;
    }

    onClickSessionStart() {
        const req = {
            'notificationType': {
                'description': this.notificationStatusList.START.NAME,
                'typeId': this.notificationStatusList.START.ID
            }
        };

        this.clientSev.doctorNotification(req, this.sessionObj.sessionId).then( (response: any) => {
            // console.log(response);
            if (response.status === 200) {
                this.messagesSev.mesageSuccess('Session started');

                const selectedSessionId = this.selectedSessionId;
                this.selectedSessionId = 0;
                this.getSessionData(selectedSessionId);
            } else {
                this.messagesSev.mesageError('Failed to start');
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Failed to start');
        });
    }

    onClickSessionDoctorArrived() {

    }

    onClickSessionEnd() {

    }

    onClickSessionDelay() {
        Swal.fire({
            title: 'Add the delay time',
            html: '<input class="swal2-input" type="number" id="swal-delay-hrs" placeholder="Hours" min="0" value="0" /> ' +
            '<input class="swal2-input" type="number" id="swal-delay-mins" placeholder="Minutes" min="0" value="0" />',
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Save',
            confirmButtonColor: '#119111',
            cancelButtonText: 'Cancel',
            cancelButtonColor: '#d76b1f',
            preConfirm: () => {
                if (($('#swal-delay-hrs').val() && ($('#swal-delay-hrs').val() > 0))
                    || $('#swal-delay-mins').val() && ($('#swal-delay-mins').val() > 0)) {
                    return {hrs: $('#swal-delay-hrs').val(), mins: $('#swal-delay-mins').val()};
                } else {
                    this.messagesSev.mesageError('Invalid entry!');
                    return false;
                }

            },

        }).then((result) => {

            if (result.value) {
                const req = {
                    'hours': result.value.hrs || 0,
                    'minites': result.value.mins || 0,
                    'notificationType': {
                        'description': this.notificationStatusList.DELAY.NAME,
                        'typeId': this.notificationStatusList.DELAY.ID
                    }
                };

                this.clientSev.doctorNotification(req, this.sessionObj.sessionId).then( (response: any) => {
                    // console.log(response);
                    if (response.status === 200) {
                        this.messagesSev.mesageSuccess('Successfully updated!');

                        const selectedSessionId = this.selectedSessionId;
                        this.selectedSessionId = 0;
                        this.getSessionData(selectedSessionId);
                    } else {
                        this.messagesSev.mesageError('Failed to update!');
                    }
                }).catch( (error: any) => {
                    this.messagesSev.mesageError('Failed to update');
                });
            }



        });

    }

}
