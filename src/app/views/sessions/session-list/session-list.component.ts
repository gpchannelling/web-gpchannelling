import {Component, Input, OnInit} from '@angular/core';
import { SessionComponent } from '../session/session.component';
import {AppConfig, StaticConfig} from '../../../core/config/index';
import {makeStateKey, Title, TransferState} from '@angular/platform-browser';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes/index';
import {BookingCartService, ClientService, GeoStorageService, GuardService, SessionStorageService} from '../../../services/index';
import {DatePipe} from '@angular/common';
import {Router} from '@angular/router';
import { DialogModule} from 'primeng/dialog';
import {MatInput, MatFormField, MatFormFieldControl, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {forkJoin, Observable} from 'rxjs/index';
import {ApiServiceConfig} from '../../../core';
import {AuthenticationService} from '../../../services/authentication.service';

const CENTERS_LIST = makeStateKey<string>('CENTERS_LIST');
const DOCTORS_LIST = makeStateKey<string>('DOCTORS_LIST');

declare const Object: any;

@Component({
  selector: 'app-session-list',
  templateUrl: './session-list.component.html',
  styleUrls: ['./session-list.component.css']
})
export class SessionListComponent implements OnInit {
    public Object: any = Object;

    @Input() isParent: string;
    @Input() selectedCenterObject: any;
    @Input() selectedDoctorObject: any;

    public appConfig: any = AppConfig;
    public staticConfig: any = StaticConfig;
    public centerObject: any;
    public doctorObject: any;

    public action = false;
    public sessionList = [];

    public doctorsList: any = [];
    public centersList: any = [];

    public searchObject: any = {
        rangeDates:  [new Date(), new Date()],
        selectedCenter: 'viewAll',
        selectedDoctor: 'viewAll',
        fromMin: new Date()
    };

    public displayDoctorDetails: any = false;
    public doctorData: any = [];
    public displayCenterDetails: any = false;
    public centerData: any = [];
    public selectedDoctorId = 0;
    public selectedCenterId = 0;

    constructor(public gVariable: GlobalVariable,
                private tState: TransferState,
                private clientSev: ClientService,
                private gService: GuardService,
                private datePipe: DatePipe,
                private bookingSev: BookingCartService,
                private route: Router,
                private sessionStorageSev: SessionStorageService,
                private geoStorageSev: GeoStorageService,
                public dialogBox: MatDialog,
                public authSev: AuthenticationService,
                private externalJS: ExternalJs,
                title: Title) {
        title.setTitle(StaticConfig.pageTitle + ' - SESSION');
    }

    ngOnInit() {

        if (this.tState.hasKey(DOCTORS_LIST) && this.tState.hasKey(CENTERS_LIST)) {
            // console.log('have state key');
            this.centersList = this.tState.get(CENTERS_LIST, 'CENTERS_LIST');
            this.doctorsList = this.tState.get(DOCTORS_LIST, 'DOCTORS_LIST');

        } else if (!this.gVariable.platformBrowser) {
            // console.log('in server');

        } else {
            // console.log('in browser');
            this.externalJS.closeLoading();
            if (!this.gVariable.authentication || !this.gVariable.authentication.authorized) {
                if (!this.selectedCenterObject && !this.selectedDoctorObject) {
                    // console.log('normal user in session list');
                    this.gService.removeAuthentication();
                    this.route.navigate(['', 'login']);
                } else {
                    this.loadData();
                }
            } else {
                this.loadData();
            }
        }

    }

    private loadData() {
        this.centerObject = {};
        this.doctorObject = {};

        this.getSelectedCenter();
        this.getSelectedDoctor();

        const selected_searchObj = this.sessionStorageSev.getSelectedSearchObj();

        if (selected_searchObj && (Object.keys(selected_searchObj).length > 0)) {
            this.searchObject.rangeDates =   selected_searchObj.rangeDates;
        }

        forkJoin(
            this.getAllDoctorsList(),
            this.getAllCentersList(),
        ).subscribe( (response) => {

            this.doctorsList = response[0];
            this.centersList = response[1];

            if ( selected_searchObj && (selected_searchObj.selectedDoctor) && (selected_searchObj.selectedDoctor > 0 ) &&  (this.doctorsList.length > 0)) {
                this.searchObject.selectedDoctor =   selected_searchObj.selectedDoctor;
            }

            if (selected_searchObj && (selected_searchObj.selectedCenter) && (selected_searchObj.selectedCenter > 0 ) &&  (this.centersList.length > 0)) {
                this.searchObject.selectedCenter =   selected_searchObj.selectedCenter;
            }

            this.getSessionList();
        });


    }

    private setSearchData() {
        this.sessionStorageSev.setSelectedSearchObj(this.searchObject);
    }

    private getSelectedCenter() {

        if (this.selectedCenterObject) {
            this.centerObject = this.selectedCenterObject;
        }  else {
            this.centerObject = this.geoStorageSev.getSelectedCenter();
        }

        // console.log(this.centerObject);
    }

    private getSelectedDoctor() {

        if (this.selectedDoctorObject) {
            this.doctorObject = this.selectedDoctorObject;
        }  else {
            this.doctorObject = this.geoStorageSev.getSelectedSearchDoctor();
        }

        // console.log(this.centerObject);
    }

    private getAllDoctorsList() {
        const promise = new Promise( (resolve, reject) => {
            let centerId = this.searchObject.selectedCenter || 0;

            if ( this.centerObject && Object.keys(this.centerObject).length > 0) {
                centerId = this.centerObject.centerId;
            } else if ((this.gVariable.authentication &&
                    this.gVariable.authentication.roles &&
                    (this.gVariable.authentication.roles[0].roleId === this.staticConfig.userTypes.CENTER_ADMIN.id)) &&
                this.gVariable.authentication.centers[0]) {

                centerId = this.gVariable.authentication.centers[0].centerId;
            }

            if (centerId == 'viewAll') {
                centerId = 0;
            }

            this.clientSev.getAllDoctorsList(centerId).then( (response: any) => {
                if (response) {
                    resolve(response);
                }
            }).catch( (error: any) => {
                console.log(error);
                resolve([]);
            });
        });

        return promise;


    }

    private getAllCentersList() {

        const promise = new Promise( (resolve, reject) => {

            let doctorId = this.searchObject.selectedDoctor || 0;

            if ( this.doctorObject && Object.keys(this.doctorObject).length > 0) {
                doctorId = this.doctorObject.doctorId;
            } else if (this.gVariable.authentication && this.gVariable.authentication.roles
                && (this.gVariable.authentication.roles[0].roleId === this.staticConfig.userTypes.DOCTOR.id)) {
                doctorId = this.gVariable.authentication.doctorId;

            }

            if (doctorId == 'viewAll') {
                doctorId = 0;
            }

            this.clientSev.getAllCentersList(doctorId).then( (response: any) => {
                resolve(response);
            }).catch((error: any) => {
                console.log(error);
                resolve([]);
            });
        });

        return promise;


    }

    private getSessionList() {

        // this.setSearchData();

        let req = {
            centerId: null,
            doctorId: null,
            startDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
            endDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd') ||
            this.datePipe.transform(new Date(), 'yyyy-MM-dd')
        };

        if (this.searchObject.rangeDates) {
            req = {
                centerId: null,
                doctorId: null,
                startDate: this.datePipe.transform(this.searchObject.rangeDates[0], 'yyyy-MM-dd'),
                endDate: this.datePipe.transform(this.searchObject.rangeDates[1], 'yyyy-MM-dd') ||
                this.datePipe.transform(this.searchObject.rangeDates[0], 'yyyy-MM-dd')
            };
        }

        if (this.gVariable.authentication &&
            this.gVariable.authentication.roles &&
            (this.gVariable.authentication.roles[0].roleId === this.staticConfig.userTypes.DOCTOR.id)) {

            // console.log(this.searchObject);
                req.doctorId = this.gVariable.authentication.doctorId;
                this.selectedDoctorId = this.gVariable.authentication.doctorId;


                if (this.searchObject.selectedCenter === 'viewAll') {
                    req.centerId = null;
                    this.selectedCenterId = 0;
                } else {
                    req.centerId = this.searchObject.selectedCenter;
                    this.selectedCenterId = this.searchObject.selectedCenter;
                }

                this.getSessionListData(req);

        } else if (this.gVariable.authentication &&
            this.gVariable.authentication.roles &&
            (this.gVariable.authentication.roles[0].roleId === this.staticConfig.userTypes.CENTER_ADMIN.id)) {

            req.centerId = this.gVariable.authentication.centers[0].centerId;
            this.selectedCenterId = this.gVariable.authentication.centers[0].centerId;

            if (this.searchObject.selectedDoctor === 'viewAll') {
                req.doctorId = null;
                this.selectedDoctorId = 0;
            } else {
                req.doctorId = this.searchObject.selectedDoctor;
                this.selectedDoctorId = this.searchObject.selectedDoctor;
            }

            this.getSessionListData(req);

        } else {
            // console.log(this.searchObject);
            // this.selectedCenterId = 0;
            if (this.centerObject && (Object.keys(this.centerObject).length > 0)) {

                req.centerId = this.centerObject.centerId;
                this.selectedCenterId = this.centerObject.centerId;

                if (this.searchObject.selectedDoctor === 'viewAll') {
                    req.doctorId = null;
                    this.selectedDoctorId = 0;
                } else {
                    req.doctorId = this.searchObject.selectedDoctor;
                    this.selectedDoctorId = this.searchObject.selectedDoctor;
                }

                this.getSessionListData(req);
            } else if (this.doctorObject && (Object.keys(this.doctorObject).length > 0)) {
                this.selectedDoctorId = this.doctorObject.doctorObject;

                req.doctorId = this.doctorObject.doctorId;
                this.selectedDoctorId = this.doctorObject.doctorId;

                if (this.searchObject.selectedCenter === 'viewAll') {
                    req.centerId = null;
                    this.selectedCenterId = 0;
                } else {
                    req.centerId = this.searchObject.selectedCenter;
                    this.selectedCenterId = this.searchObject.selectedCenter;
                }

                this.getSessionListData(req);
            }

        }

    }

    private getSessionListData(req) {
        this.clientSev.getSessionsList(req).then( (response: any ) => {
            // console.log(response);
            this.setSearchData();
            if (response.status === 200) {
                this.sessionList = response.data;
            } else {
                this.sessionList = [];
            }
        }).catch( (error: any) => {
            console.log(error);
            this.sessionList = [];
        });
    }

    onclickAddNew() {
        // this.setSearchData();

        // console.log(this.selectedDoctorId);
        // console.log(this.selectedCenterId);
        this.sessionStorageSev.createSessionSelectedDoctor(this.selectedDoctorId);
        this.sessionStorageSev.createSessionSelectedCenter(this.selectedCenterId);
        this.route.navigate(['', 'schedule-session']);
        // this.action = true;
    }

    onclickBook(session, $event) {

        // this.setSearchData();

        this.bookingSev.initCart({selectedSessionId: session.sessionId});
        setTimeout(() => {
            this.route.navigate(['', 'appointment-booking', session.sessionId]);
        }, 100);
    }

    onClickViewDoctorDetails($event) {
        const doctorId = $event.doctor.doctorId || 0;

        this.clientSev.getDoctorById(doctorId).then( (response: any) => {
            this.doctorData = response;
        }).catch( (error: any) => {
            this.doctorData = [];
        });

        this.displayDoctorDetails = true;
    }

    onClickViewCenterDetails($event) {
        const centerId = $event.center.centerId || 0;

        this.clientSev.getCenterById(centerId).then( (response: any) => {
            this.centerData = response;
        }).catch( (error: any) => {
            this.centerData = [];
        });

        this.displayCenterDetails = true;
    }

    onChangeFromDate() {

        const fromDate  = new Date(this.datePipe.transform(this.searchObject.rangeDates[0]));
        const toDate  = new Date(this.datePipe.transform(this.searchObject.rangeDates[1]));

        if (fromDate.getTime() > toDate.getTime()) {
            this.searchObject.rangeDates[1] = this.searchObject.rangeDates[0];
        }

        this.getSessionList();
    }

    onChangeToDate() {
        this.getSessionList();
    }

    onChangeCenter() {
        this.getSessionList();
    }

    onChangeDoctor() {
        this.getSessionList();
    }






}
