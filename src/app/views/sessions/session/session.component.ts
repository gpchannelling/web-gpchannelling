import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import {AppConfig, StaticConfig} from '../../../core/config';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {

  public appConfig: any = AppConfig;
  public staticConfig: any = StaticConfig;

  @Input() sessionObj: any;
  @Input() selectedDoctor: any;
  @Input() selectedCenter: any;
  @Input() centerObject: any;
  @Input() doctorObject: any;

  @Output() clickBookNow: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickViewCenter: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickViewDoctor: EventEmitter<any> = new EventEmitter<any>();

  public sessionsList: any = [];
  public dostorDetails: any = [];
  public centerDetails: any = [];
  public sessionEnded: boolean;
  public currentTime: Date;
  public appointmentEndTime: Date;
  public appointmentEndHour: number;
  public appointmentEndMinute: number;

  constructor(public gVariable: GlobalVariable,
              private route: Router,
              public authSev: AuthenticationService,
              private externalJS: ExternalJs,
              title: Title) {
      title.setTitle(StaticConfig.pageTitle + ' - SESSION');
  }

  ngOnInit() {
    if (this.gVariable.platformBrowser) {
        this.externalJS.closeLoading();

        if (this.sessionObj) {
            this.sessionsList = this.sessionObj;
        }

        if (this.selectedDoctor !== 'viewAll') {
            this.dostorDetails = this.selectedDoctor;
        }

        if (this.selectedCenter !== 'viewAll') {
            this.centerDetails = this.selectedCenter;
        }
        // console.log(this.sessionObj);
        // console.log(this.selectedDoctor);
        // console.log(this.selectedCenter);
    }

      this.currentTime = new Date(); /* this.currentTime = new Date(new Date().setHours(0,0,0,0))*/
      if (this.sessionObj.startTime.substr(6, 2) === 'AM') {
          this.appointmentEndHour = parseInt(this.sessionObj.endTime);
      } else {
          this.appointmentEndHour = parseInt(this.sessionObj.endTime) + 12;
      }
      this.appointmentEndMinute = parseInt(this.sessionObj.endTime.substr(3, 2));
      // tslint:disable-next-line:max-line-length
      this.appointmentEndTime = new Date(new Date(this.sessionObj.appointmentDate).setHours( this.appointmentEndHour, this.appointmentEndMinute, 0, 0));
      if (this.currentTime > this.appointmentEndTime) {
          this.sessionEnded = true;
      }
  }


  onClickBook(sessionsList) {
    this.clickBookNow.emit(sessionsList);
  }

  onClickViewCenter(sessionsList) {
    this.clickViewCenter.emit(sessionsList);

  }

  onClickViewDoctor(sessionsList) {
    this.clickViewDoctor.emit(sessionsList);

  }

  onClickViewSession(sessionsList) {
    // console.log(sessionsList);
    this.route.navigate(['', 'session-view', sessionsList.sessionId]);
  }
}
