import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../services';

import { HomeComponent } from './home/home.component';
import { P404Component } from './404/404.component';
import { MyDashboardComponent } from './auth/my-dashboard/my-dashboard.component';
import {LoginComponent} from './auth/login/login.component';
import {SessionListComponent} from './sessions/session-list/session-list.component';
import {CenterDetailsComponent} from './center-details/center-details.component';
import {SessionViewComponent} from './sessions/session-view/session-view.component';
import {SessionComponent} from './sessions/session/session.component';
import {PaymentViewComponent} from './payment-view/payment-view.component';
import {UpdateProfileComponent} from './auth/update-profile/update-profile.component';
import {AppointmentListComponent} from './appointments/appointment-list/appointment-list.component';
import {AppointmentBookingComponent} from './appointments/appointment-booking/appointment-booking.component';
import {AppointmentViewComponent} from './appointments/appointment-view/appointment-view.component';
import {RegistrationComponent} from './auth/registration/registration.component';
import {ForgetPasswordComponent} from './auth/forget-password/forget-password.component';
import {ResetPasswordComponent} from './auth/change-password/reset-password.component';
import {SessionScheduleComponent} from './sessions/session-schedule/session-schedule.component';
import {IncomeReportComponent} from './reports/income-report/income-report.component';
import {OrderDetailsComponent} from './order-details/order-details.component';
import {AddToBillComponent} from './add-to-bill/add-to-bill.component';
import {PaymentConfirmComponent} from './payment-confirm/payment-confirm.component';
import {PaymentCompleteComponent} from './payment-complete/payment-complete.component';
import {SelfRegistrationComponent} from './self-registration/self-registration.component';
import {UserRegisterComponent} from './user-register/user-register.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'VIEW'
    },
    children: [
      { path: '', component: HomeComponent },
      { path: 'my-dashboard', component: MyDashboardComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      // { path: 'update-profile', component: UpdateProfileComponent, canActivate: [AuthGuard] },
      { path: 'center-details', component: CenterDetailsComponent },
      { path: 'doctor-details', component: CenterDetailsComponent },
      { path: 'session-list', component: SessionListComponent, canActivate: [AuthGuard] },
      // { path: 'session', component: SessionComponent },
      { path: 'schedule-session', component: SessionScheduleComponent, canActivate: [AuthGuard] },
      { path: 'session-view/:sessionId', component: SessionViewComponent },
      { path: 'appointment-booking/:sessionId', component: AppointmentBookingComponent },
      { path: 'appointment-book/:session', component: AppointmentBookingComponent },
      { path: 'payment-response', data: {isPaymentResponse: true} , component: OrderDetailsComponent },
      // { path: 'appointment-list', component: AppointmentListComponent, canActivate: [AuthGuard] },
      // { path: 'appointment-view', component: AppointmentViewComponent, canActivate: [AuthGuard] },
      // { path: 'registration', component: RegistrationComponent },
      { path: 'forget-password', component: ForgetPasswordComponent },
      { path: 'reset-forgotten-password', component: ResetPasswordComponent},
      { path: 'report/revenue', component: IncomeReportComponent, canActivate: [AuthGuard] },
      { path: '404', component: P404Component },
      { path: 'add-to-bill', component: AddToBillComponent },
      { path: 'payment-confirm', component: PaymentConfirmComponent },
      { path: 'payment-complete', component: PaymentCompleteComponent },
      { path: 'self-registration', component: SelfRegistrationComponent },
      { path: 'user-register', component: UserRegisterComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewsRoutingModule { }
