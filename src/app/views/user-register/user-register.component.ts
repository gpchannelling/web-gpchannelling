import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ClientService, MessagesService} from '../../services';
import * as jsSHA from 'jssha';
import {StaticConfig} from '../../core/config';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
  staticConfig = StaticConfig;
  userRegObj: any = {
    roles: [{
      roleId: 3,
      description: 'Center admin privileges',
      roleName: 'CENTERADMIN',
      privileges: this.staticConfig.privilegesList
    }],
    userType: 'CENTERADMIN'
};
  requestPending = false;

  constructor(
      private router: Router,
      private clientSev: ClientService,
      private messagesSev: MessagesService,
  ) { }

  ngOnInit() {

  }

  createHash(hashText) {
    const js_sha = new jsSHA('SHA-256', 'TEXT');
    js_sha.update('ech@nne!-gpc' + btoa(hashText));
    const hasCode = js_sha.getHash('HEX');
    return hasCode.toUpperCase();
  }

  onClickUserRegister() {
    this.userRegObj.centers = JSON.parse(localStorage.getItem('authentication_pk')).centers;
    this.userRegObj.password = this.createHash(this.userRegObj.password);
    this.userRegObj.confirmPassword = this.createHash(this.userRegObj.confirmPassword);
    console.log(this.userRegObj);
    this.requestPending = true;
    this.clientSev.userRegistration(this.userRegObj).then( (response: any) => {
      if (response.status === 200) {
        this.requestPending = false;
        this.messagesSev.mesageSuccess('Registration successful');
        setTimeout(() => {
          this.router.navigate(['session-list']);
        }, 4000);
      } else {
        this.requestPending = false;
        this.messagesSev.mesageError('Data submission failed, please try again.');
      }
    }).catch( (error: any) => {
      this.requestPending = false;
      this.messagesSev.mesageError('Data submission failed, please try again.');
    });
  }

  onClickBack() {
    this.router.navigate(['session-list']);
  }
}
