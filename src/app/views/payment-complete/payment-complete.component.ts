import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService, MessagesService} from '../../services';
import {LocalStorageService} from '../../core/services';

@Component({
  selector: 'app-payment-complete',
  templateUrl: './payment-complete.component.html',
  styleUrls: ['./payment-complete.component.css']
})
export class PaymentCompleteComponent implements OnInit {
  private queryString = '';
  private keys = [];
  private values = [];

  constructor(
      private acRoute: ActivatedRoute,
      private clientSev: ClientService,
      private router: Router,
      private messagesSev: MessagesService,
      private storageSev: LocalStorageService,
      ) { }

  ngOnInit() {
    this.acRoute.queryParams.subscribe(params => {
      this.keys = Object.keys(params);
      this.values = Object.values(params);
      for (let i = 0; i < this.keys.length; i++ ) {
        this.queryString += this.keys[i] + '=' + this.values[i] + '&';
      }
    });
    this.clientSev.paymentComplete(this.queryString.substr(0, this.queryString.length - 1)).then((response) => {
      const values = Object.values(response);
      if (values[5] == 'SUCCESS') {
        // this.router.navigateByUrl('/payment-response');
        localStorage.setItem('bookingEChannelCart_pk', JSON.stringify(response));
        this.storageSev.set('paymentMethod', 'HSBC');
        this.router.navigate(['', 'payment-response']);
      } else {
        this.messagesSev.mesageError('Transaction failed');
      }
    }).catch((error) => {
      this.messagesSev.mesageError('Transaction failed');
    });
  }

}
