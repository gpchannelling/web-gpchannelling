import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import {StaticConfig} from '../../core';
import {ExternalJs, GlobalVariable} from '../../core/com-classes';

@Component({
  templateUrl: './404.component.html',
  styleUrls: ['./404.component.css']
})
export class P404Component implements OnInit {

  constructor(meta: Meta, private title: Title, private externalJS: ExternalJs, private gVariable: GlobalVariable) {
    // meta.addTags([]);
    title.setTitle(StaticConfig.pageTitle + '-404');
  }

  ngOnInit() {
      if (this.gVariable.platformBrowser) {
          this.externalJS.closeLoading();
      }
  }

}
