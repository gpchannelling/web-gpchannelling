import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { Location } from '@angular/common';
import {ClientService, GuardService} from '../../../services/index';
import {MessagesService} from '../../../services/message.service';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import {StaticConfig} from '../../../core/config';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css'],
})
export class ForgetPasswordComponent implements OnInit {

  public passwordForgetRequest: any = {};
  private hostName = '';
  public submitPending = false;

  constructor(
      private router: Router,
      private location: Location,
      private clientSev: ClientService,
      private guardSev: GuardService,
      private messageSev: MessagesService,
      public gVariable: GlobalVariable,
      private externalJS: ExternalJs,
      title: Title) {

      title.setTitle(StaticConfig.pageTitle + ' - PROFILE');
  }

  ngOnInit() {
      // console.log(this.location);
      if (this.gVariable.platformBrowser) {
          this.hostName = window.location.origin;
          this.externalJS.closeLoading();
      }

  }

  onClickSubmitForm(form) {
    // console.log(this.passwordForgetRequest);

    const req = {
      email: this.passwordForgetRequest.userName,
      callBackUrl: this.hostName + '/web-gpchannelling/#/reset-forgotten-password'
    };

    this.submitPending = true;

    this.clientSev.forgetPassword(req).then( (response: any) => {
        this.submitPending = false;
      // console.log(response);
      if (response.status == 200) {
          this.guardSev.setForgetPasswordEmail(req.email);
          this.messageSev.mesageSuccess('Please check your e-mail.');

          setTimeout(() => {
              this.router.navigate(['', 'login']);
          }, 3000);
      } else if (response.status === 0) {
          this.messageSev.mesageError('Error');
          this.router.navigate(['', 'login']);
          // this.messageSev.mesageSuccess('Your request submitted, please check you mail.');
          // const errorMessage = 'Connection Error.'
          // this.messageSev.mesageError('Request failed, ' + errorMessage);
      } else {
          this.messageSev.mesageError('Error');
          this.router.navigate(['', 'login']);
          // this.messageSev.mesageError('Request failed, ' + response.error.message);
      }
    }).catch((error: any) => {
      // console.log(error);
        this.messageSev.mesageError('Error');
        this.router.navigate(['', 'login']);
    });

  }

}
