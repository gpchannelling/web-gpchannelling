import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ClientService, GuardService} from '../../../services/index';
import { MessageService } from 'primeng/api';
import { Message } from 'primeng/api';
import {StaticConfig} from '../../../core/config/index';
import {GlobalVariable} from '../../../core/com-classes/index';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentication.service';
import {MessagesService} from '../../../services/message.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

    @Output() closePasswordView: EventEmitter<any> = new EventEmitter<any>();
    @Output() permissionDenied: EventEmitter<any> = new EventEmitter<any>();

    public changePasswordObj: any = {};
    public msg: Message[] = [];
    public staticConfig: any = StaticConfig;
    public submitPending = false;

    constructor(private clientSev: ClientService,
                private messageService:  MessageService,
                private gardSev: GuardService,
                private gVariable: GlobalVariable,
                private route: Router,
                private authSev: AuthenticationService,
                private messagesSev: MessagesService,
                title: Title) {
        title.setTitle(StaticConfig.pageTitle + ' - PROFILE');
    }

    ngOnInit() {
        if (this.gVariable.platformBrowser) {
            if (this.authSev.checkEntitlementAvailability([this.staticConfig.entitlementsList.resetPassword.id])) {
                return true;
            } else {
                this.messagesSev.mesageError('Permission denied');
                this.permissionDenied.emit(true);
            }
        }

    }

    onClickSubmitChangePasswordForm(form) {
        // console.log(form);
        this.submitPending = true;
        this.clientSev.passwordUpdate(this.changePasswordObj).then( (response: any) => {
            // console.log(response);
            if (response.status == 200) {

                this.reLogin(this.changePasswordObj.newPassword);

            }

        }).catch( (error: any) => {
            // console.log(error);
            const errorMsg = error.error ? error.error.message : '';
            this.messagesSev.mesageError('Update Failed ' + errorMsg);
            this.submitPending = false;

        });

    }


    private reLogin(newPassword) {
        if (this.gVariable.authentication && this.gVariable.authentication.userName) {
            const req = {
                userName: this.gVariable.authentication.userName,
                passWord: newPassword
            };

            // this.changePasswordObj = {};

            this.clientSev.login(req).then((response: any) => {
                // console.log(response);
                if (response.status == 200) {
                    response.data.userName = req.userName;
                    this.gardSev.createAuthentication(response.data); // set authentication

                    this.messagesSev.mesageSuccess('Password update successful');
                    this.submitPending = false;
                    this.closePasswordView.emit(true);

                }
            }).catch( (error: any) => {
                this.gardSev.removeAuthentication();

                this.messagesSev.mesageError('Password was updated but you have to login again');
                this.submitPending = false;
                this.route.navigate(['']);

            });
        } else {
            this.gardSev.removeAuthentication();

            this.messagesSev.mesageError('Password was updated but you have to login again');
            this.submitPending = false;
            this.route.navigate(['']);
        }
    }

    onClickCancel() {
      this.closePasswordView.emit(true);
    }



}
