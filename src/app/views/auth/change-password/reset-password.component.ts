import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ClientService, GuardService} from '../../../services/index';
import { MessageService } from 'primeng/api';
import { Message } from 'primeng/api';
import {StaticConfig} from '../../../core/config/index';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes/index';
import {ActivatedRoute, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./change-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

    @Input() parentName: any;
    @Output() closePasswordView: EventEmitter<any> = new EventEmitter<any>();

    public resetPasswordObj: any = {};
    public msg: Message[] = [];
    public staticConfig: any = StaticConfig;
    public submitPending = false;
    private subscribe: any;
    private queryParams: any = {};


    constructor(
        private clientSev: ClientService,
        private messageService:  MessageService,
        private gardSev: GuardService,
        private gVariable: GlobalVariable,
        private route: Router,
        private acRoute: ActivatedRoute,
        private externalJS: ExternalJs,
        title: Title) {
        title.setTitle(StaticConfig.pageTitle + ' - PROFILE');
    }

    ngOnInit() {
        if (this.gVariable.platformBrowser) {
            this.externalJS.closeLoading();
            this.subscribe = this.acRoute.queryParams.subscribe(
                params => {
                    this.queryParams = params || {};
                    if (!this.queryParams.token ) {
                        this.route.navigate(['', 'login']);
                    }
                    // console.log(this.queryParams.token);
                });
        }

    }

    public onClickSubmitResetPasswordForm(form) {
        // console.log(form);
        this.submitPending = true;

        const req = {
            newPassword: this.resetPasswordObj.newPassword,
            reNewPassword: this.resetPasswordObj.confirmNewPassword,
            token: this.queryParams.token
        };

        this.clientSev.passwordReset(req).then( (response: any) => {
            // console.log(response);
            if (response.status == 200) {

                this.route.navigate(['', 'login']);

                setTimeout(() => {
                    this.msg = [];
                }, 2000);

            }

        }).catch( (error: any) => {
            // console.log(error);
            const errorMsg = error.error ? error.error.message : '';
            this.msg = [];
            this.msg.push({severity: 'error', summary: 'Update Failed', detail: errorMsg});

            setTimeout(() => {
                this.msg = [];
                this.submitPending = false;
                this.route.navigate(['', 'login']);
            }, 2000);
        });

    }


    public reLogin(newPassword) {

        const req = {
            userName: this.gardSev.getForgetPasswordEmail(),
            passWord: newPassword
        };


        this.clientSev.login(req).then((response: any) => {
            // console.log(response);
            if (response.status == 200) {
                response.data.userName = req.userName;
                this.gardSev.createAuthentication(response.data); // set authentication

                this.msg = [];
                this.msg.push({severity: 'success', summary: 'Password update successful'});

                setTimeout(() => {
                    this.msg = [];
                    this.submitPending = false;
                    this.route.navigate(['']);
                    // this.closePasswordView.emit(true);
                }, 1000);


            }
        }).catch( (error: any) => {
            // console.log(error);
            this.gardSev.removeAuthentication();
            this.msg = [];
            this.msg.push({severity: 'error', summary: 'Password was updated but you have to login again'});

            setTimeout(() => {
                this.msg = [];
                this.submitPending = false;
                this.route.navigate(['', 'login']);
            }, 1000);
        });


    }

    public onClickCancel() {
        // this.closePasswordView.emit(true);
        this.route.navigate(['', 'login']);
    }



}
