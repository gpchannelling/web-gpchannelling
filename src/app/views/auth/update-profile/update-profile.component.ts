import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ClientService} from '../../../services';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import {StaticConfig} from '../../../core/config';
import {Message} from 'primeng/api';
import {AuthenticationService} from '../../../services/authentication.service';
import {MessagesService} from '../../../services/message.service';
import {Title} from '@angular/platform-browser';
import {LocalStorageService} from '../../../core/services';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {

  @Output() closeUpdateView: EventEmitter<any> = new EventEmitter<any>();
  @Output() permissionDenied: EventEmitter<any> = new EventEmitter<any>();

  public staticConfig: any = StaticConfig;
  public doctorProfile: any = {};
  public doctorProfileData: any = [];
  public centerProfileData: any = [];
  public msg: Message[] = [];
  public submitPending = false;
  public role = '';

  constructor(
      public gVariable: GlobalVariable,
      private clientSev: ClientService,
      private authSev: AuthenticationService,
      private messagesSev: MessagesService,
      private externalJS: ExternalJs,
      private localStorage: LocalStorageService,
      title: Title) {
      if (this.gVariable.platformBrowser) {
          this.loadUserData();
      }

      title.setTitle(StaticConfig.pageTitle + ' - PROFILE');

  }

  ngOnInit() {
      this.role = this.localStorage.get('authentication_pk').roles[0].roleName;

      if (this.gVariable.platformBrowser) {
          // tslint:disable-next-line:max-line-length
          if (this.authSev.checkEntitlementAvailability([this.staticConfig.entitlementsList.profileUpdate_CenterAdmin.id, this.staticConfig.entitlementsList.profileUpdate_doctor.id])) {
              return true;
          } else {
              this.permissionDenied.emit(true);
          }

          this.externalJS.closeLoading();
      }

  }

  public loadUserData() {
    if (this.gVariable.authentication.authorized && this.gVariable.authentication.roles[0] &&
        this.gVariable.authentication.roles[0].roleId === this.staticConfig.userTypes.DOCTOR.id) {
      const doctorId = this.gVariable.authentication.doctorId;
      this.clientSev.getDoctorById(doctorId).then( (response: any) => {
        // console.log(response);
        this.doctorProfileData = response;
        this.doctorProfile.contactNo = response.user.contactNo;
        this.doctorProfile.remark = response.qualification.remark;
        // console.log(this.doctorProfileData);
      }).catch( (error: any) => {
        console.log(error);
      });
    } else if (this.gVariable.authentication.authorized && this.gVariable.authentication.roles[0] &&
        this.gVariable.authentication.roles[0].roleId === this.staticConfig.userTypes.CENTER_ADMIN.id) {
      this.centerProfileData = this.gVariable.authentication.centers[0];
      this.centerProfileData.firstName = this.gVariable.authentication.firstName;
      this.centerProfileData.lastName = this.gVariable.authentication.lastName;
    }
  }

  onSubmitUpdateProfileForm(form) {
      // console.log(form);
      let req = {};

      if (this.role === 'DOCTOR') {
          req = {
              'doctorId': this.doctorProfileData.doctorId,
              'contactNo': this.doctorProfile.contactNo,
              'remark': this.doctorProfile.remark,
          };
          this.submitPending = true;

          this.clientSev.updateDoctor(req).then((response: any) => {
              // console.log(response);
              if (response.status === 200) {
                  this.messagesSev.mesageSuccess('Updated successfully');
                  this.submitPending = false;
                  this.closeUpdateView.emit(true);
              } else {
                  this.messagesSev.mesageError('Update Failed ' + response.error.message);
                  this.submitPending = false;
              }
          }).catch( (error: any) => {
              // console.log(error);
              this.messagesSev.mesageError('Update Failed');
              this.submitPending = false;
          });
      } else {
          req = {
              'centerId': this.centerProfileData.centerId,
              'contactNo': this.centerProfileData.contactNo,
          };
          this.submitPending = true;

          this.clientSev.updateCenter(req).then((response: any) => {
              // console.log(response);
              if (response.status === 200) {
                  this.messagesSev.mesageSuccess('Updated successfully');
                  this.submitPending = false;
                  this.closeUpdateView.emit(true);
              } else {
                  this.messagesSev.mesageError('Update Failed ' + response.error.message);
                  this.submitPending = false;
              }
          }).catch( (error: any) => {
              // console.log(error);
              this.messagesSev.mesageError('Update Failed');
              this.submitPending = false;
          });
      }



  }

  onClickCancel() {
    this.closeUpdateView.emit(true);
  }


    onEnterContactNo($event: Event) {
        if (this.role === 'DOCTOR') {
            this.doctorProfile.contactNo = (<HTMLInputElement>event.target).value;
        } else {
            this.centerProfileData.contactNo = (<HTMLInputElement>event.target).value
        }
  }
}
