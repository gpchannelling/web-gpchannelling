import { Component, OnInit } from '@angular/core';
import { GlobalVariable} from '../../../core/index';
import {AppConfig, StaticConfig} from '../../../core/config/index';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService, GuardService} from '../../../services/index';
import {AuthenticationService} from '../../../services/authentication.service';
import {MessagesService} from '../../../services/message.service';
import {Title} from '@angular/platform-browser';
import {ExternalJs} from '../../../core/com-classes';

declare const Math: any;

@Component({
  selector: 'app-my-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: ['./my-dashboard.component.css']
})
export class MyDashboardComponent implements OnInit {

  public appConfig: any = AppConfig;
  public staticConfig: any = StaticConfig;
  public Math: any = Math;

  public centerProfileData: any = [];
  public doctorProfileData: any = [];
  public patientProfileData: any = [];

  public viewPasswordChange = false;
  public viewUpdateProfile = false;
  public profileImage = null;

  constructor(
      public gVariable: GlobalVariable,
      private route: Router,
      private clientSev: ClientService,
      private router: ActivatedRoute,
      public authSev: AuthenticationService,
      private messagesSev: MessagesService,
      private guardSev: GuardService,
      private externalJS: ExternalJs,
      title: Title) {

      if (this.gVariable.platformBrowser) {
          const auth = this.guardSev.getAuthentication();
          if (!auth || !auth.authorized) {
              this.messagesSev.mesageError('You do not have permission');
              this.route.navigate(['']);
          }
      }

      title.setTitle(StaticConfig.pageTitle + ' - PROFILE');


  }

  ngOnInit() {
      if (this.gVariable.platformBrowser) {
          if (this.gVariable &&
              (this.gVariable.authentication &&
                  (!this.gVariable.authentication.authorized) || !this.gVariable.authentication)) {
              this.route.navigate(['', 'login']);
          } else {
              this.loadUserData();
          }

          this.externalJS.closeLoading();
      }
  }

  private loadUserData() {
      if (this.gVariable.authentication.authorized && this.gVariable.authentication.roles[0]
          && this.gVariable.authentication.roles[0].roleId === this.staticConfig.userTypes.DOCTOR.id) {

        const doctorId = this.gVariable.authentication.doctorId;
        this.getDoctor(doctorId);

      } else if (
          this.gVariable.authentication.authorized && this.gVariable.authentication.roles[0]
          && this.gVariable.authentication.roles[0].roleId === this.staticConfig.userTypes.CENTER_ADMIN.id) {
        this.centerProfileData = this.gVariable.authentication.centers[0];
        this.centerProfileData.firstName = this.gVariable.authentication.firstName;
        this.centerProfileData.lastName = this.gVariable.authentication.lastName;
        this.centerProfileData.email = this.gVariable.authentication.userName;

        // console.log(this.gVariable.authentication);
      }
  }

  private getDoctor(doctorId) {
      this.clientSev.getDoctorById(doctorId).then( (response: any) => {
          // console.log(response);
          this.doctorProfileData = response;
          this.profileImage = 'https://172.26.144.34:8443/' + this.doctorProfileData.image + '?' + Math.random();
         // console.log(this.doctorProfileData);
      }).catch( (error: any) => {
          console.log(error);
      });
  }

  private closeAllChildView() {
      this.viewUpdateProfile = false;
      this.viewPasswordChange = false;
  }

  onClickUpdateProfile() {
      this.closeAllChildView();
      this.viewUpdateProfile = true;
  }

  onClickChangePassword() {
      this.closeAllChildView();
      this.viewPasswordChange = true;
  }

  onChangeSuccessPasswordChange(event) {
      // console.log(event);
      this.closeAllChildView();
  }

  onChangePermissionDeniedChange(event) {
      // console.log(event);
      this.closeAllChildView();
  }

  onChangeSuccessUpdateProfile(event) {
      // console.log(event);
      this.closeAllChildView();
      this.loadUserData();
  }

  onChangeProfilePicture(files: FileList) {
    const fileToUpload = files.item(0);
    const req = {
        file: fileToUpload
    };

    this.clientSev.fileUpload(req).then( (response: any) => {
        if (response.status === 200) {
            const doctorId = this.gVariable.authentication.doctorId;
            this.profileImage = 'https://172.26.144.34:8443/' + response.error.text + '?' + Math.random();
            this.messagesSev.mesageSuccess('Updated successfully');
        } else {
            this.messagesSev.mesageError('Failed to update');
        }
    }).catch( (error: any) => {
        console.log(error);
    });
  }
}
