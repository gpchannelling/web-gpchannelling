import { Component, OnInit } from '@angular/core';
import {ClientService, GeoStorageService, GuardService, SessionStorageService} from '../../../services/index';
import { AuthenticationService } from '../../../services/authentication.service';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes/index';
import { Router } from '@angular/router';
import { StaticConfig } from '../../../core/index';
import { MessagesService } from '../../../services/message.service';
import {Title} from '@angular/platform-browser';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginRequest: any = {};
  public login = true;
  public staticConfig: any = StaticConfig;
  public submitPending = false;

  constructor(private clientSev: ClientService,
              private authSev: AuthenticationService,
              private gardSev: GuardService,
              private gVariable: GlobalVariable,
              private router: Router,
              private geoStorageSev: GeoStorageService,
              private messageSev: MessagesService,
              private sessionStorageSev: SessionStorageService,
              private externalJS: ExternalJs,
              title: Title) {
      if (this.gVariable.authentication && this.gVariable.authentication.authorized) {
          this.redirectToHome();
      }

      title.setTitle(StaticConfig.pageTitle + ' - LOGIN');
  }

  ngOnInit() {
      if (this.gVariable.platformBrowser) {
          this.externalJS.closeLoading();
      }
  }

  onClickLoginForm(loginForm) {
    this.submitPending = true;
    if (this.loginRequest.userName && this.loginRequest.passWord) {
        this.clientSev.login(this.loginRequest).then((response: any) => {
        this.submitPending = false;
          if (response.status === 200) {
              response.data.userName = this.loginRequest.userName;
              this.gardSev.createAuthentication(response.data); // save authentication
              this.authSev.setEntitlements(response.data); // set entitlements
                // console.log(response.data);
              this.geoStorageSev.setSelectedMapPoint({}); // set geo point to empty
              this.sessionStorageSev.setSelectedSearchObj({});
              this.gVariable.selectedCenter = response.data.centers[0]; // set selected center to empty

              this.authSev.UpdateLoginStatus('loggedIn');
              this.geoStorageSev.setSelectedSearchDoctor({
                  doctorId: response.data.doctorId,
                  firstName: response.data.firstName,
                  lastName: response.data.lastName,
              });
              if (response.data.firstLoginStatus === 'NO') {
                  setTimeout(() => {
                      this.router.navigate(['session-list']);
                  });
              } else {
                  this.login = false;
              }

          } else if (response.status === 0) {
              const errorMessage = 'Connection Error.';
              this.messageSev.mesageError('Login Failed, ' + errorMessage);
          } else {
              this.messageSev.mesageError('Login Failed, ' + response.error.message);
          }

        }).catch( (error: any) => {
            this.submitPending = false;
            const errorMesssage = error.error.message ? error.error.message : 'Connection Error.';
            this.messageSev.mesageError('Login Failed, ' + errorMesssage);

        });
    }



  }

  onChangeSuccessPasswordChange(event) {
      this.redirectToHome();
  }


  private redirectToHome() {
      this.router.navigate(['']);
  }



}
