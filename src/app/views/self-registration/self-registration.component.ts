import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ClientService, MessagesService} from '../../services';

@Component({
  selector: 'app-self-registration',
  templateUrl: './self-registration.component.html',
  styleUrls: ['./self-registration.component.css']
})
export class SelfRegistrationComponent implements OnInit {
  selfRegObj: any = {
    registrationType: 'doctor',
};
  requestPending = false;

  constructor(
      private router: Router,
      private clientSev: ClientService,
      private messagesSev: MessagesService,
  ) { }

  ngOnInit() {
  }

  onClickSelfRegister() {
    console.log(this.selfRegObj)
    this.requestPending = true;
    this.clientSev.selfRegistration(this.selfRegObj).then( (response: any) => {
      if (response.status === 200) {
        this.requestPending = false;
        this.messagesSev.mesageSuccess('Your data have been successfully submitted. Our team will be contacting you soon. Thank you!');
        setTimeout(() => {
          this.router.navigate(['']);
        }, 4000);
      } else {
        this.requestPending = false;
        this.messagesSev.mesageError('Data submission failed, please try again.');
      }
    }).catch( (error: any) => {
      this.requestPending = false;
      this.messagesSev.mesageError('Data submission failed, please try again.');
    });
  }

  onClickBack() {
    this.router.navigate(['']);
  }

  updateSelection() {
    this.selfRegObj.registrationType = this.selfRegObj.registrationType === 'doctor' ? 'centerAdmin' : 'doctor';
  }
}
