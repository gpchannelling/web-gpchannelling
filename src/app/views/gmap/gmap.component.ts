import { Component, OnInit } from '@angular/core';
import {ClientService, GeolocationService, GeoStorageService, MessagesService, SessionStorageService} from '../../services';
import { } from 'googlemaps';
import {AppConfig} from '../../core';
import {Observable, of} from 'rxjs/index';
import { forEach } from '@angular/router/src/utils/collection';
import {absFloor} from 'ngx-bootstrap/chronos/utils';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {ExternalJs, GlobalVariable} from '../../core/com-classes';
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged, startWith, switchMap, map} from 'rxjs/operators';
import {catchError, debounce, finalize} from 'rxjs/internal/operators';

declare var $: any;
declare var google: any;
declare var window: any;

@Component({
  selector: 'app-gmap',
  templateUrl: './gmap.component.html',
  styleUrls: ['./gmap.component.css']
})
export class GmapComponent implements OnInit {

    public appConfig = AppConfig;
    public markerList: any = [];
    public markedLocation: any;
    public selectedLocation = '';
    public map: any;
    public searchBox: any;
    public markerPoint: any;
    public mapRadius: any = 5;
    public latitude: 0;
    public longitude: 0;
    public radiusRange = 2;

    public directionsService: any;
    public directionsDisplay: any;

    public seectedInfoWindow: any;
    public cityCircle: any;
    public distanceMatrixService: any;
    public googlePlaceSevice: any;
    public mobileLatitude: any;
    public mobileLongitude: any;

    public mapStyle = [
        {
            featureType: 'administrative',
            elementType: 'geometry',
            stylers: [
                {
                    'visibility': 'off'
                }
            ]
        },
        {
            featureType: 'poi',
            stylers: [
                {
                    'visibility': 'off'
                }
            ]
        },
        {
            featureType: 'poi.business',
            stylers: [
                {
                    visibility: 'off'
                }
            ]
        },
        {
            featureType: 'road',
            elementType: 'labels.icon',
            stylers: [
                {
                    visibility: 'on'
                }
            ]
        },
        {
            featureType: 'transit',
            stylers: [
                {
                    visibility: 'on'
                }
            ]
        }
    ];
    public centersList = [];
    public gpCentersList = [];
    public vetCentersList = [];
    public loaderComplete = false;
    public doctorsList = [];
    public filteredDoctorsList: Observable<string[]>;
    public searchControl = new FormControl();
    public selectedDoctor: any = [];
    public selectedDoctorName = '';
    public countryRestriction = {
        latLngBounds: {
            east: 81.91078937838002,
            north: 9.859870138030162,
            south: 5.888495325628273,
            west: 79.60366045297664
        },
        strictBounds: true
    };
    doctorType = 'GP';

    constructor(private geolocationSev: GeolocationService,
                private clientSev: ClientService,
                private router: Router,
                private geoStorageSev: GeoStorageService,
                private acRoute: ActivatedRoute,
                private eJs: ExternalJs,
                public gVariable: GlobalVariable,
                private sessionStorageSev: SessionStorageService,
                private messageSev: MessagesService,
                private externalJS: ExternalJs) {

        if (this.gVariable.platformBrowser) {
            this.loadLiveSearch();
            this.getRouteParams();
        }
    }

    ngOnInit() {
        if (this.gVariable.platformBrowser) {
            this.externalJS.closeLoading();
        }
    }

    updateSelection() {
        this.doctorType = this.doctorType === 'GP' ? 'VET' : 'GP';
    }

    private loadLiveSearch() {
        this.clientSev.getAllDoctors().then((response: any) => {
            if (response) {
                this.doctorsList = Object.values(response);
                this.filteredDoctorsList = this.searchControl.valueChanges
                    .pipe(
                        startWith(''),
                        map(value => this._filter(value))
                    );
            } else {
                return [];
            }
        });
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();

        return this.doctorsList.filter(option => `${option.user.firstName} ${option.user.lastName}`.toLowerCase().includes(filterValue));
    }

   private lookup(value: string) {
        if (value.length > 2) {
            const data =  this.clientSev.getDoctorsListByName(value.toLowerCase()).then((response: any) => {
                if (response) {
                    return response;
                } else {
                    return [];
                }

            });

            return data;

        } else {
            return [];
        }


    }

    onSelectDoctorSearch(value) {
        // console.log(event);
        this.searchControl.setValue('Dr. ' + value.user.firstName + ' ' + value.user.lastName);
        this.selectedDoctor = value;
    }

    onClickSearchSelectedDoctor(selectedDoctor) {
        // console.log(selectedDoctor);
        if(selectedDoctor.doctorId > 0) {
            this.setStorageEmpty();
            this.geoStorageSev.setSelectedSearchDoctor(selectedDoctor);

            setTimeout(() => {
                this.router.navigate(['', 'doctor-details']);
            },  100);
        } else {
            this.messageSev.mesageError('Select a valid doctor');
        }

    }

    private setStorageEmpty() {
        this.geoStorageSev.setSelectedSearchDoctor({});
        this.sessionStorageSev.setSelectedSearchObj({});

        this.geoStorageSev.setSelectedCenter({});
        this.geoStorageSev.setSelectedMapPoint({latitude: 0, longitude: 0});
    }


    private getRouteParams() {
        this.acRoute.queryParams.subscribe( (params: any) => {
            // console.log(params);
            if (params.lat && params.lng) {
                this.mobileLatitude = params.lat;
                this.mobileLongitude = params.lng;

                let location = {
                    coords: {
                        latitude: this.mobileLatitude,
                        longitude: this.mobileLongitude
                    }
                };

                this.setPosition(location);
            } else {

            }
        });
        this.loaderComplete = true;
    }

    private setPosition(location) {

        this.latitude = 0;
        this.longitude = 0;

        if (location.coords.latitude > 0 && location.coords.longitude > 0) {
            this.latitude = location.coords.latitude;
            this.longitude = location.coords.longitude;
        }

    }

    private setMapOption(map) {
        if (this.gVariable.platformBrowser) {
            const zoomLevel = this.geoStorageSev.getSelectedMapZoomLevel() || 10;
            map.setOptions({
                zoom: zoomLevel,
                zoomControl: false,
                gestureHandling: 'greedy',
                maxZoom: 17,
                minZoom: 14,
                // fullscreenControl: 'true',
                streetViewControl: false,
                setMyLocationEnabled: true,
                setOnMyLocationButtonClickListener: this,
                setOnMyLocationClickListener: this,

            });
        }
    }


    private setMapSearchBox(map, searchBox, marker) {
        if (this.gVariable.platformBrowser) {
            const thisClass = this;

            map.addListener('bounds_changed', () => {
                searchBox.setBounds(map.getBounds());
            });

            searchBox.addListener('places_changed', () => {
                const places = searchBox.getPlaces();

                if (places.length === 0) {
                    return false;
                }

                // For each place, get the icon, name and location.
                const bounds = new google.maps.LatLngBounds();

                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log('Returned place contains no geometry');
                        return;
                    }

                    thisClass.latitude = place.geometry.location.lat();
                    thisClass.longitude = place.geometry.location.lng();

                    thisClass.setMarkerPoint(map, marker);

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);

                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);

            });
        }
    }

    private getMapGeoCodeToSearchBox(latLng) {
        if (this.gVariable.platformBrowser) {
            const geocoder = new google.maps.Geocoder;
            geocoder.geocode({'location': latLng}, (result, status) => {
                if (status == 'OK') {
                    $("#searchBox").val(result[0].formatted_address);
                    this.selectedLocation = result[0].formatted_address;
                }
            });
        }
    }

    private createHomeIcon(map, marker) {
        if (this.gVariable.platformBrowser) {
            let thisClass = this;

            /* home button*/
            const controlDiv = document.createElement('div');
            const controlUI = document.createElement('icon');
            controlUI.style.backgroundColor = '#fff';
            controlUI.style.border = '2px solid #fff';
            controlUI.style.borderRadius = '3px';
            controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
            controlUI.style.cursor = 'pointer';
            controlUI.style.marginBottom = '45px';
            controlUI.style.marginRight = '22px';
            // controlUI.style.width = '25px';
            // controlUI.style.height = '25px';
            controlUI.title = 'Click to recenter the map';
            controlUI.setAttribute('class', 'fa fa-home fa-lg');
            controlDiv.appendChild(controlUI);

            /* home button event */
            controlUI.addEventListener('click', () => {

                if (this.mobileLatitude > 0 && this.mobileLongitude > 0) {
                    let location = {
                        coords: {
                            latitude: this.mobileLatitude,
                            longitude: this.mobileLongitude
                        }
                    };

                    this.setPosition(location);
                    if (this.map && this.searchBox && this.markerPoint) {
                        this.setMarkerPoint(this.map, this.markerPoint);
                        this.getMapGeoCodeToSearchBox({lat: this.latitude, lng: this.longitude});
                    }
                } else {
                    this.geolocationSev.getUserLocation().subscribe((result: any) => {

                        this.setPosition(result);
                        this.setMarkerPoint(map, marker);
                        thisClass.getMapGeoCodeToSearchBox({lat: thisClass.latitude, lng: thisClass.longitude});

                        this.getCenterList();
                    });
                }



            });

            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(
                controlDiv
            );
        }
    }

    private setMarkerPoint(map, marker) {
        if (this.gVariable.platformBrowser) {
            marker.setPosition(new google.maps.LatLng(this.latitude, this.longitude));
            map.setCenter(marker.getPosition()); /*center the map with marker*/
            this.drawCircle(map);
        }
    }

    onMapReady(map) {
        if (this.gVariable.platformBrowser) {
            const thisClass = this;

            /* to set the basic map configs */
            this.setMapOption(map);

            /* create search box */
            const inputBox = document.getElementById('searchBox');
            const searchBox = new google.maps.places.SearchBox(inputBox);
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(inputBox);

            /* create default marker*/
            let marker = new google.maps.Marker({
                position: new google.maps.LatLng(thisClass.latitude, thisClass.longitude),
                icon: ('./assets/images/icons/male.svg'),
                draggable: true,
                map: map
            });


            marker.addListener('dragend', function($event) {
                // console.log($event.latLng.lat());
                // console.log($event.latLng.lng());
                const result = {coords: {
                        latitude : $event.latLng.lat(),
                        longitude : $event.latLng.lng(),
                    }};
                thisClass.setPosition(result);
                map.setCenter(marker.getPosition()); /*center the map with marker*/
                thisClass.getMapGeoCodeToSearchBox($event.latLng); /* set selected geo code */

                thisClass.getCenterList();

                thisClass.drawCircle( map );

            });

            this.setMapSearchBox(map, searchBox, marker);
            this.createHomeIcon(map, marker);

            this.map = map;
            this.searchBox = searchBox;
            this.markerPoint = marker;

            this.cityCircle = new google.maps.Circle();

            google.maps.event.addDomListener(map, 'zoom_changed', () => {

                thisClass.drawCircle( map );

                if (this.map && this.searchBox && this.markerPoint) {
                    this.getCenterList();
                }

                setTimeout(() => { map.setCenter(this.markerPoint.getPosition()); }, 50);

            });

            this.directionsService = new google.maps.DirectionsService();
            this.directionsDisplay = new google.maps.DirectionsRenderer();
            this.distanceMatrixService = new google.maps.DistanceMatrixService();
            this.googlePlaceSevice = new google.maps.places.PlacesService(map);

            // to set the map marker location

            const coords = this.geoStorageSev.getSelectedMapPoint();

            if (coords && (Object.keys(coords).length > 0) && (coords.latitude > 0) && (coords.longitude > 0) ) {

                let result = {coords: coords};
                setTimeout(() => {
                    this.setPosition(result);
                    this.setMarkerPoint(map, marker);
                    this.getMapGeoCodeToSearchBox({lat: thisClass.latitude, lng: thisClass.longitude});

                    thisClass.getCenterList();

                }, 100);

            } else {

                if (this.mobileLatitude > 0 && this.mobileLongitude > 0) {

                    let location = {
                        coords: {
                            latitude: this.mobileLatitude,
                            longitude: this.mobileLongitude
                        }
                    };

                    this.setPosition(location);
                    if (this.map && this.searchBox && this.markerPoint) {
                        this.setMarkerPoint(this.map, this.markerPoint);
                        this.getMapGeoCodeToSearchBox({lat: this.latitude, lng: this.longitude});
                    }
                } else {

                    this.geolocationSev.getUserLocation().subscribe((result: any) => {

                        setTimeout(() => {
                            this.setPosition(result);
                            this.setMarkerPoint(map, marker);
                            this.getMapGeoCodeToSearchBox({lat: thisClass.latitude, lng: thisClass.longitude});

                            thisClass.getCenterList();

                        }, 100);


                    });
                }

            }
        }

    }

    private drawCircle( map ) {
        if (this.gVariable.platformBrowser) {
            if (map) {
                const r = 3963.0; // radius of earth 3963 miles

                const bounds = map.getBounds();

                const center = this.markerPoint.getPosition();

                // Then the points
                if (bounds) {

                    const swPoint = bounds.getSouthWest();
                    const nePoint = bounds.getNorthEast();


                    const proximity = google.maps.geometry.spherical.computeDistanceBetween(swPoint, nePoint);

                    const displayWidth = document.getElementById('mapDiv').offsetWidth - (document.getElementById('mapDiv').offsetWidth * 0.1); // removing the outer pading and margins
                    const displayHeight = document.getElementById('mapDiv').offsetHeight - ((document.getElementById('mapDiv').offsetHeight) * 0.1); // footer: (document.getElementById('mapDiv').offsetHeight) * 0.1

                    const sqrOfWidth = Math.pow(displayWidth, 2);
                    const sqrOfHeight = Math.pow(displayHeight, 2);

                    const lengthOfMidPoint = Math.sqrt(sqrOfWidth + sqrOfHeight);

                    let displayDiameter = 0;

                    if (displayWidth > displayHeight) {
                        displayDiameter = (proximity / lengthOfMidPoint) * displayHeight;
                    } else {
                        displayDiameter = (proximity / lengthOfMidPoint) * displayWidth;
                    }

                    const constant = 0.95;

                    const radius = ((displayDiameter / 2) * constant);
                    this.mapRadius = radius / 100;

                    this.cityCircle.setMap(null);

                    this.cityCircle = new google.maps.Circle({
                        strokeColor: '#f7dcdc',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#4b31ff',
                        fillOpacity: 0.1,
                        map: map,
                        center: center,
                        radius: radius
                    });
                    // console.log(displayDiameter / 2);

                    // console.log(this.mapRadius);
                    this.getCenterList();


                }

            }
        }
    }

    private getCenterList() {
        if (this.gVariable.platformBrowser) {
            let radius = 0;
            this.centersList = [];
            this.directionsDisplay.set('directions', null);

            const mapRadius = this.mapRadius;
            const marker = this.markerPoint.getPosition();

            if (mapRadius && parseFloat(mapRadius)) {
                radius = parseFloat(parseFloat(mapRadius).toFixed(3));

                const req = {
                    latitude: marker.lat(),
                    longitude: marker.lng(),
                    distance: radius
                };

                this.clientSev.getCenterListForMap(req).then((response: any) => {
                    // console.log(response);
                    if (response) {
                        for (let i in response) {
                            const origin = new google.maps.LatLng(marker.lat(), marker.lng());
                            const destination = new google.maps.LatLng(response[i].address.latitude, response[i].address.longitude);

                            this.geolocationSev.getDistance(this.distanceMatrixService, origin, destination).subscribe((distanceResponse: any) => {
                                response[i].distance = distanceResponse.rows[0].elements[0].distance.text;
                                response[i].duration = distanceResponse.rows[0].elements[0].duration.text;
                            });
                        }
                        this.centersList = response;
                        this.gpCentersList = this.centersList.filter(center => center.centerType === 'GP' || center.centerType == null);
                        this.vetCentersList = this.centersList.filter(center => center.centerType === 'VET');
                    }
                }).catch((error: any) => {
                    console.log(error);
                });
            }

        }

    }

    onClickedMarker(center) {
        if (this.gVariable.platformBrowser) {
            // public clickedMarker(center, infoWindow) {
            let thisClass = this;

            // to draw the path / road-map between selected and center points
            const originLatLng = thisClass.markerPoint.getPosition();
            const destinationLatLng = {lat: center.address.latitude, lng: center.address.longitude};


            let lineSymbol = {
                path: 'M 0,1,0,1',
                strokeOpacity: 1,
                scale: 5
            };


            this.directionsDisplay.setOptions({
                draggable: false,
                map: this.map,
                preserveViewport: true,
                suppressMarkers: true,
                polylineOptions: {
                    strokeColor: 'blue',
                    strokeOpacity: 0,
                    icons: [{
                        icon: lineSymbol,
                        offset: '0',
                        repeat: '10px'
                    }],
                }
            });

            this.directionsService.route({
                origin: originLatLng,
                destination: destinationLatLng,
                travelMode: 'DRIVING',
                avoidTolls: true,
            }, function (response, status) {
                // console.log(response);
                if (status === 'OK') {
                    thisClass.directionsDisplay.setDirections(response);
                } else {
                    alert('Could not display directions due to: ' + status);
                }
            });


            this.directionsDisplay.setMap(this.map);

        }
    }


    onClickCenterDetails(center) {
        if (this.gVariable.platformBrowser) {
            this.setStorageEmpty();
            this.geoStorageSev.setSelectedCenter(center);
            this.geoStorageSev.setSelectedMapPoint({latitude: this.latitude, longitude: this.longitude});
            this.geoStorageSev.setSelectedMapZoomLevel(this.map.getZoom() || 15);

            setTimeout(() => {
                this.router.navigate(['', 'center-details']);
            },  100);

        }

    }
}
