import { Component, OnInit } from '@angular/core';
import {ExternalJs, GlobalVariable} from '../../core/com-classes';
import {AppConfig, StaticConfig} from '../../core/config';
import {makeStateKey, Title, TransferState} from '@angular/platform-browser';
import {ClientService, GeoStorageService} from '../../services';
import {Router} from '@angular/router';

declare const Object: any;

const SELECTED_CENTER = makeStateKey<string>('SELECTED_CENTER');

@Component({
  selector: 'app-center-details',
  templateUrl: './center-details.component.html',
  styleUrls: ['./center-details.component.css']
})
export class CenterDetailsComponent implements OnInit {

  public Object: any = Object;
  public appConfig: any = AppConfig;
  public staticConfig: any = StaticConfig;

  public selectedCenter: any;
  public selectedDoctor: any;
  public searchBy = '';


  constructor(public gVariable: GlobalVariable,
              private tState: TransferState,
              private geoStorageSev: GeoStorageService,
              private router: Router,
              private clientSev: ClientService,
              private externalJS: ExternalJs,
              title: Title) {
      title.setTitle(StaticConfig.pageTitle + ' - Sessions');
  }

  ngOnInit() {
    if (this.gVariable.platformBrowser) {
        this.externalJS.closeLoading();
    }

    if (this.tState.hasKey(SELECTED_CENTER)) {
      this.selectedCenter = this.tState.get(SELECTED_CENTER, 'SELECTED_CENTER');
      this.tState.remove(SELECTED_CENTER);

    } else if (! this.gVariable.platformBrowser) {
        // this.getCenterDetails();
    } else {
      this.getCenterDetails();
    }
  }


  private getCenterDetails() {

    // console.log(this.gVariable.selectedCenter);
    // console.log(this.gVariable.selectedCenter);

    // console.log(this.router.url.split('/'));
      this.selectedCenter = {};
      this.selectedDoctor = {};

      const url = this.router.url.split('/');
      this.geoStorageSev.setPreviousPath(url[1]);

      if (url[1] == 'doctor-details') {

          if (this.gVariable.selectedSearchedDoctor
              && (this.gVariable.selectedSearchedDoctor != null)
              && (this.gVariable.selectedSearchedDoctor != undefined)
              && (Object.keys(this.gVariable.selectedSearchedDoctor || {}).length > 0) ) {
              const selectedDoctor = this.gVariable.selectedSearchedDoctor;
              this.getDoctorDetails(selectedDoctor.doctorId);

          } else {
              const selectedDoctor = this.geoStorageSev.getSelectedSearchDoctor();
              this.getDoctorDetails(selectedDoctor.doctorId);

          }

          this.searchBy = 'BYDOCTOR';

          // console.log(this.selectedDoctor);

      } else {
          if (this.gVariable.selectedCenter
              && (this.gVariable.selectedCenter != null)
              && (this.gVariable.selectedCenter != undefined)
              && (Object.keys(this.gVariable.selectedCenter || {}).length > 0)) {
                    this.selectedCenter = this.gVariable.selectedCenter;
          } else {
              this.selectedCenter = this.geoStorageSev.getSelectedCenter();
              this.gVariable.selectedCenter = this.selectedCenter;
          }

          this.searchBy = 'BYCENTER';
          // console.log(this.selectedCenter);
      }
  }

    private getDoctorDetails(selectedDoctorId) {
        const doctorId = selectedDoctorId || 0;

        this.clientSev.getDoctorById(doctorId).then( (response: any) => {
            this.selectedDoctor = response;

        }).catch( (error: any) => {
            this.selectedDoctor = [];
        });

    }

}
