import {Component, Input, OnInit} from '@angular/core';
import {ExternalJs, GlobalVariable} from '../../core/com-classes';
import {ActivatedRoute, Router} from '@angular/router';
import {BookingCartService, SessionStorageService} from '../../services';
import {ClientService} from '../../services';
import {MessagesService} from '../../services/message.service';
import {StaticConfig} from '../../core/config';
import {Title} from '@angular/platform-browser';
import {LocalStorageService} from '../../core/services';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  @Input() parentOrderRef: any;

  public orderDetails: any = [];
  public appointmentDetailsOrder: any = [];
  public totalCharge = 0;
  public cart: any = {};
  public staticConfig: any = StaticConfig;
  public orderRef: any;
  public paymentMethod = '';

  constructor(private clientSev: ClientService,
              private sessionStorageSev: SessionStorageService,
              private storageSev: LocalStorageService,
              private router: Router,
              private bookingCartSev: BookingCartService,
              private acRoute: ActivatedRoute,
              private gVariable: GlobalVariable,
              private messagesSev: MessagesService,
              private externalJS: ExternalJs,
              title: Title) {

      if (!this.gVariable.platformBrowser) {

      } else {
          this.getRouterDetails();
      }

      title.setTitle(StaticConfig.pageTitle + ' - ORDER DETAIL');
  }

  ngOnInit() {
      this.orderRef = JSON.parse(localStorage.getItem('bookingEChannelCart_pk'));
      this.paymentMethod = this.storageSev.get('paymentMethod');
    if (this.gVariable.platformBrowser) {
        if (this.paymentMethod.trim() == 'HSBC') {
            this.getRouterDetailsGlobalPay(this.orderRef);
            this.externalJS.closeLoading();
        } else {
            this.getRouterDetails();
            this.externalJS.closeLoading();
        }
    }
  }

  private getRouterDetails() {
      if (this.gVariable.platformBrowser) {

          if (!this.gVariable.authentication.authorized && this.paymentMethod.trim() === 'HSBC') {
              this.acRoute.queryParams.subscribe( (params: any) => {
                  const key = params.key || null;
                  this.getAppointmentByHashKey(key);
              });
          } else if (!this.gVariable.authentication.authorized  && this.paymentMethod.trim() === 'ADDTOBILL') {
              this.getAppointmentDetails(this.orderRef.appointment.refNo);
          }
      }
  }

    private getRouterDetailsGlobalPay(appointmentResponse: any) {
        if (this.gVariable.platformBrowser) {
            this.getAppointmentByHashKey(appointmentResponse);
        }
    }

    private getAppointmentByHashKey(appointmentResponse) {
            const values = Object.values(appointmentResponse);
            if (values[5] === 'SUCCESS') {
                // this.sessionStorageSev.removeCreateSessionSelectedCenter();
                // this.sessionStorageSev.removeSessionID();
                this.messagesSev.mesageSuccess('Payment submitted successfully');
                this.orderDetails = appointmentResponse;
                this.bookingCartSev.updateAppointmentResponse(appointmentResponse).then((resp: any) => {
                    this.calculateTotalChrges();
                    this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.INIT);
                });
            } else {
                this.messagesSev.mesageError('Payment submission failed, Try again');
                this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.PAYMENT);
                this.router.navigate(['', 'appointment-book', 'payment']);

            }
    }


  private calculateTotalChrges() {

      if (this.orderDetails && this.orderDetails.appointmentCharges) {

          const appointmentCharges = this.orderDetails.appointmentCharges;
          this.totalCharge = 0;
          this.orderDetails.total = 0;
          const onlineApplicableAppointmentCharges = [];
          const otherAppointmentCharges = [];

          for (let i = 0; i < appointmentCharges.length; i++) {

              if (!this.gVariable.authentication.authorized) {
                  if (appointmentCharges[i].onlinePayable === 'YES') {
                      if ( (appointmentCharges[i].onlineViewable) && (appointmentCharges[i].onlineViewable !== 'false') && (appointmentCharges[i].onlineViewable !== 'NO')) {
                          onlineApplicableAppointmentCharges.push(appointmentCharges[i]);
                      }
                  } else if ((appointmentCharges[i].onlinePayable !== 'YES') && (appointmentCharges[i].onlinePayable !== true)) {
                      if (appointmentCharges[i].onlineViewable && (appointmentCharges[i].onlineViewable !== 'false') && (appointmentCharges[i].onlineViewable !== 'NO')) {
                          otherAppointmentCharges.push(appointmentCharges[i]);
                      }
                  }
              } else {
                  if (appointmentCharges[i].onlinePayable === 'YES') {
                      if ( (appointmentCharges[i].onlineViewable) && (appointmentCharges[i].onlineViewable !== 'false') && (appointmentCharges[i].onlineViewable !== 'NO')) {
                          onlineApplicableAppointmentCharges.push(appointmentCharges[i]);
                      }
                  } else if ((appointmentCharges[i].onlinePayable !== 'YES')  && (appointmentCharges[i].onlinePayable !== true)) {
                      otherAppointmentCharges.push(appointmentCharges[i]);

                  }
              }

          }

          if (!this.gVariable.authentication.authorized) {
              if (onlineApplicableAppointmentCharges.length > 0) {
                  this.orderDetails.appointmentCharges = onlineApplicableAppointmentCharges;
              }

          } else {
              if (otherAppointmentCharges.length > 0) {
                  this.orderDetails.appointmentCharges = otherAppointmentCharges;
              }
          }

          for (const i in this.orderDetails.appointmentCharges) {
              this.totalCharge = this.totalCharge + this.orderDetails.appointmentCharges[i].amount;
          }
      }
  }


    private getAppointmentDetails(RefNo) {
        if (RefNo > 0) {
            this.clientSev.getAppointmentByRefNo(RefNo).then( (appontmentResponse: any) => {
                // console.log(appontmentResponse);
                if (appontmentResponse.refNo) {
                    this.orderDetails = appontmentResponse;
                    this.bookingCartSev.updateAppointmentResponse(appontmentResponse).then((resp: any) => {
                        this.calculateTotalChrges();
                    });
                }
            }).catch( (error: any) => {
            });
        }
    }

    onPrintReceipt() {
        window.print();
    }

}
