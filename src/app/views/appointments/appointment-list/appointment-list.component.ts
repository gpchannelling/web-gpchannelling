import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ClientService, BookingCartService, GuardService, AuthenticationService} from '../../../services';
import {AppConfig, StaticConfig} from '../../../core/config';
import {Router} from '@angular/router';
import {MessagesService} from '../../../services/message.service';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import {LowerCasePipe} from '@angular/common';
import {Title} from '@angular/platform-browser';

declare const Object: any;

@Component({
  selector: 'app-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.css'],

})
export class AppointmentListComponent implements OnInit {

  @Input() selectedSessionId: any;
  @Input() selectedSessObj: any;
  @Output() appointmentViewed: EventEmitter<any> = new EventEmitter<any>();

  public appConfig: any = AppConfig;
  public staticConfig: any = StaticConfig;

  public appointmentList: any = [];
  public selectedSessionObj: any = [];
  public statusTypes: any = StaticConfig.statusTypes;
  public appointmentsStatusList: any = StaticConfig.appointmentsStatusList;
  public msgs: any = [];
  public selectedBookingObj: any = {};
  public Object = Object;
  public appointmentStatusList: any = [];
  public filterByAppointmentNo: any;
  public filterByStatus: any;

  public sessionEnded: boolean;
  public sessionStarted: boolean;
  public currentTime: Date;
  public appointmentEndTime: Date;
  public appointmentStartTime: Date;
  public appointmentEndHour: number;
  public appointmentStartHour: number;
  public appointmentEndMinute: number;
  public appointmentStartMinute: number;

  constructor(private clientSev: ClientService,
              private bookingCartSev: BookingCartService,
              private router: Router,
              private messagesSev: MessagesService,
              public gVariable: GlobalVariable,
              private guardSev: GuardService,
              public authSev: AuthenticationService,
              private externalJS: ExternalJs) {

      if (this.gVariable.platformBrowser) {
          const auth = this.guardSev.getAuthentication();
          if (!auth || !auth.authorized ||
              (!authSev.checkEntitlementAvailability([
                  this.staticConfig.entitlementsList.createAppoinment_doctor.id,
                  this.staticConfig.entitlementsList.viewAppoinment_doctor.id,
                  this.staticConfig.entitlementsList.editAppoinment_doctor.id,
                  this.staticConfig.entitlementsList.createAppointment_CenterAdmin.id,
                  this.staticConfig.entitlementsList.viewAppointment_CenterAdmin.id,
                  this.staticConfig.entitlementsList.updateAppointment_CenterAdmin.id]))) {
              this.messagesSev.mesageError('You do not have permission');
              this.router.navigate(['']);
          }
      }

  }

  ngOnInit() {
      if (this.gVariable.platformBrowser) {
          this.externalJS.closeLoading();
          this.getAppointmentsList(this.selectedSessionId);
          this.getSessionData(this.selectedSessionId);

          this.loadStatusFilterList();
      }
      this.currentTime = new Date();
      this.appointmentStartHour = parseInt(this.selectedSessObj.startTime);
      if (this.selectedSessObj.endTime.substr(6, 2) === 'AM') {
          this.appointmentEndHour = parseInt(this.selectedSessObj.endTime);
      } else {
          this.appointmentEndHour = parseInt(this.selectedSessObj.endTime) + 12;
      }
      this.appointmentStartMinute = parseInt(this.selectedSessObj.startTime.substr(3, 2));
      this.appointmentEndMinute = parseInt(this.selectedSessObj.endTime.substr(3, 2));
      // tslint:disable-next-line:max-line-length
      this.appointmentStartTime = new Date(new Date(this.selectedSessObj.appointmentDate).setHours( this.appointmentStartHour, this.appointmentStartMinute, 0, 0));
      this.appointmentEndTime = new Date(new Date(this.selectedSessObj.appointmentDate).setHours( this.appointmentEndHour, this.appointmentEndMinute, 0, 0));
      if (this.currentTime > this.appointmentStartTime) {
          this.sessionStarted = true;
      }
      if (this.currentTime > this.appointmentEndTime) {
          this.sessionEnded = true;
      }
  }

  private loadStatusFilterList() {
      for (let i in this.staticConfig.appointmentsStatusList) {
          this.appointmentStatusList.push({
              name: this.staticConfig.appointmentsStatusList[i],
              value: this.staticConfig.appointmentsStatusList[i]
          });
      }
  }

  private getAppointmentsList(sessionId) {
    this.clientSev.getAppointmentsListBySessionId(sessionId).then( (response: any) => {
      // console.log(response);
      this.appointmentList = response;
    }).catch( (error: any) => {

    });
  }

  private getSessionData(sessionId) {
      this.selectedSessionObj = this.selectedSessObj;
    // this.clientSev.getSessionDataByID(sessionId).then( (response: any) => {
    //   // console.log(response);
    //   this.selectedSessionObj = response;
    // }).catch( (error: any) => {
    //   console.log(error);
    // });
  }

  onClickAppointmentCompleted(appointment) {

    const appointmentStatusUpdateReq = {
        changeType: this.statusTypes.APPOINTMENT_STATUS,
        refNo: appointment.refNo,
        status: this.appointmentsStatusList.VISITED
    };

    this.clientSev.changeAppointmentStatus(appointmentStatusUpdateReq).then( (response: any) => {
      // console.log(response);
        if (response.status === 200) {

            this.messagesSev.mesageSuccess('Status updated successfully')
            this.appointmentViewed.emit(true);
            this.getAppointmentsList(this.selectedSessionId);
            this.getSessionData(this.selectedSessionId);

            this.selectedSessionObj.currentPatient = this.selectedSessionObj.currentPatient + 1;

            const req = {
                currentPatientNo: this.selectedSessionObj.currentPatient,
                sessionId: this.selectedSessionId
            };

            this.clientSev.addAppointmentViewed(req).then( (Incrementresponse: any) => {
                if (Incrementresponse.status === 200) {

                } else {
                    this.messagesSev.mesageError('Failed, ' + Incrementresponse.error.message + ' error');
                }
            }).catch( (error: any) => {
                this.messagesSev.mesageError('Failed, ' + error.error.message + ' error');
            });


        } else {
            this.messagesSev.mesageError('Failed');
        }



    }).catch( (error: any) => {
        this.messagesSev.mesageError('Failed, ' + error.error.message + ' error');
    });
  }

  onClickInActiveAppointment(appointment) {
      // console.log(appointment);

      const appointmentStatusUpdateReq = {
          changeType: this.statusTypes.APPOINTMENT_STATUS,
          refNo: appointment.refNo,
          status: this.appointmentsStatusList.INACTIVE
      };

      this.clientSev.changeAppointmentStatus(appointmentStatusUpdateReq).then( (response: any) => {
          if (response.status === 200) {
              this.messagesSev.mesageSuccess('Updated successfully');
              this.appointmentViewed.emit(true);
              this.getAppointmentsList(this.selectedSessionId);
              this.getSessionData(this.selectedSessionId);
          } else {
              this.messagesSev.mesageError('Failed, ' + response.error.message + ' error');
          }
      });
  }

    onClickPayAppointment(selectedBookingObj) {
        this.selectedBookingObj = selectedBookingObj;
    }

  onClickNewAppointment(sessionId) {
        this.bookingCartSev.initCart({selectedSessionId: sessionId});

        setTimeout(() => {
            this.router.navigate(['', 'appointment-booking', sessionId]);
        }, 100);

  }

  onClickBack() {
      this.selectedBookingObj = {};
      this.getAppointmentsList(this.selectedSessionId);
  }

}
