import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AppConfig, StaticConfig} from '../../../core/config';
import {AuthenticationService, ClientService, GuardService} from '../../../services';
import {MessagesService} from '../../../services/message.service';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';

declare const $: any;

@Component({
  selector: 'app-appointment-view',
  templateUrl: './appointment-view.component.html',
  styleUrls: ['./appointment-view.component.css']
})
export class AppointmentViewComponent implements OnInit {

    @Input() selectedBookingObj: any;
    @Input() selectedSessionObj: any;
    @Output() clickBackToList: EventEmitter<any> = new EventEmitter<any>();
    @Output() permissionDenied: EventEmitter<any> = new EventEmitter<any>();
    public appConfig: any = AppConfig;
    public staticConfig: any = StaticConfig;
    public submitPending = false;
    public paymentSubmitted = false;

    public dateOption: any = {
        maxDate : new Date()
    };

    public bookingObj: any = {
        appointmentCharges: []
    };
    public booking: any = {
        appointmentCharges: [],
        otherAppointmentCharges: [],
        total: 0
    };
    public feesList: any = [];
    public selectedCharge: any = {};

  constructor(private clientSev: ClientService,
              private messageSev: MessagesService,
              public gVariable: GlobalVariable,
              public authSev: AuthenticationService,
              private router: Router,
              private guardSev: GuardService,
              private externalJS: ExternalJs) {
      if (this.gVariable.platformBrowser) {
          const auth = this.guardSev.getAuthentication();
          if (!auth || !auth.authorized ||
              (!authSev.checkEntitlementAvailability([
                  this.staticConfig.entitlementsList.viewAppoinment_doctor.id,
                  this.staticConfig.entitlementsList.editAppoinment_doctor.id,
                  this.staticConfig.entitlementsList.viewAppointment_CenterAdmin.id,
                  this.staticConfig.entitlementsList.updateAppointment_CenterAdmin.id]))) {
              this.messageSev.mesageError('You do not have permission');
              this.permissionDenied.emit(true);
          }
      }

  }

  ngOnInit() {
      if (this.gVariable.platformBrowser) {
          this.externalJS.closeLoading();
          this.getFeesList();
          // this.bookingObj.sessionFees = this.selectedSessionObj.sessionFees;
          this.getAppointmentCharges(this.selectedBookingObj.refNo);
      }

  }

    private getFeesList() {
        this.clientSev.getFeesList().then( (response: any) => {
            // console.log(response);
            if (response.status !== 0) {
                this.feesList = response;
                // console.log(this.selectedSessionObj);
                // console.log(this.selectedBookingObj);
            } else {
                this.feesList = [];
            }

        }).catch( (error: any) => {
            this.feesList = [];
        });
    }

    private getAppointmentCharges(refNo) {
        if (refNo > 0) {
            this.clientSev.getAppointmentByRefNoForPaymentUpdate(refNo).then( (appontmentResponse: any) => {
                // console.log(appontmentResponse);
                if (appontmentResponse.refNo) {
                    this.bookingObj = appontmentResponse;
                    this.booking = appontmentResponse.patient;
                    // this.booking.appointmentCharges = appontmentResponse.appointmentCharges;
                    // console.log(this.booking);


                    const appointmentCharges = appontmentResponse.appointmentCharges;
                    const otherAppointmentCharges = [];
                    this.booking.total = 0;

                    for (let i = 0; i < appointmentCharges.length; i++) {

                        if ((!appointmentCharges[i].onlinePayable) || (appointmentCharges[i].onlinePayable && appointmentCharges[i].onlinePayable != 'YES')) {
                            otherAppointmentCharges.push(appointmentCharges[i]);
                            this.booking.total = this.booking.total + appointmentCharges[i].amount;
                            // this.totalCharge = this.totalCharge + appointmentCharges[i].amount;
                        }

                    }

                    if (otherAppointmentCharges.length > 0) {
                        this.booking.otherAppointmentCharges = otherAppointmentCharges;
                    } else {
                        // this.booking.otherAppointmentCharges = this.booking.appointmentCharges;
                    }

                } else {
                    this.messageSev.mesageError(appontmentResponse.error.message);
                }
            }).catch( (error: any) => {
            });
        }
    }


    onClickAddAmount(selectedCharge) {
        let alredyExist = false;

        for (let j in this.bookingObj.sessionFees) {

            if ( this.bookingObj.sessionFees[j].feeCode == selectedCharge.chargeType) {
                alredyExist = true;
            }
        }


        if (alredyExist == false) {
            for (let i in this.feesList) {

                if (this.feesList[i].chargeCode == selectedCharge.chargeType) {

                    this.bookingObj.sessionFees.push(
                        {
                            'feeCode' : this.feesList[i].chargeCode,
                            'amount' : selectedCharge.chargeAmount,
                            'description' : this.feesList[i].description
                        }
                    );

                    this.selectedCharge = {};

                }
            }

        } else {
            this.messageSev.mesageError('Already Exist');
        }


    }

    onClickRemoveCharge(chargeCode) {
        for (let i in this.bookingObj.sessionFees) {
            if (this.bookingObj.sessionFees[i].feeCode === chargeCode) {
                this.bookingObj.sessionFees.splice(i, 1);
            }
        }

        // console.log(this.sessionObj.sessionFees);
    }

    onClickEditCharge(chargeCode) {
        for (let i in this.bookingObj.sessionFees) {
            if (this.bookingObj.sessionFees[i].feeCode === chargeCode) {
                // this.bookingObj.sessionFees.splice(i, 1);
            }
        }

        // console.log(this.sessionObj.sessionFees);
    }

    onClickBackToList() {
      this.clickBackToList.emit();
    }

    onClickSubmitPayment(paymentObj) {
      this.submitPending = true;
      let total = 0;

      let req = {
          refNo: this.bookingObj.refNo,
          appointmentCharges: []
      };

      for (let i in paymentObj.otherAppointmentCharges) {
          total += paymentObj.otherAppointmentCharges[i].amount;

          req.appointmentCharges.push({
              amount: paymentObj.otherAppointmentCharges[i].amount,
              chargeCode: paymentObj.otherAppointmentCharges[i].chargeCode
          });
      }

      if (total < 1) {
          this.messageSev.mesageError('Enter a valid amount.');
          this.submitPending = false;
      } else {
          // console.log(req);
          this.clientSev.updateAppointmentPayment(req).then( (response: any) => {
              // console.log(response);
              if (response.status == 200) {
                  this.messageSev.mesageSuccess('Udated Successfully');
                  this.getAppointmentDetails(this.selectedBookingObj.refNo);
                  this.submitPending = false;
                  // this.onClickBackToList();
              } else {
                  this.submitPending = false;
                  this.messageSev.mesageError('Failed to update.');
              }
          }).catch( (error: any) => {
              // console.log(error);
              this.submitPending = false;
              this.messageSev.mesageError('Failed to update.');
          });
      }



    }

    private getAppointmentDetails(refNo) {

        if (refNo > 0) {
            this.clientSev.getAppointmentByRefNo(refNo).then( (appontmentResponse: any) => {
                // console.log(appontmentResponse);

                if (appontmentResponse.refNo) {
                    this.messageSev.closeMessage();
                    this.bookingObj = appontmentResponse;
                    this.booking = appontmentResponse.patient;
                    this.booking.appointmentCharges = appontmentResponse.appointmentCharges;

                    const appointmentCharges = appontmentResponse.appointmentCharges;
                    const otherAppointmentCharges = [];
                    this.booking.total = 0;

                    for (let i = 0; i < appointmentCharges.length; i++) {

                        if ((!appointmentCharges[i].onlinePayable) || (appointmentCharges[i].onlinePayable && appointmentCharges[i].onlinePayable != 'YES')) {
                            otherAppointmentCharges.push(appointmentCharges[i]);
                            this.booking.total = this.booking.total + appointmentCharges[i].amount;
                            // this.totalCharge = this.totalCharge + appointmentCharges[i].amount;
                        }

                    }

                    if (otherAppointmentCharges.length > 0) {
                        this.booking.otherAppointmentCharges = otherAppointmentCharges;
                    } else {
                        // this.booking.otherAppointmentCharges = this.booking.appointmentCharges;
                    }


                    this.paymentSubmitted = true;
                    // console.log(this.booking);
                } else {
                    this.messageSev.mesageError(appontmentResponse.error.message);
                }
            }).catch( (error: any) => {
            });
        }

    }

    onChangeCalculateTotal(chargesList) {
        this.booking.total = 0;
        for (let i = 0; i < chargesList.length; i++) {
            this.booking.total = this.booking.total + chargesList[i].amount;
        }

    }

    onPrintReceipt() {
        $('.mat-tab-label-container').css('display', 'none');
        window.print();
        $('.mat-tab-label-container').css('display', 'block');
    }
}
