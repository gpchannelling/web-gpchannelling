import {Component, Input, OnInit} from '@angular/core';
import {Message} from 'primeng/api';
import {ClientService, GeoStorageService, SessionStorageService} from '../../../services';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import { Router, ActivatedRoute } from '@angular/router';
import {DatePipe, Location} from '@angular/common';
import {BookingCartService} from '../../../services';
import {AppConfig, StaticConfig} from '../../../core/config';
import {MAT_STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MessagesService} from '../../../services/message.service';
import {Title} from '@angular/platform-browser';

declare const Object: any;

@Component({
  selector: 'app-appointment-booking',
  templateUrl: './appointment-booking.component.html',
  styleUrls: ['./appointment-booking.component.css'],
  providers: [{
    provide: MAT_STEPPER_GLOBAL_OPTIONS,
    useValue: {displayDefaultIndicatorType: false}
  }]
})
export class AppointmentBookingComponent implements OnInit {

    public appConfig: any = AppConfig;
    public staticConfig: any = StaticConfig;

  public booking: any = {};
  public msg: Message[] = [];
  public sessionId: number;
  public refNo: number;
  public sessionFees: any;
  public appointmentDetails: any = {};
  public selectedSessionData: any = [];
  public step: any;
  public bookingObj: any = {
      sessionFees: []
  };
  public dateOption: any = {
      maxDate : new Date()
  };
  public feesList: any = [];
  public selectedCharge: any = {};
  public timeNow = new Date();
  public cart: any = {};
  public pagetitle = 'Appointment Booking';
  public printOrder = false;
  public requestPending = false;
  public stepperIndex = 0;
  public Object = Object;

    firstFormGroup: FormGroup;
    secondFormGroup: FormGroup;

  constructor(private router: Router,
              private localStorage: SessionStorageService,
              private datePipe: DatePipe,
              private activatedRoute: ActivatedRoute,
              private clientSev: ClientService,
              public gVariable: GlobalVariable,
              private bookingCartSev: BookingCartService,
              private _formBuilder: FormBuilder,
              private messagesSev: MessagesService,
              private _location: Location,
              private geoStorageSev: GeoStorageService,
              private externalJS: ExternalJs,
              title: Title) {
      if (this.gVariable.platformBrowser) {
          this.getRouterParams();
      }

      title.setTitle(StaticConfig.pageTitle + ' - APPOINTMENT BOOKING');

  }

  ngOnInit() {

      if (this.gVariable.platformBrowser) {
          this.externalJS.closeLoading();
          this.getFeesList();
          this.getRouterParams();
      }
  }

  private onClickBackToHome() {
      this.bookingCartSev.removeCart();
      const previousRoute = this.geoStorageSev.getPreviousPath();
      if (this.gVariable.authentication.authorized) {
          this.router.navigate(['session-list']);
      } else {
          if (previousRoute) {
              this.router.navigate(['', previousRoute]);
          } else {
              this.router.navigate(['']);
          }
      }
  }

  onCancelBooking() {
      this.bookingCartSev.removeCart();
      const previousRoute = this.geoStorageSev.getPreviousPath();
      if (this.gVariable.authentication.authorized) {
          this.router.navigate(['session-view/' + this.sessionId]);
      } else {
          if (previousRoute) {
              this.router.navigate(['', previousRoute]);
          } else {
              this.router.navigate(['']);
          }

      }
  }

  private getRouterParams() {
      this.activatedRoute.params.subscribe((data: any) => {

          this.cart = this.bookingCartSev.getBookingCart();

          this.step = this.activatedRoute.snapshot.url[1].path;
          if (this.activatedRoute.snapshot.url[0].path === 'appointment-booking' && this.activatedRoute.snapshot.url[1].path) {

              this.localStorage.setSessionID(this.activatedRoute.snapshot.url[1].path);
              this.router.navigate(['', 'appointment-book', 'booking']);
          }
          this.sessionId = this.localStorage.getSessionID();
          // console.log(this.cart);
          this.getSessionData(this.sessionId);
          this.validateRoute();

          this.init();

      });

  }

  private validateRoute() {

      if ((this.cart.step == this.staticConfig.appoinmentBookingStages.INIT) && (this.activatedRoute.snapshot.url[1].path != 'booking')) {
          this.router.navigate(['', 'appointment-book', 'booking']);
      }  else if ((this.cart.step == this.staticConfig.appoinmentBookingStages.EDIT) && (this.activatedRoute.snapshot.url[1].path != 'edit-booking')) {
          this.getAppointmentData();
          this.router.navigate(['', 'appointment-book', 'edit-booking']);
      } else if ((this.cart.step == this.staticConfig.appoinmentBookingStages.COMFIRM) && (this.activatedRoute.snapshot.url[1].path != 'confirm')) {
          this.getAppointmentData();
          this.router.navigate(['', 'appointment-book', 'confirm']);
      } else if ((this.cart.step == this.staticConfig.appoinmentBookingStages.PAYMENT) && (this.activatedRoute.snapshot.url[1].path != 'payment')) {
          this.router.navigate(['', 'appointment-book', 'payment']);
      } else if ((this.cart.step == this.staticConfig.appoinmentBookingStages.DETAILS) && (this.activatedRoute.snapshot.url[1].path != 'order-details')) {
          this.getAppointmentData();
          this.router.navigate(['', 'appointment-book', 'order-details']);
      }
  }

  private init() {
      if (this.cart.step == this.staticConfig.appoinmentBookingStages.INIT) {
          this.pagetitle = 'Appointment Booking';
          this.bookingObj.sessionFees = [];
          this.booking.dateOfBirth = null;
          this.booking.mobileNumber = null;
          this.booking.patientName = null;
          this.booking.nic = null;
          this.stepperIndex = 0;

      } else if (this.cart.step == this.staticConfig.appoinmentBookingStages.EDIT) {

          this.pagetitle = 'Appointment Edit';
          this.bookingObj.sessionFees = this.cart.appointment.request.appointmentCharges || [];
          this.booking.dateOfBirth = this.cart.appointment.request.patient.dateOfBirth ? new Date(this.datePipe.transform(this.cart.appointment.request.patient.dateOfBirth, 'yyyy-MM-dd')) : null;
          this.booking.mobileNumber = this.cart.appointment.request.patient.mobileNo || null;
          this.booking.patientName = this.cart.appointment.request.patient.name || null;
          this.booking.nic = this.cart.appointment.request.patient.nic || null;
          this.stepperIndex = 0;


      } else if (this.cart.step == this.staticConfig.appoinmentBookingStages.COMFIRM) {
          this.pagetitle = 'Appointment Confirm';
          this.stepperIndex = 1;


      } else if (this.cart.step == this.staticConfig.appoinmentBookingStages.DETAILS) {
          this.pagetitle = 'Order Details';
          this.stepperIndex = 1;


      } else if (this.cart.step == this.staticConfig.appoinmentBookingStages.PAYMENT) {
          this.pagetitle = 'Payment';
          this.stepperIndex = 2;

      }
  }

    onSubmitBookingForm(formData, stepper) {
      // console.log(formData);
      // console.log(this.cart.step);
      // console.log(stepper);

        this.requestPending = true;

        if (this.cart.step == this.staticConfig.appoinmentBookingStages.INIT) {
            if (this.selectedSessionData.noOfPatient > this.selectedSessionData.noOfReservedPatient) {
                const saveRequest = {
                    'appointmentCharges': [],
                    'patient': {
                        'dateOfBirth': this.datePipe.transform(this.booking.dateOfBirth, 'yyyy-MM-dd'),
                        'mobileNo': this.booking.mobileNumber,
                        'name': this.booking.patientName,
                        'nic': this.booking.nic,
                    },
                };

                this.bookingCartSev.updateBookingReqData({
                    'appointmentCharges': saveRequest.appointmentCharges,
                    'patient': this.booking,
                });

                this.clientSev.bookAppointment(saveRequest, this.sessionId).then( (response: any) => {
                    if (response.status == 200) {

                        // this.messagesSev.mesageSuccess('Appointment booking is success.');
                        this.requestPending = false;
                        this.getAppointmentDetails(response.data.refNo);
                        this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.COMFIRM);

                        setTimeout(() => {
                            this.router.navigate(['', 'appointment-book', 'confirm']);
                        }, 100);



                    } else {
                        this.messagesSev.mesageError('Booking failed due to, ' + response.error.message + ' error');
                        this.requestPending = false;

                    }
                }).catch( (error: any) => {
                    this.messagesSev.mesageError('Booking failed due to, ' + error.error.message + ' error');
                    this.requestPending = false;

                });

            } else {
                this.messagesSev.mesageError('Maximum amount of appointments reached.');
            }

        } else if (this.cart.step == this.staticConfig.appoinmentBookingStages.EDIT) {

            const saveRequest = {
                'appointmentCharges': [],
                'patient': {
                    'dateOfBirth': this.datePipe.transform(this.booking.dateOfBirth, 'yyyy-MM-dd'),
                    'mobileNo': this.booking.mobileNumber,
                    'name': this.booking.patientName,
                    'nic': this.booking.nic,
                },
                'refNo': this.cart.appointment.response.refNo
            };

            this.bookingCartSev.updateBookingReqData({
                'appointmentCharges': saveRequest.appointmentCharges,
                'patient': this.booking,
                'refNo': this.cart.appointment.response.refNo
            });

            this.clientSev.updateAppointment(saveRequest).then( (response: any) => {
                if (response.status == 200) {

                    // this.messagesSev.mesageSuccess('Appointment update is success.');
                    this.requestPending = false;

                    this.getAppointmentDetails(saveRequest.refNo);
                    this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.COMFIRM);

                    setTimeout(() => {
                        this.router.navigate(['', 'appointment-book', 'confirm']);
                    }, 100);

                } else {
                    this.messagesSev.mesageError('Booking failed due to, ' + response.error.message + ' error');
                    this.requestPending = false;

                }
            }).catch( (error: any) => {
                this.messagesSev.mesageError('Booking failed due to, ' + error.error.message + ' error');
                this.requestPending = false;

            });
        }

    }

    onClickConfirmAppointment() {

        if (!this.gVariable.authentication.authorized) {
            this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.PAYMENT);
            setTimeout(() => {
                this.router.navigate(['', 'appointment-book', 'payment']);
            }, 100);

        } else {
            this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.INIT);
            // this.router.navigate(['', 'appointment-book', 'order-details']);
            setTimeout(() => {
                this.onClickBackToHome();
            }, 100);

        }

    }

    onClickEdit() {
      this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.EDIT);
        setTimeout(() => {
            this.router.navigate(['', 'appointment-book', 'edit-booking']);
        }, 100);

    }

    private getSessionData(selectedSessionId) {
        this.clientSev.getSessionDataByID(selectedSessionId).then( (response: any) => {
            // console.log(response);
            // if (response.noOfPatient > response.noOfReservedPatient) {
                let alredyExist = false;
                this.selectedSessionData = response;
                this.dateOption.maxDate = this.selectedSessionData.appointmentDate;

                for (const i in response.sessionFees) {
                    for (const j in this.bookingObj.sessionFees) {
                        if ( this.bookingObj.sessionFees[j].chargeCode == response.sessionFees[i].feeCode) {
                            alredyExist = true;
                        }
                    }

                    if (alredyExist == false) {
                        let description = '';
                        for (const i in this.feesList) {
                            if (this.feesList[i].chargeCode === this.selectedCharge.chargeType) {
                                description = this.feesList[i].description;
                            }
                        }
                        this.bookingObj.sessionFees.push({
                            'chargeCode' : response.sessionFees[i].feeCode,
                            'amount' : response.sessionFees[i].amount,
                            'description': description !== '' ? description : response.sessionFees[i].feeCode
                        });
                    }

                }
            // } else {
            //     this.router.navigate(['']);
            // }






        }).catch( (error: any) => {
          console.log(error);
        });
    }

  private getAppointmentData() {
      const appointmentRef = this.cart.appointment.response.refNo || 0;
      if (appointmentRef > 0) {
          this.getAppointmentDetails(appointmentRef);
      }

  }
  private getFeesList() {
      this.clientSev.getFeesList().then( (response: any) => {
          // console.log(response);
          if (response.status !== 0) {
              this.feesList = response;
          } else {
              this.feesList = [];
          }

      }).catch( (error: any) => {
          this.feesList = [];
      });
  }

  onSelectChargeType() {

  }

  onClickAddAmount() {
        let alredyExist = false;
        for (const i in this.feesList) {
            if (this.feesList[i].chargeCode === this.selectedCharge.chargeType) {
                for (const j in this.bookingObj.sessionFees) {
                    if ( this.bookingObj.sessionFees[j].chargeCode == this.feesList[i].chargeCode) {
                        // this.bookingObj.sessionFees[j].amount = this.selectedCharge.chargeAmount;
                        // this.bookingObj.sessionFees[j].description = this.feesList[i].description;
                        alredyExist = true;
                    }
                }
                if (alredyExist == false) {
                    this.bookingObj.sessionFees.push(
                        {
                            'chargeCode' : this.feesList[i].chargeCode,
                            'amount' : this.selectedCharge.chargeAmount,
                            'description' : this.feesList[i].description
                        }
                    );
                }
            }
        }

    }

    onClickEditSelectedFee(fee) {
     this.selectedCharge.chargeType = fee.chargeCode;
     this.selectedCharge.chargeAmount = fee.amount;
   }


    private getAppointmentDetails(RefNo) {

      if (RefNo > 0) {
          this.clientSev.getAppointmentByRefNo(RefNo).then( (appontmentResponse: any) => {

              if (appontmentResponse.refNo) {

                  this.bookingCartSev.updateAppointmentResponse(appontmentResponse).then((resp: any) => {
                      this.cart = this.bookingCartSev.getBookingCart();
                      this.appointmentDetails = appontmentResponse;
                      const appointmentCharges = this.appointmentDetails.appointmentCharges;
                      this.appointmentDetails.total = 0;
                      const onlineApplicableAppointmentCharges = [];
                      const otherAppointmentCharges = [];

                      for (let i = 0; i < appointmentCharges.length; i++) {

                          if (!this.gVariable.authentication.authorized) {
                              if (appointmentCharges[i].onlinePayable == 'YES') {
                                  this.appointmentDetails.total += appointmentCharges[i].amount;
                                  if ( (appointmentCharges[i].onlineViewable)
                                      && (appointmentCharges[i].onlineViewable !== 'false')
                                      && (appointmentCharges[i].onlineViewable !== 'NO')) {
                                      onlineApplicableAppointmentCharges.push(appointmentCharges[i]);
                                  }
                              } else if (appointmentCharges[i].onlinePayable == 'NO') {
                                  if (appointmentCharges[i].onlineViewable
                                      && (appointmentCharges[i].onlineViewable !== 'false')
                                      && (appointmentCharges[i].onlineViewable !== 'NO')) {
                                      otherAppointmentCharges.push(appointmentCharges[i]);
                                  }
                              }
                          } else {
                              if (appointmentCharges[i].onlinePayable == 'YES') {

                                  if ( (appointmentCharges[i].onlineViewable)
                                      && (appointmentCharges[i].onlineViewable !== 'false')
                                      && (appointmentCharges[i].onlineViewable !== 'NO')) {
                                      onlineApplicableAppointmentCharges.push(appointmentCharges[i]);
                                  }
                              } else if (appointmentCharges[i].onlinePayable == 'NO') {
                                  this.appointmentDetails.total += appointmentCharges[i].amount;
                                  otherAppointmentCharges.push(appointmentCharges[i]);
                              }
                          }

                          if (onlineApplicableAppointmentCharges.length > 0) {
                              this.appointmentDetails.onlineApplicableAppointmentCharges = onlineApplicableAppointmentCharges;
                          } else {
                              this.appointmentDetails.onlineApplicableAppointmentCharges = [];
                          }


                          if (otherAppointmentCharges.length > 0) {
                              this.appointmentDetails.otherAppointmentCharges = otherAppointmentCharges;
                          } else {
                              this.appointmentDetails.otherAppointmentCharges = [];
                          }



                          this.appointmentDetails.chargeDescription = this.selectedSessionData.chargeDescription;
                          // console.log(this.selectedSessionData);

                          this.requestPending = false;
                          this.validateRoute();

                      }


                  });

              }


          }).catch( (error: any) => {
          });
      }

    }

    onPrintReceipt() {
        this.printOrder = true;
        setTimeout(() => {
            this.printOrder = false;
        }, 200);
        // window.print();
    }

}
