import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { ViewsRoutingModule } from './views-routing.module';
import { HomeComponent } from './home/home.component';
import { P404Component } from './404/404.component';
import { MyDashboardComponent } from './auth/my-dashboard/my-dashboard.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { LoginComponent } from './auth/login/login.component';
import { SessionListComponent } from './sessions/session-list/session-list.component';
import {AgmCoreModule} from '@agm/core';
import { CenterDetailsComponent } from './center-details/center-details.component';
import { GmapComponent } from './gmap/gmap.component';
import { SessionViewComponent } from './sessions/session-view/session-view.component';
import { SessionComponent } from './sessions/session/session.component';
import { PaymentViewComponent } from './payment-view/payment-view.component';
import { ChangePasswordComponent } from './auth/change-password/change-password.component';
import { UpdateProfileComponent } from './auth/update-profile/update-profile.component';
import { AppointmentListComponent } from './appointments/appointment-list/appointment-list.component';
import { AppointmentBookingComponent } from './appointments/appointment-booking/appointment-booking.component';
import { AppointmentViewComponent } from './appointments/appointment-view/appointment-view.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { ButtonModule } from 'primeng/button';
import { SliderModule } from 'primeng/slider';
import { AgmDirectionModule } from 'agm-direction';

import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import {AccordionModule, CalendarModule, SidebarModule} from 'primeng/primeng';
import {ToastModule} from 'primeng/toast';
import {ResetPasswordComponent} from './auth/change-password/reset-password.component';
import {DialogModule} from 'primeng/dialog';
import { SessionScheduleComponent } from './sessions/session-schedule/session-schedule.component';
import {AgmSnazzyInfoWindowModule} from '@agm/snazzy-info-window';
import { IncomeReportComponent } from './reports/income-report/income-report.component';
import { GoogleChartsModule } from 'angular-google-charts';
import {SidebarComponent} from '../containers/sidebar/sidebar.component';
import {
    MatAutocomplete,
    MatAutocompleteModule,
    MatButtonModule,
    MatCheckbox,
    MatCheckboxModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule, MatRadioModule,
    MatRippleModule,
    MatSelect,
    MatSelectModule,
    MatSnackBarModule,
    MatStepperModule,
    MatTabsModule,
    MatTooltipModule
} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { OrderDetailsComponent } from './order-details/order-details.component';
import {AppointmentsFilter} from '../pipes/AppointmentsFilter.pipe';
import {AppointmentsStatusFilter} from '../pipes/AppointmentsStatusFilter.pipe';
import { TokenDetailsComponent } from './token-details/token-details.component';
import { AddToBillComponent } from './add-to-bill/add-to-bill.component';
import { PaymentConfirmComponent } from './payment-confirm/payment-confirm.component';
import { PaymentCompleteComponent } from './payment-complete/payment-complete.component';
import { SelfRegistrationComponent } from './self-registration/self-registration.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import {ConfirmEqualValidatorDirective} from '../directives/confirm-equal-validator.directive';




@NgModule({
    imports: [
        CommonModule,
        ViewsRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        BsDatepickerModule.forRoot(),
        TimepickerModule.forRoot(),
        GoogleChartsModule.forRoot(),
        PaginationModule.forRoot(),
        AgmCoreModule.forRoot(
            {
                apiKey: 'AIzaSyDsDEf93Xd0ws0b1bL6fjP6W2vT-yvCU5Y',
                libraries: ['places', 'geocoder', 'geometry']
            }
        ),
        AgmSnazzyInfoWindowModule,
        ButtonModule,
        SliderModule,
        AgmDirectionModule,
        MessagesModule,
        MessageModule,
        CalendarModule,
        ToastModule,
        DialogModule,
        AccordionModule,
        MatFormFieldModule,
        MatButtonModule,
        MatRippleModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSnackBarModule,
        MatDialogModule,
        NgxMaterialTimepickerModule.forRoot(),
        MatExpansionModule,
        MatListModule,
        MatTabsModule,
        MatStepperModule,
        MatIconModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatAutocompleteModule,
        MatRadioModule,

    ],
  declarations: [

      AppointmentsFilter,
      AppointmentsStatusFilter,
    HomeComponent,
    P404Component,
    MyDashboardComponent,
    LoginComponent,
    SessionListComponent,
    CenterDetailsComponent,
    GmapComponent,
    SessionViewComponent,
    SessionComponent,
    PaymentViewComponent,
    ChangePasswordComponent,
    UpdateProfileComponent,
    AppointmentListComponent,
    AppointmentBookingComponent,
    AppointmentViewComponent,
    RegistrationComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    SessionScheduleComponent,
    IncomeReportComponent,
    OrderDetailsComponent,
    TokenDetailsComponent,
    AddToBillComponent,
    PaymentConfirmComponent,
    PaymentCompleteComponent,
    SelfRegistrationComponent,
    UserRegisterComponent,
      ConfirmEqualValidatorDirective
  ],
})
export class ViewsModule { }
