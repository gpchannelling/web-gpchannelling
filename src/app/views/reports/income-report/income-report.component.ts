import {Component, Input, OnInit, HostListener } from '@angular/core';
import {AuthenticationService, ClientService, GeoStorageService, GuardService} from '../../../services';
import {Message} from 'primeng/api';
import {makeStateKey, Title, TransferState} from '@angular/platform-browser';
import {ExternalJs, GlobalVariable} from '../../../core/com-classes';
import {AppConfig, StaticConfig} from '../../../core/config/index';
import {DatePipe} from '@angular/common';
import { groupBy, process } from '@progress/kendo-data-query';
import {MessagesService} from '../../../services/message.service';
import {Router} from '@angular/router';

const CENTERS_LIST = makeStateKey<string>('CENTERS_LIST');
const DOCTORS_LIST = makeStateKey<string>('DOCTORS_LIST');

@Component({
  selector: 'app-income-report',
  templateUrl: './income-report.component.html',
  styleUrls: ['./income-report.component.css']
})
export class IncomeReportComponent implements OnInit {
  @Input() selectedCenterObject: any;

  public msg: Message[] = [];
  public sessionList = [];
  public doctorsList: any = [];
  public centersList: any = [];
  public income: any = [];
  public Byclinic: any = [];
  public BySession: any = [];
  public ByDoctor: any = [];
  public centerObject: any;
  public staticConfig: any = StaticConfig;
  public selectedDoctorId = 0;
  public selectedCenterId = 0;
  public rangeDates: Date[] = [new Date(), new Date()];
  screenHeight:any;
  screenWidth:any;
  /*public dateOption: any = {
    startDate : {
      minDate : new Date()
    },
    endDate : {
      minDate: this.rangeDates[0],
      viewDate: true
    }
  };*/
  public searchObject: any = {
    selectedDate: this.datePipe.transform(new Date(), 'yyyy-MM-dd'),
    selectedCenter: 'viewAll',
    selectedDoctor: 'viewAll'
  };
  chart = {
    title: '',
    type: 'PieChart',
    data: [],
    columnNames: ['Center', 'Total Appointments', 'Income'],
    options: {
      width: 600,
      height: 300,
      animation: {
        duration: 250,
        easing: 'ease-in-out',
        startup: true
      },
      legend: { position: 'left', maxLines: 2 },
        chartArea: {
            right: 20,   // set this to adjust the legend width
            left: 6,     // set this eventually, to adjust the left margin
        },
    }
  };

  constructor(private clientSev: ClientService,
              private geoStorageSev: GeoStorageService,
              private datePipe: DatePipe,
              public gVariable: GlobalVariable,
              private tState: TransferState,
              private messagesSev: MessagesService,
              private router: Router,
              private guardSev: GuardService,
              public authSev: AuthenticationService,
              private externalJS: ExternalJs,
              title: Title) {

      if (this.gVariable.platformBrowser) {
          this.externalJS.closeLoading();
          const auth = this.guardSev.getAuthentication();
          if (!auth || !auth.authorized ||
              (!authSev.checkEntitlementAvailability([
                  this.staticConfig.entitlementsList.viewIncome_doctor.id,
                  this.staticConfig.entitlementsList.viewIncome_CenterAdmin.id]))) {
              this.messagesSev.mesageError('You do not have permission');
              this.router.navigate(['']);
          }

          this.getScreenSize();
      }

      title.setTitle(StaticConfig.pageTitle + ' - REPORT');
  }

    @HostListener('window:resize', ['$event']) getScreenSize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
        // console.log(this.screenHeight, this.screenWidth);
    }

  ngOnInit() {

    if (this.tState.hasKey(DOCTORS_LIST) && this.tState.hasKey(CENTERS_LIST)) {
      this.centersList = this.tState.get(CENTERS_LIST, 'CENTERS_LIST');
      this.doctorsList = this.tState.get(DOCTORS_LIST, 'DOCTORS_LIST');
    } else if (!this.gVariable.platformBrowser) {
    } else {
      this.getSelectedCenter();
      this.getAllDoctorsList();
      this.getAllCentersList();
      // this.getSessionList();
        this.getIncomeReports();
    }

    if (this.gVariable.authentication.autherized && (this.gVariable.authentication.doctorId > 0)) {
      this.getCenters();
    } else if (this.gVariable.authentication.autherized && (this.gVariable.authentication.centers[0]) && (this.gVariable.authentication.centers[0].centerId > 0)) {
      this.getDoctors();
    }
  }



  public print() {
    window.print();
  }

  public getAllCentersList() {
    let doctorId = this.searchObject.selectedDoctor.doctorId || 0;
    if (this.gVariable.authentication && this.gVariable.authentication.roles && (this.gVariable.authentication.roles[0].roleId ==
        this.staticConfig.userTypes.DOCTOR.id)) {
      doctorId = this.gVariable.authentication.doctorId;
      this.clientSev.getAllCentersList(doctorId).then( (response: any) => {
        this.centersList = response;
      }).catch((error: any) => {
        console.log(error);
        this.centersList = [];
      });
    } else {
      this.centersList = [];
    }
  }

  public onSelectDateRange() {
    this.getIncomeReports();
  }

  public getIncomeReports() {
    let req = {
      centerId: null,
      doctorId: null,
      startDate: this.datePipe.transform(this.rangeDates[0], 'yyyy-MM-dd'),
      endDate: this.datePipe.transform(this.rangeDates[1], 'yyyy-MM-dd') || this.datePipe.transform(this.rangeDates[0], 'yyyy-MM-dd')
    };
    if (this.gVariable.authentication && this.gVariable.authentication.roles && (this.gVariable.authentication.roles[0].roleId == this.staticConfig.userTypes.DOCTOR.id)) {
      req.doctorId = this.gVariable.authentication.doctorId;
      this.selectedDoctorId = this.gVariable.authentication.doctorId;
      if (this.searchObject.selectedCenter == 'viewAll') {
        req.centerId = null;
        this.selectedCenterId = 0;
      } else {
        req.centerId = this.searchObject.selectedCenter.centerId;
        this.selectedCenterId = this.searchObject.selectedCenter.centerId;
      }
      this.getIncomeReportsData(req);
    } else if (this.gVariable.authentication && this.gVariable.authentication.roles && (this.gVariable.authentication.roles[0].roleId == this.staticConfig.userTypes.CENTER_ADMIN.id)) {
      req.centerId = this.gVariable.authentication.centers[0].centerId;
      this.selectedCenterId = this.gVariable.authentication.centers[0].centerId;
      if (this.searchObject.selectedDoctor == 'viewAll') {
        req.doctorId = null;
        this.selectedDoctorId = 0;
      } else {
        req.doctorId = this.searchObject.selectedDoctor.doctorId;
        this.selectedDoctorId = this.searchObject.selectedDoctor.doctorId;
      }
      this.getIncomeReportsData(req);
    } else {
      // this.selectedCenterId = 0;
      this.selectedDoctorId = 0;
      if ( this.centerObject ) {
        // console.log(this.centerObject);
        req.centerId = this.centerObject.centerId;
        this.selectedCenterId = this.centerObject.centerId;
        if (this.searchObject.selectedDoctor == 'viewAll') {
          req.doctorId = null;
          this.selectedDoctorId = 0;
        } else {
          req.doctorId = this.searchObject.selectedDoctor.doctorId;
          this.selectedDoctorId = this.searchObject.selectedDoctor.doctorId;
        }
        this.getIncomeReportsData(req);
      }
    }
  }

  public onSelect(event) {
      if (this.chart.type == 'PieChart') {
        if (this.gVariable.authentication && this.gVariable.authentication.roles && (this.gVariable.authentication.roles[0].roleId == this.staticConfig.userTypes.DOCTOR.id)) {
          const inventory = this.centersList;
          const result = inventory.find(center => center.centerName === this.Byclinic[event[0].row][0]);
          this.searchObject.selectedCenter = result;
        } else {
          const inventory = this.doctorsList;
          const result = inventory.find(doctor => doctor.user.firstName + ' ' + doctor.user.lastName === this.ByDoctor[event[0].row][0]);
          this.searchObject.selectedDoctor = result;
        }
          this.getIncomeReports();
      }
  }

  private getIncomeReportsData(req) {
    this.clientSev.getIncomeReports(req).then( (response: any ) => {
       // console.log(response);
      if (response.status == 200) {
        this.income = response.data;
        // response.data.centerWiseIncomes

        if (response.data.centerWiseIncomes) {
          const incomeByclinic = response.data.centerWiseIncomes;
          const incomeBySession = response.data.centerWiseIncomes;
          const incomeBySessionSum = process(incomeBySession, {
            group: [{
              field: 'sessionIncome.sessionId',
              aggregates: [
                { aggregate: 'sum', field: 'sessionIncome.amount' },
                { aggregate: 'sum', field: 'sessionIncome.totalAppointments' },
              ]
            }]
          });
          // console.log(JSON.stringify(incomeBySessionSum.data, null, 2));
          const incomeByclinicSum = process(incomeByclinic, {
            group: [{
              field: 'centerId',
              aggregates: [
                { aggregate: 'sum', field: 'sessionIncome.amount' },
                { aggregate: 'sum', field: 'sessionIncome.totalAppointments' },
              ]
            }]
          });
          this.Byclinic = [];
          incomeByclinicSum.data.forEach(item => {
            this.Byclinic.push([ item.items[0].name, item.aggregates['sessionIncome.amount'].sum, item.aggregates['sessionIncome.totalAppointments'].sum ]);
          });
          this.BySession = [];
          incomeBySessionSum.data.forEach(item => {
            this.BySession.push([ item.items[0].sessionIncome.appointmentDate + '\n' + item.items[0].sessionIncome.startTime, item.aggregates['sessionIncome.amount'].sum , item.aggregates['sessionIncome.totalAppointments'].sum ]);
          });

          if (this.searchObject.selectedCenter == 'viewAll') {
            this.chart.data = this.Byclinic;
            this.chart.type = 'PieChart';
            this.chart.options = { width: this.screenWidth * 0.8, height: this.screenHeight * 0.5, animation: {duration: 250, easing: 'ease-in-out',
                    startup: true}, legend: { position: 'right', maxLines: 2 }, chartArea: { right: 13, left: 6 }};
          } else {
            this.chart.data = this.BySession;
            this.chart.type = 'Bar';
            this.chart.columnNames = ['Sessions', 'Income'];
            this.chart.options = { width: this.screenWidth * 0.5, height: this.screenHeight * 0.3, animation: {duration: 250, easing: 'ease-in-out',
                    startup: true}, legend: { position: 'right', maxLines: 2 }, chartArea: { right: 13, left: 6 }};
          }
        }

        if (response.data.sessionWiseIncomes) {
          const incomeByDoctor = response.data.sessionWiseIncomes;
          const incomeBySession = response.data.sessionWiseIncomes;
          const incomeBySessionSum = process(incomeBySession, {
            group: [{
              field: 'sessionIncome.sessionId',
              aggregates: [
                { aggregate: 'sum', field: 'sessionIncome.amount' },
                { aggregate: 'sum', field: 'sessionIncome.totalAppointments' },
              ]
            }]
          });
          const incomeByDoctorSum = process(incomeByDoctor, {
            group: [{
              field: 'doctorId',
              aggregates: [
                { aggregate: 'sum', field: 'sessionIncome.amount' },
                { aggregate: 'sum', field: 'sessionIncome.totalAppointments' },
              ]
            }]
          });
          // console.log(incomeByDoctor)
          // console.log(JSON.stringify(incomeByDoctorSum, null, 2));
          // console.log(JSON.stringify(incomeByclinicSum.data, null, 2));

          this.ByDoctor = [];
          incomeByDoctorSum.data.forEach(item => {
            this.ByDoctor.push([ item.items[0].name, item.aggregates['sessionIncome.amount'].sum, item.aggregates['sessionIncome.totalAppointments'].sum ]);
          });
          // console.log('products - ' + JSON.stringify(this.ByDoctor));
          this.BySession = [];
          incomeBySessionSum.data.forEach(item => {
            this.BySession.push([ item.items[0].sessionIncome.appointmentDate + '\n' + item.items[0].sessionIncome.startTime, item.aggregates['sessionIncome.amount'].sum, item.aggregates['sessionIncome.totalAppointments'].sum ]);
          });

          if (this.searchObject.selectedDoctor == 'viewAll') {
            this.chart.data = this.ByDoctor;
            this.chart.type = 'PieChart';
            this.chart.options = { width: this.screenWidth * 0.8, height: this.screenHeight * 0.5, animation: {duration: 250, easing: 'ease-in-out',
                    startup: true}, legend: { position: 'right', maxLines: 2 }, chartArea: { right: 13, left: 6 }};
          } else {
            this.chart.data = this.BySession;
            this.chart.type = 'Bar';
            this.chart.columnNames = ['Sessions', 'Income'];
            this.chart.options = { width: this.screenWidth * 0.5, height: this.screenHeight * 0.3, animation: {duration: 250, easing: 'ease-in-out',
                    startup: true}, legend: { position: 'right', maxLines: 2 }, chartArea: { right: 13, left: 6 }};
          }
        }


      } else {
        this.income = [];
      }
    }).catch( (error: any) => {
      // console.log(error);
      this.income = [];
    });
  }

  private getSelectedCenter() {
    if (this.selectedCenterObject) {
      this.centerObject = this.selectedCenterObject;
    } else {
      this.centerObject = this.geoStorageSev.getSelectedCenter();
    }
    // console.log(this.centerObject);
  }

  public getAllDoctorsList() {
    let centerId = 0;
    if (this.centerObject && Object.keys(this.centerObject).length > 0) {
      centerId = this.centerObject.centerId;
    } else if ((this.gVariable.authentication && this.gVariable.authentication.roles && (this.gVariable.authentication.roles[0].roleId ==
        this.staticConfig.userTypes.CENTER_ADMIN.id)) && this.gVariable.authentication.centers[0]) {
      centerId = this.gVariable.authentication.centers[0].centerId;
    }
    this.clientSev.getAllDoctorsList(centerId).then((response: any) => {
      if (response) {
        this.doctorsList = response;
      }
    }).catch((error: any) => {
      console.log(error);
      this.doctorsList = [];
    });
  }

  private getCenters() {
    const doctorId = this.gVariable.authentication.doctorId || 0;
    this.clientSev.getAllCentersList(doctorId).then( (response: any) => {
      this.centersList = response;
    }).catch((error: any) => {
      console.log(error);
      this.centersList = [];
    });
  }


  private getDoctors() {
    const centerId = this.gVariable.authentication.centers[0].centerId;
    this.clientSev.getAllDoctorsList(centerId).then( (response: any) => {
      if (response) {
        this.doctorsList = response;
      }
    }).catch( (error: any) => {
      console.log(error);
      this.doctorsList = [];
    });
  }

}
