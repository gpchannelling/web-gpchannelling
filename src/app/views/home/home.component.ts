import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import { StaticConfig, GlobalVariable , AppConfig } from '../../core';
import { ClientService } from '../../services';
import { } from 'googlemaps';
import {ExternalJs} from '../../core/com-classes';

declare var $: any;
declare var google: any;
declare var window: any;

const GROUP_KEY = makeStateKey<string>('GROUP_KEY');

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


    public appConfig = AppConfig;
    public staticConfig = StaticConfig;

    public userType = 1;
    public radiusRange = 15;

    constructor(title: Title,
                private clientSev: ClientService,
                private tState: TransferState,
                public gVariable: GlobalVariable,
                private externalJS: ExternalJs) {
      title.setTitle(StaticConfig.pageTitle + ' - HOME');

    }

    ngOnInit() {
      if (this.tState.hasKey(GROUP_KEY)) {
        // We are in the browser

      } else if (!this.gVariable.platformBrowser) {
        // We are on the server

      } else {
        // No result received


      }

      this.stopLoader();
    }

    private stopLoader() {
        if (this.gVariable.platformBrowser) {
            this.externalJS.closeLoading();
        }
    }




}
