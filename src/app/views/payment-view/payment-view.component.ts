import {Component, Input, OnInit} from '@angular/core';
import {ApiServiceConfig, AppConfig, StaticConfig} from '../../core/config';
import {ClientService, SessionStorageService} from '../../services';
import {ActivatedRoute, Router} from '@angular/router';
import {BookingCartService} from '../../services';
import {ExternalJs, GlobalVariable} from '../../core/com-classes';
import {MessagesService} from '../../services/message.service';
import {Title} from '@angular/platform-browser';
import {LocalStorageService} from '../../core/services';

@Component({
  selector: 'app-payment-view',
  templateUrl: './payment-view.component.html',
  styleUrls: ['./payment-view.component.css']
})
export class PaymentViewComponent implements OnInit {

  @Input() appointmentData: any;

  public appConfig: any = AppConfig;
  public staticConfig: any = StaticConfig;

  public paymentTypesList: any = [];
  public selectedPaymentType: any = '';
  public termsAndConditions = false;
  public totalCharge = 0;
  public appointmentDetailsPayment: any = {};
  public paymentStatus = false;
  public msg: any = [];
  public cart: any = {};
  public submitPending = false;
  public getTermsAndCondition = '';
  public echRefNo: any;


  constructor(private clientSev: ClientService,
              private sessionStorageSev: SessionStorageService,
              private storageSev: LocalStorageService,
              private router: Router,
              private bookingCartSev: BookingCartService,
              private acRoute: ActivatedRoute,
              public gVariable: GlobalVariable,
              private messagesSev: MessagesService,
              private externalJS: ExternalJs,
              title: Title) {

      if (this.gVariable.platformBrowser) {
          this.acRoute.data.subscribe( (params: any) => {

              if (params.isPaymentResponse) {
                  // console.log(params);
                  this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.DETAILS);
                  this.router.navigate(['', 'appointment-book', 'order-details']);
              }
          });

          this.router.events.subscribe((event: any) => {
              this.getCartDetaild();
          });
      }

      title.setTitle(StaticConfig.pageTitle + ' - PAYMENT');
  }

  ngOnInit() {
    // console.log(this.appointmentDetailsPayment);
    // console.log(this.appointmentData);

      if (!this.gVariable.platformBrowser) {

      } else if (this.gVariable.platformBrowser) {
          // this.appointmentDetailsPayment = {};
          this.getCartDetaild();
          this.getPaymentTypes();
          this.externalJS.closeLoading();
      }

  }

  private getCartDetaild() {
      if (!this.appointmentData || this.appointmentData == undefined || Object.keys(this.appointmentData).length < 1) {
          this.cart = this.bookingCartSev.getBookingCart();

          if (Object.keys(this.appointmentDetailsPayment).length == 0) {
              this.getAppointmentDetails(this.cart.appointment.refNo);
          }
      } else {
          if (Object.keys(this.appointmentDetailsPayment).length == 0) {
              this.getAppointmentDetails(this.appointmentData.refNo);
          }
      }
  }

  private getPaymentTypes() {

    this.clientSev.getPaymentTypes().then( (response: any) => {
      // console.log(response);
      this.paymentTypesList = response;
    }).catch( (error: any) => {
      console.log(error);
    });
  }

  private calculateTotalChrges() {
    this.totalCharge = 0;

    if (this.appointmentDetailsPayment && this.appointmentDetailsPayment.appointmentCharges) {

        const appointmentCharges = this.appointmentDetailsPayment.appointmentCharges;
        const onlineApplicableAppointmentCharges = [];
        const otherAppointmentCharges = [];

        for (let i = 0; i < appointmentCharges.length; i++) {

            if (appointmentCharges[i].onlinePayable == 'YES') {
                if (appointmentCharges[i].onlineViewable && (appointmentCharges[i].onlineViewable !== 'false') && (appointmentCharges[i].onlineViewable !== 'NO')) {
                    onlineApplicableAppointmentCharges.push(appointmentCharges[i]);
                }

                this.totalCharge = this.totalCharge + appointmentCharges[i].amount;
            } else {
                // if (appointmentCharges[i].onlineViewable && (appointmentCharges[i].onlineViewable !== 'false')) {
                //     otherAppointmentCharges.push(appointmentCharges[i]);
                // }

            }

        }

        this.appointmentDetailsPayment.onlineApplicableAppointmentCharges = onlineApplicableAppointmentCharges;

    }


  }

  onClickAddPayment() {
    // console.log(this.selectedPaymentType);
    this.submitPending = true;
    let req = {};
    this.storageSev.set('paymentMethod', this.selectedPaymentType);

      if (this.selectedPaymentType === 'ADDTOBILL') {
        this.echRefNo = JSON.parse(localStorage.getItem('bookingEChannelCart_pk')).appointment.refNo;
        req = {
            headingMessage: 'test',
            content: 'test',
            redirectURL: 'https://172.26.144.34:8443/web-gpchannelling/payment-confirm',
            echRef: this.echRefNo
        };
        this.clientSev.mobitelVerifyPayment(req).then( (response: any) => {
            if (response.status == 202) {
                this.messagesSev.mesageInfo('Wait until we redirect you to the payment process.....');
                this.paymentStatus = true;
                this.submitPending = false;

                setTimeout(() => {
                    this.router.navigateByUrl('/add-to-bill');
                }, 2000);

            } else {
                this.messagesSev.mesageError('Payment submission failed, ' + response.error.message + ' error');
                this.submitPending = false;
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Payment submission failed, ' + error.error.message + ' error');
            this.submitPending = false;
        });
    } else {
        req = {
            paymentMode: this.selectedPaymentType || null,
            refNo: this.appointmentDetailsPayment.refNo || 0
        };
        this.clientSev.confirmPayment(req).then( (response: any) => {
            // console.log(response);
            if (response.status == 200) {
                this.messagesSev.mesageInfo('Wait until we redirect you to the payment process.....');
                this.paymentStatus = true;
                this.submitPending = false;

                setTimeout(() => {
                    // if (window && this.gVariable && this.gVariable.webAppConfig && this.gVariable.webAppConfig.API_URL) {
                    // window.location.href = AppConfig.API_URL.PAYMENT + response.data.url;
                    // window.location.href = this.gVariable.webAppConfig.API_URL.PAYMENT + response.data.url;
                    window.location.href = 'https://172.26.144.34:8443/gpc-service' + response.data.url;

                    // }
                }, 2000);
            } else {
                this.messagesSev.mesageError('Payment submission failed, ' + response.error.message + ' error');
                this.submitPending = false;
            }
        }).catch( (error: any) => {
            this.messagesSev.mesageError('Payment submission failed, ' + error.error.message + ' error');
            this.submitPending = false;
        });
    }
  }

    private getAppointmentDetails(RefNo) {

      if ((RefNo > 0)) {
          this.clientSev.getAppointmentByRefNo(RefNo).then( (response: any) => {

              this.appointmentDetailsPayment = response;
              this.calculateTotalChrges();

          }).catch( (error: any) => {
          });
      } else {
          this.calculateTotalChrges();
      }

    }

    onCLickContinueWithoutPayment() {
        this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.DETAILS);
        this.router.navigate(['', 'appointment-book', 'order-details']);
    }


    onMouseOverTerms() {
        this.clientSev.getTermsAndConditions().then((response: any) => {
            // console.log(response);
            if (response.status == 200) {
                this.getTermsAndCondition = response.error.text;
            }
        });
    }

    closeTerms() {
        this.getTermsAndCondition = '';
    }

}
