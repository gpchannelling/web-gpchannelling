import { Component, OnInit } from '@angular/core';
import {BookingCartService, ClientService, MessagesService} from '../../services';
import {StaticConfig} from '../../core/config';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-to-bill',
  templateUrl: './add-to-bill.component.html',
  styleUrls: ['./add-to-bill.component.css']
})
export class AddToBillComponent implements OnInit {
  public mobileNo: number;
  public pinNo: number;
  public merchantRefNo = '';
  public echRefNo: any;
  public requestPending = false;
  public creditCheckSuccess = false;
  public newPin = false;
  public pinTimerText = '';
  public appointmentDetails: any;
  public appointmentCharges = [];
  public totalCharge = 0;
  public staticConfig: any = StaticConfig;


  constructor(
      private clientSev: ClientService,
      private messagesSev: MessagesService,
      private bookingCartSev: BookingCartService,
      private router: Router
  ) { }

  ngOnInit() {
    this.appointmentDetails = JSON.parse(localStorage.getItem('bookingEChannelCart_pk'));
    this.echRefNo = this.appointmentDetails.appointment.refNo;
    this.appointmentCharges = this.appointmentDetails.appointment.response.appointmentCharges;

    this.appointmentCharges.forEach((charge) => {
      this.totalCharge += parseInt(charge.amount);
    });
  }

  onSubmitMobileNumberForm() {
    this.requestPending = true;
    this.newPin = false;
    this.clientSev.creditCheck({
        echRefNo: this.echRefNo,
        mobileNo: this.mobileNo,
        serialNo: this.merchantRefNo}
    ).then((response: any) => {
      if (response.status === 202) {
        this.merchantRefNo = response.data.serialNo;
        this.requestPending = false;
        this.creditCheckSuccess = true;
        this.setCountDown();
      } else if (response.status === '403') {
        this.messagesSev.mesageError('You have exceeded maximum attempts for requesting a pin number!');
        this.requestPending = false;
      } else if (response.status === '0') {
        this.messagesSev.mesageError('You don\'t have enough credit to complete the transaction!');
        this.requestPending = false;
      } else {
        this.messagesSev.mesageError('Transaction failed due to, ' + response.error.message + ' error');
        this.requestPending = false;
      }
    }).catch((error: any) => {
      this.messagesSev.mesageError('Transaction failed due to, ' + error.error.message + ' error');
      this.requestPending = false;
    });
  }

  setCountDown() {
    this.newPin = true;
    const countDownDate = new Date().getTime() + 1000 * 45;
    const x = setInterval(() => {
      const now = new Date().getTime();
      const distance = countDownDate - now;
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);
      this.pinTimerText = 'Request new PIN in ' + seconds + 's ';
      if (distance < 0) {
        clearInterval(x);
        this.pinTimerText = '';
        this.newPin = false;
      }
    }, 1000);
  }

  onSubmitPinNumberForm() {
    this.newPin = false;
    this.requestPending = true;
    this.clientSev.creditConfirm({
      pin: this.pinNo,
      echRefNo: this.echRefNo}
    ).then((response: any) => {
      if (response.data.headingMessage === 'Your Payment Has Been Received. Thank you !') {
        this.clientSev.saveConfirmAppointment(this.echRefNo).then((response1: any) => {
          if (response1.message == 'Successfully updated') {
            this.requestPending = false;
            this.messagesSev.mesageSuccess(response.data.headingMessage);
            this.bookingCartSev.setBookingStep(this.staticConfig.appoinmentBookingStages.DETAILS);
            this.router.navigateByUrl('/payment-response');
          } else {
            this.messagesSev.mesageError('Transaction failed');
            this.requestPending = false;
            this.router.navigateByUrl('/payment-confirm');
          }
        }).catch((error: any) => {
          this.messagesSev.mesageError('Transaction failed');
          this.requestPending = false;
          this.router.navigateByUrl('/payment-confirm');
        });
      } else {
        this.messagesSev.mesageError(response.data.headingMessage);
        this.requestPending = false;
        this.router.navigateByUrl('/payment-confirm');
      }
    }).catch((error: any) => {
      this.messagesSev.mesageError('Transaction failed');
      this.requestPending = false;
      this.router.navigateByUrl('/payment-confirm');
    });
  }


}
