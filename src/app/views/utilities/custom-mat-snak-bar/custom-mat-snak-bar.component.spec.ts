import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomMatSnakBarComponent } from './custom-mat-snak-bar.component';

describe('CustomMatSnakBarComponent', () => {
  let component: CustomMatSnakBarComponent;
  let fixture: ComponentFixture<CustomMatSnakBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomMatSnakBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomMatSnakBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
