import {Component, Inject} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material';

@Component({
  selector: 'app-custom-mat-snak-bar',
  templateUrl: './custom-mat-snak-bar.component.html',
  styleUrls: ['./custom-mat-snak-bar.component.css']
})
export class CustomMatSnakBarComponent {

  public snakBarMessage: any;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {
    // console.log(data);
  }


}
