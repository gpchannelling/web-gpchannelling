import {Injectable, Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'AppointmentsStatusFilter'
})

@Injectable()

export class AppointmentsStatusFilter implements PipeTransform {

    transform(appointmentList: any[], args: number): any {
        // console.log(appointmentList);
        // console.log(args);
        // console.log(appointmentList.filter(appointment => (appointment.appointmentNo.toString().toLowerCase().indexOf(args[0]))));
        // console.log(appointmentList.filter(appointment => (appointment.appointmentNo.toString().toLowerCase() === args)));

        return appointmentList.filter(appointment => (!args ? true : appointment.status === (args)));
    }

}



